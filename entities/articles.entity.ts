import { Element } from 'mdx/types';

import type { Career, Meta, Thumbnail, ThumbnailData } from './careers.entity';
import type { Metadata } from './static-page.entity';

export enum BlockTypes {
  SECTION = 'shared.section',
  QUOTE = 'shared.quote',
  PAGE_LINK = 'shared.page-link',
  UP_SELL = 'shared.upsell',
  TABLE = 'shared.table',
  MEDIA = 'shared.media',
}

export enum UpSellTypes {
  SchoolLoan = 'school_loan',
  RetireEarly = 'retire_early',
  ToolLicenseLookup = 'tool_license_lookup',
  ToolResumeBuilder = 'tool_resume_builder',
  ToolFindSchool = 'tool_find_school',
}

export enum TableTypes {
  SALARY_RANGE_ANNUAL = 'salary_range_annual',
  SALARY_RANGE_HOURLY = 'salary_range_hourly',
  SALARY_BY_STATE = 'salary_by_state',
  CERTIFICATIONS = 'certifications',
  TOOLS = 'tools',
  EQUIPMENT = 'equipment',
  ACCESSORIES = 'accessories',
  PROTECTIVE_GEAR = 'protective_gear',
  SCHOOL_LIST = 'school_list_top',
}

export enum LinkTypes {
  INTERNAL = 'internal',
  EXTERNAL = 'external',
  UP_NEXT = 'up_next',
}

export interface Articles {
  data: Article[];
  meta: Meta;
}

export interface Article {
  id: number;
  attributes: ArticleAttributes;
}

export interface ArticleAttributes {
  title: string;
  description: string;
  slug: string;
  cover: Thumbnail;
  author: { name: string; avatar: Thumbnail; email: string; articles: ArticleAttributes[] };
  category: { data: Category };
  blocks: Block[];
  articleType: string;
  featured: boolean;
  publishedAt: string;
  updatedAt: string;
  metadata: Metadata;
  career: { data: Career };
  careerPage: string;
}

export interface Category {
  id: number;
  attributes: {
    name: 'workers' | 'employers';
    slug: string;
    description?: string;
  };
}

export interface Block {
  id: number;
  __component: string;
  file?: { data: ThumbnailData };
  altText?: string;
  title?: string;
  quote?: string;
  author?: string;
  source?: string;
  authorPhoto?: Thumbnail;
  files?: { data: ThumbnailData[] };
  body?: string | Element;
  type?: string;
  linkText?: string;
  linkURL?: string;
}
