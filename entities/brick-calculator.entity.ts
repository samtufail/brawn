export enum ProjectType {
  Residential = 'Residential',
  Commercial = 'Commercial',
}

export enum LevelOfExpertise {
  Professional = 'Professional builder',
  DIY = 'Do it yourself (DIY)',
}

export enum UnitType {
  Feet = 'Ft',
  Meters = 'Meters',
}

export enum WallThickness {
  Single = 'Single Brick',
  Double = 'Double Brick',
}

export enum BrickType {
  Standard = 'Standard',
  Queensize = 'Queensize',
  Modular = 'Modular',
  Engineer = 'Engineer',
  Utility = 'Utility',
  Closure = 'Closure',
  Kingsize = 'Kingsize',
}

export enum JointThickness {
  '10 mm (3/8 inch)' = 10,
  '12 mm (1/2 inch)' = 12,
  '6 mm (1/4 inch)' = 6,
}

export enum MixRatio {
  '1:4 cement to sand' = 0.8,
}

export interface BrickCalculator {
  projectType: ProjectType | '';
  levelOfExpertise: LevelOfExpertise | '';
  unit: UnitType;
  wallHeight: number;
  wallWidth: number;
  wallThickness: WallThickness | '';
  brickType: BrickType | '';
  jointThickness: JointThickness;
  mixRatio: number;
}

export interface Quantities {
  numberOfBricksNeeded: number;
  numberOfBags: number;
  sandCbFt: number;
}

export interface Prices {
  bricksPrice: number;
  mortarBagsPrice: number;
  sandPrice: number;
}
