import { Career, Meta } from './careers.entity';

export interface Salaries {
  data: Salary[];
  meta: Meta;
}

export interface Salary {
  id: number;
  attributes: SalaryAttributes;
}

export interface SalaryAttributes {
  state: string;
  career: Career;
  annualAvg: number;
  annualLow: number;
  annualHigh: number;
}
