import { Career, Meta } from './careers.entity';

export interface StaticPage {
  data: StaticData;
  meta: Meta;
}

export interface StaticData {
  id: number;
  attributes: StaticAttributes;
}

interface StaticAttributes {
  title: string;
  description: string;
  body: string;
  createdAt: Date;
  updatedAt: Date;
  publishedAt: Date;
  metadata: Metadata;
}

export interface Metadata {
  id: number;
  metaTitle: string;
  metaDescription: string;
  shareImage: {
    data: {
      id: number;
      attributes: {
        name: string;
        alternativeText: string;
        caption: string;
        width: number;
        height: number;
        formats: string;
        hash: string;
        ext: string;
        mime: string;
        size: number;
        url: string;
        previewUrl: string;
        provider: string;
        provider_metadata: string;
        related: { id: number; attributes: any };
        folder: { id: number; attributes: any };
        folderPath: string;
      };
    };
  };
}
