import { Meta } from './careers.entity';

export interface Schools {
  data: School[];
  meta: Meta;
}

export interface School {
  id: number;
  attributes: SchoolAttributes;
}

export interface SchoolAttributes {
  name: string;
  description: string;
  city: string;
  state: string;
  slug: string;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  zipcode: string;
  phone: string;
  rating: number;
  url: string;
  cost?: number;
}
