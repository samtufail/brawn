import type { Articles } from './articles.entity';
import type { Salaries } from './salaries.entity';

export interface Careers {
  data: Career[];
  meta: Meta;
}

export interface Career {
  id: number;
  attributes: CareerAttributes;
}

export interface CareerAttributes {
  articles: Articles;
  name: string;
  slug: string;
  description: string;
  hourlyRate: number;
  annualPay: number;
  jobSatisfaction: string;
  thumbnail: Thumbnail;
  rating: number;
  salaries: Salaries;
  faqs: FAQs;
  schools: Schools;
  licenses: Licenses;
  certifications: Certifications;
  tool: Tool;
  updatedAt: string;
}

export interface FAQs {
  data: {
    id: number;
    attributes: {
      career: Career;
      section: string;
      question: string;
      answer: string;
    };
  }[];
}

export interface Schools {
  data: {
    id: number;
    attributes: {
      career: Career;
      name: string;
      description: string;
      city: string;
      state: string;
      slug: string;
      zipcode: string;
      phone: string;
      rating: number;
      url: string;
    };
  }[];
}

export interface Licenses {
  data: {
    id: number;
    attributes: {
      career: Career;
      type: string;
      name: string;
      description: string;
      state: string;
      required: boolean;
      administrator: string;
    };
  }[];
}

export interface Certifications {
  data: {
    id: number;
    attributes: {
      career: Career;
      name: string;
      description: string;
      memberCost: number;
      nonMemberCost: number;
      programUrl: string;
      formUrl: string;
      priceUrl: string;
      slug: string;
    };
  }[];
}

export interface Tool {
  data: ToolData[];
}

export interface ToolData {
  id: number;
  attributes: ToolAttributes;
}

export interface ToolAttributes {
  career: Career;
  type: string;
  name: string;
  description: string;
  photoSourceUrl: string;
  slug: string;
  photoUrl: string;
  photoAltText: string;
}

export interface Thumbnail {
  data: ThumbnailData;
}

export interface ThumbnailData {
  id: number;
  attributes: ThumbnailDataAttributes;
}

export interface ThumbnailDataAttributes {
  name: string;
  alternativeText: any;
  caption: any;
  width?: number;
  height?: number;
  formats?: Formats;
  hash: string;
  ext: string;
  mime: string;
  size: number;
  url: string;
  previewUrl: any;
  provider: string;
  provider_metadata: any;
  createdAt: string;
  updatedAt: string;
}

export interface Formats {
  small: Small;
  thumbnail: FormatThumbnail;
  large?: Large;
  medium?: Medium;
}

export interface Small {
  ext: string;
  url: string;
  hash: string;
  mime: string;
  name: string;
  path: any;
  size: number;
  width: number;
  height: number;
  sizeInBytes: number;
}

export interface FormatThumbnail {
  ext: string;
  url: string;
  hash: string;
  mime: string;
  name: string;
  path: any;
  size: number;
  width: number;
  height: number;
  sizeInBytes: number;
}

export interface Large {
  ext: string;
  url: string;
  hash: string;
  mime: string;
  name: string;
  path: any;
  size: number;
  width: number;
  height: number;
  sizeInBytes: number;
}

export interface Medium {
  ext: string;
  url: string;
  hash: string;
  mime: string;
  name: string;
  path: any;
  size: number;
  width: number;
  height: number;
  sizeInBytes: number;
}

export interface Meta {
  pagination: Pagination;
}

export interface Pagination {
  page: number;
  pageSize: number;
  pageCount: number;
  total: number;
}
