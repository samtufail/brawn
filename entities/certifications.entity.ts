import { Career, Meta } from './careers.entity';

export interface Certifications {
  data: Certificate[];
  meta: Meta;
}

export interface Certificate {
  id: number;
  attributes: CertificateAttributes;
}

export interface CertificateAttributes {
  name: string;
  description: string;
  memberCost: number;
  nonMemberCost: number;
  programUrl: string;
  formUrl: string;
  priceUrl: string;
  slug: string;
  createdAt: string;
  updatedAt: string;
  publishedAt: string;
  career: { data: Career };
}
