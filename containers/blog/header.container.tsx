import Image from 'next/image';
import Link from 'next/link';
import { getFeaturedBlogArticles } from '@/api/blog';
import { Article } from '@/entities';
import clsx from 'clsx';

import { formatDate } from '@/lib/utils';

function BlogFeatureCard({
  attributes: {
    publishedAt,
    title,
    slug,
    cover: {
      data: {
        attributes: { url },
      },
    },
    category: {
      data: {
        attributes: { name },
      },
    },
  },
}: Article) {
  return (
    <Link prefetch={false} href={`/blog/${slug}`}>
      <div className="relative mb-[32px] h-[288px] w-full rounded-[4px]">
        <Image
          src={url || ''}
          fill
          alt={`An Image representing ${title}`}
          className="rounded-[4px] object-cover object-center"
        />
      </div>
      <h3 className="mb-[8px] text-[24px] font-[600] tracking-[-0.24px] text-[#101828]">{title}</h3>
      <p className="mb-[24px] text-[16px] uppercase text-gray-500">{formatDate(publishedAt)}</p>
      <div
        className={clsx('w-fit rounded-[6px] bg-opacity-[0.08] px-[16px] py-[6px]', {
          'bg-[#46b8ff]': name === 'workers',
          'bg-[#8958FE]': name === 'employers',
        })}
      >
        <span
          className={clsx('text-[14px] font-[600] capitalize tracking-[-0.14px]', {
            'text-[#46b8ff]': name === 'workers',
            'text-[#8958FE]': name === 'employers',
          })}
        >
          {name}
        </span>
      </div>
    </Link>
  );
}

export async function Header() {
  const featured = await getFeaturedBlogArticles();
  return (
    <header className="bg-cover bg-center bg-no-repeat xl:bg-[url(/images/blog/bg.jpg)]">
      <div className="global-container">
        <div className="flex flex-col items-center gap-[24px] bg-neutral60 px-[20px] py-[80px] text-center xl:bg-[unset] xl:px-0 xl:py-[91px]">
          <h1 className="text-[48px] font-[700] tracking-[-0.48px] text-neutral90 xl:text-[72px] xl:tracking-[-0.72px]">
            Our Blog
          </h1>
          <p className="text-[18px] text-neutral20 xl:text-[20px]">
            The Brawn blog covers insightful tips and trends on the construction and trades
            industry.
          </p>
        </div>
        <div className="px-[20px] py-[40px] xl:mb-[80px] xl:px-[60px] xl:py-0">
          <h2 className="mb-[24px] text-[24px] font-[600] tracking-[-0.24px] text-neutral90">
            New Articles
          </h2>
          <div className="grid gap-[32px] xl:grid-cols-2">
            {featured.data.map((post) => (
              <BlogFeatureCard key={post.id} {...post} />
            ))}
          </div>
        </div>
      </div>
    </header>
  );
}
