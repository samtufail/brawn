import Image from 'next/image';
import Link from 'next/link';
import { getAllBlogArticles } from '@/api/blog';
import { Article } from '@/entities';
import clsx from 'clsx';

import { formatDate } from '@/lib/utils';

export function ArticleCard({
  attributes: {
    publishedAt,
    title,
    description,
    slug,
    cover: {
      data: {
        attributes: { url },
      },
    },
    category: {
      data: {
        attributes: { name },
      },
    },
  },
}: Article) {
  return (
    <Link
      prefetch={false}
      href={`/blog/${slug}`}
      className="grid items-start gap-[24px] xl:grid-cols-[364px_1fr]"
    >
      <div className="relative min-h-[196px] w-full">
        <Image
          src={url || ''}
          fill
          alt={`An Image Representing ${title}`}
          className="rounded-[4px] object-cover object-center"
        />
      </div>
      <div>
        <p className="mb-[20px] text-[16px] uppercase text-gray-500">{formatDate(publishedAt)}</p>
        <h3 className="mb-[12px] text-[20px] font-[600] tracking-[-0.2px] text-gray-900">
          {title}
        </h3>
        <p className="mb-[24px] text-ellipsis text-[16px] text-neutral70">{description}</p>
        <div
          className={clsx('w-fit rounded-[2px] bg-opacity-[0.08] px-[16px] py-[6px]', {
            'bg-[#46b8ff]': name === 'workers',
            'bg-[#8958FE]': name === 'employers',
          })}
        >
          <span
            className={clsx('text-[14px] font-[600] capitalize tracking-[-0.14px]', {
              'text-[#46b8ff]': name === 'workers',
              'text-[#8958FE]': name === 'employers',
            })}
          >
            {name}
          </span>
        </div>
      </div>
    </Link>
  );
}

export async function Articles() {
  const articles = await getAllBlogArticles();
  return (
    <section className="global-container px-[20px] py-[40px] xl:px-[60px] xl:py-[80px]">
      <h2 className="mb-[24px] text-[24px] font-[600] tracking-[-0.24px]">All Articles</h2>
      <div className="flex flex-col gap-[48px]">
        {articles.data.map((article) => (
          <ArticleCard key={article.id} {...article} />
        ))}
      </div>
    </section>
  );
}
