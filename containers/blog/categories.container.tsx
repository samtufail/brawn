import { getCategories } from '@/api/blog';

function Card({ name }: { name: string }) {
  return (
    <div className="rounded-lg border border-solid border-neutral60 p-6 text-[20px] capitalize text-neutral90">
      {name}
    </div>
  );
}

export async function Categories() {
  const categories = await getCategories();
  return (
    <section className="global-container px-[20px] py-[40px] xl:p-[60px]">
      <h2 className="mb-[24px] text-[20px] font-[600] tracking-[-0.24px] text-neutral90 xl:text-[24px]">
        Categories
      </h2>
      <div className="grid gap-[12px] xl:grid-cols-3 xl:gap-[16px]">
        {categories.data.map((category) => (
          <Card name={category.attributes.name} key={category.id} />
        ))}
      </div>
    </section>
  );
}
