import { Button } from '@/components';
import { Article } from '@/entities';

import { numberWithCommas } from '@/lib/utils';

import { Links } from './links.container';

function CareerInfo({ value, title }: { value: string; title: string }) {
  return (
    <div className="flex flex-col gap-[4px]">
      <p className="text-[16px] font-[600] leading-[1.4] tracking-[-0.16px] text-neutral90">
        {value}
      </p>
      <p className="text-[12px] leading-[1.6] tracking-[-0.12px] text-neutral70">{title}</p>
    </div>
  );
}

const links = [
  { link: '/overview', name: 'Overview' },
  { link: '/how-to-become', name: 'How To Become' },
  { link: '/salary', name: 'Salary' },
  { link: '/schools', name: 'Schools' },
  { link: '/certifications-licenses', name: 'Certifications licenses' },
  { link: '/tools-equipment', name: 'Tools equipment' },
];

export async function Sidebar({ slug, article }: { slug: string; article: Article }) {
  const {
    career: {
      data: {
        attributes: {
          name,
          annualPay,
          hourlyRate,
          jobSatisfaction,
          thumbnail: {
            data: {
              attributes: { url, formats },
            },
          },
        },
      },
    },
  } = article.attributes;
  return (
    <aside className="bg- bg-[#f7f7f7] py-[40px] xl:bg-transparent xl:p-0">
      {/* Avatar + Title */}
      <div className="grid grid-cols-[60px_1fr] items-center gap-[16px] px-[20px] xl:px-0">
        <div className="relative size-[60px] rounded-full">
          <img
            src={formats?.thumbnail?.url || url || ''}
            alt="logo-icon"
            className="absolute size-full rounded-full object-cover object-center"
          />
        </div>
        <h2 className="text-[18px] font-bold leading-[1.4] tracking-[-0.18px] text-neutral90">
          {name}
        </h2>
      </div>
      {/* Career Info */}
      <div className="mt-[24px] flex items-center justify-between gap-[18px] px-[20px] xl:px-0">
        <CareerInfo value={`$${numberWithCommas(annualPay)}`} title="avg pay" />
        <CareerInfo value={`$${hourlyRate}/hr`} title="avg hourly" />
        <CareerInfo value={jobSatisfaction} title="satisfaction" />
      </div>
      {/* Links */}
      <div className="ml-[20px] mt-[32px] xl:ml-0 xl:mt-[40px]">
        <p className="hidden text-[14px] leading-normal text-[#969bab] xl:block">
          TABLE OF CONTENTS
        </p>
        <div className="no-scrollbar mt-[12px] flex flex-nowrap gap-[8px] overflow-auto xl:flex-col">
          <Links links={links} slug={slug} />
        </div>
      </div>
      {/* Retire Early */}
      <div className="mt-[52px] hidden bg-[#ffede5] px-[18px] py-[32px] xl:block">
        <h4 className="text-[20px] font-bold leading-[1.4] tracking-[-0.2px] text-neutral90">
          Retire Early
        </h4>
        <p className="mt-[12px] leading-[1.7] text-neutral90">
          Brawn helps blue collar workers make more, save more, and retire early
        </p>
        <Button className="mt-[27px] w-full">Retire Early</Button>
      </div>
    </aside>
  );
}
