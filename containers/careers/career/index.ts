import dynamic from 'next/dynamic';

export const HeaderContainer = dynamic(() =>
  import('./header.container').then((mod) => mod.HeaderContainer),
);
export const Sidebar = dynamic(() => import('./sidebar.container').then((mod) => mod.Sidebar));
