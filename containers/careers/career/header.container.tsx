import { Article } from '@/entities';

export async function HeaderContainer({ article }: { article: Article }) {
  const {
    attributes: {
      career: {
        data: {
          attributes: {
            name,
            thumbnail: {
              data: {
                attributes: { url, formats },
              },
            },
          },
        },
      },
    },
  } = article;

  return (
    <>
      <header className="global-container relative mt-[16px] max-h-[360px] rounded-[4px] bg-cover bg-center bg-no-repeat text-white">
        <img
          src={
            formats?.medium?.url ||
            formats?.small?.url ||
            formats?.thumbnail?.url ||
            formats?.large?.url ||
            url ||
            ''
          }
          alt={`${name} worker.`}
          className="h-[360px] w-full rounded-[4px] object-cover object-center"
          loading="eager"
          fetchPriority="high"
        />
        <div className="absolute top-0 size-full rounded-[4px] bg-black opacity-25" />
      </header>
      <div className="mb-[24px] mt-[36px] hidden items-center justify-between rounded-[6px] bg-[#eeeff4] px-[24px] py-[32px] xl:flex">
        <p className="text-[18px] font-[500] leading-[1.4] text-neutral90">
          Do you want to live well as a&nbsp;
          {name.toLowerCase()}
          &nbsp;?
        </p>
        <div className="flex items-center gap-[12px]">
          <a href="https://brawnhq.typeform.com/to/dGqJYsmZ?product_slug=livewell">
            <button
              type="button"
              className="rounded-[4px] border border-solid border-[#021e4a] bg-[#021e4a] px-[20px] py-[10px] text-[12px] font-[600] tracking-[-0.12px] text-white"
            >
              Yes, I need help
            </button>
          </a>
          <button
            type="button"
            className="rounded-[4px] border border-solid border-[#021e4a] px-[20px] py-[10px] text-[12px] font-[600] tracking-[-0.12px] text-[#021e4a]"
          >
            No thanks, I got it
          </button>
        </div>
      </div>
    </>
  );
}
