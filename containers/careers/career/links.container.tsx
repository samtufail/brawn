'use client';

import Link from 'next/link';
import { usePathname } from 'next/navigation';
import clsx from 'clsx';

export function Links({ links, slug }: { links: { name: string; link: string }[]; slug: string }) {
  const pathname = usePathname();
  return links?.map(({ link, name }) => {
    const current = `/careers/${slug}${link}`;
    return (
      <Link
        key={link}
        href={current}
        prefetch={false}
        className={clsx(
          {
            'border-[#F7E9E3] bg-[#F7E9E3] text-primary xl:border-none xl:bg-transparent':
              pathname.includes(current),
          },
          'border border-solid border-neutral30 px-[12px] py-[6px] text-[14px]',
          'xl:border-none xl:p-0 xl:text-[16px]',
          'text-nowrap capitalize text-neutral70',
        )}
      >
        {name}
      </Link>
    );
  });
}
