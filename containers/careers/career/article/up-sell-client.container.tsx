'use client';

import { FormEvent } from 'react';
import { useParams, useRouter } from 'next/navigation';
import { Button } from '@/components';
import { states } from '@/data/career-hub';
import { UpSellTypes } from '@/entities/articles.entity';
import clsx from 'clsx';
import Select from 'react-select';

import { type UpSellProps } from './up-sell.container';

function SelectElement() {
  const router = useRouter();
  const params: { slug: string; article: string } = useParams();
  return (
    <Select
      options={states}
      placeholder="Select State"
      className="w-full rounded-[4px] text-left text-[16px] leading-normal text-neutral20 focus:outline-none xl:w-[369px] xl:grow"
      classNames={{
        control: () => 'h-[48px]',
      }}
      onChange={(e) => {
        router.push(`/careers/${params.slug}/${params.article}/${e?.value}`);
      }}
    />
  );
}

export function UpSellClient({ name, type }: UpSellProps) {
  async function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
  }

  const headingText =
    type === UpSellTypes.ToolFindSchool
      ? `${name} school in my area.`
      : 'Confirm license requirements in your area';

  const description =
    type === UpSellTypes.ToolFindSchool
      ? `Want to find ${name} school in your area, enter your zip code below:`
      : `${name} licensing requirements can vary by state. To look up the requirements in your area use our easy tool`;

  return (
    <div className="global-container mb-[32px]  text-center xl:mb-[52px] xl:p-0 xl:px-[20px] xl:py-[40px]">
      <div className="flex min-h-[376px] flex-col rounded-[4px] bg-[#021E4A] bg-[url(/images/common/news-letter-v2/bg-mobile.png)] bg-contain bg-no-repeat px-[18px] py-[40px] xl:min-h-[382px] xl:items-center xl:justify-center xl:bg-[url(/images/careers/career/article/upsell-tool-bg.png)] xl:p-0">
        <h2
          className={clsx(
            'text-[24px] font-[600] leading-[1.35] tracking-[-0.54px] text-white xl:text-[36px]',
            {
              'max-w-[396px]': type === UpSellTypes.ToolFindSchool,
              'max-w-[525px]': type === UpSellTypes.ToolLicenseLookup,
            },
          )}
        >
          {headingText}
        </h2>
        <p className="mt-[16px] max-w-[609px] text-[14px] leading-normal text-neutral40 xl:text-[18px]">
          {description}
        </p>
        <form
          onSubmit={onSubmit}
          className="mt-[32px] flex max-h-[48px] w-full flex-col gap-[12px] xl:w-[586px] xl:flex-row "
        >
          <SelectElement />
          <Button type="submit" className="w-full grow-0 xl:w-[220px]">
            Find School Near Me
          </Button>
        </form>
      </div>
    </div>
  );
}
