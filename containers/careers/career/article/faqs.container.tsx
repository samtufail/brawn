import { Accordion, AccordionContent, AccordionItem, AccordionTrigger } from '@/components';
import { FAQs } from '@/entities/careers.entity';

export async function FrequentlyAskedQuestions({ faqs, article }: { faqs: FAQs; article: string }) {
  const pageFAQs = faqs.data.filter((el) => el.attributes.section === article);
  return pageFAQs.length ? (
    <div className="mb-[52px]">
      <h1 className="mb-[35.5px] text-[32px] font-bold leading-[1.4] tracking-[-0.32px] text-neutral90">
        Frequently asked questions
      </h1>
      <Accordion type="single" collapsible>
        {pageFAQs.map((el) => (
          <AccordionItem value={el.attributes.question} key={el.attributes.question}>
            <AccordionTrigger className="text-[18px] font-bold leading-[1.4] tracking-[-0.18px] text-neutral90">
              {el.attributes.question}
            </AccordionTrigger>
            <AccordionContent className="text-[14px] leading-[1.7] text-neutral70">
              {el.attributes.answer}
            </AccordionContent>
          </AccordionItem>
        ))}
      </Accordion>
    </div>
  ) : null;
}
