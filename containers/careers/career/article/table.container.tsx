import { Article, Block, TableTypes } from '@/entities/articles.entity';

import {
  AvgSalaryAnnual,
  AvgSalaryPerHour,
  Certifications,
  SalaryByState,
  SchoolsTable,
  ToolsEquipmentTable,
} from './tables';

export function Table({ ...rest }: Block & Article & { page: string; state: string }) {
  const { slug } = rest.attributes.career.data.attributes;
  switch (rest.type) {
    case TableTypes.SALARY_RANGE_ANNUAL:
      return <AvgSalaryAnnual slug={slug} />;
    case TableTypes.SALARY_RANGE_HOURLY:
      return <AvgSalaryPerHour slug={slug} />;
    case TableTypes.SALARY_BY_STATE:
      return <SalaryByState slug={slug} />;
    case TableTypes.CERTIFICATIONS:
      return <Certifications slug={slug} />;
    case TableTypes.TOOLS:
      return <ToolsEquipmentTable slug={slug} type="tool" />;
    case TableTypes.ACCESSORIES:
      return <ToolsEquipmentTable slug={slug} type="accessory" />;
    case TableTypes.EQUIPMENT:
      return <ToolsEquipmentTable slug={slug} type="equipment" />;
    case TableTypes.PROTECTIVE_GEAR:
      return <ToolsEquipmentTable slug={slug} type="protective_equipment" />;
    case TableTypes.SCHOOL_LIST:
      return <SchoolsTable slug={slug} page={rest.page} state={rest.state} />;
    default:
      return <div />;
  }
}
