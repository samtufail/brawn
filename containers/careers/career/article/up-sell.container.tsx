import { ReactNode } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components';
import { Block, UpSellTypes } from '@/entities/articles.entity';
import { CheckListIcon, DollarChartIcon, TargetIcon } from '@/icons';

import { UpSellClient } from './up-sell-client.container';

const data = [
  {
    id: 'asdhquwi1123',
    icon: <DollarChartIcon />,
    text: 'Maximize your earnings',
  },
  {
    id: '23hiu959823tyh',
    icon: <TargetIcon />,
    text: 'Set your retirement goals',
  },
  {
    id: 'c12354mu909027',
    icon: <CheckListIcon />,
    text: 'Put your retirement plan into action',
  },
];

export interface UpSellProps extends Block {
  name: string;
}

function CommonRetireSchool({ children }: { children: ReactNode }) {
  return (
    <div className="global-container pb-[32px] xl:pb-[52px]">
      <div className="relative min-h-[345px] overflow-hidden rounded-[4px] bg-[#ffede5] px-[18px] py-[24px] xl:px-0 xl:pb-0 xl:pt-[52px]">
        <Image
          src="/images/careers/construction/brawn-lines.png"
          alt="logo background"
          width={150}
          height={97}
          className="absolute right-0 top-[-30px] hidden xl:inline-block"
        />
        <Image
          src="/images/careers/construction/brawn-lines-mobile.png"
          alt="logo background"
          width={50}
          height={43}
          className="absolute right-0 top-0 xl:hidden"
        />
        <div className="mx-auto flex-col items-center justify-center gap-[20px] xl:max-w-[928px] xl:flex-row xl:gap-[52px] xl:pr-[50px]">
          {children}
        </div>
      </div>
    </div>
  );
}

function UpSellRetireEarly({ name }: UpSellProps) {
  const text = `Do you want to retire rearly as a ${name}?`;
  return (
    <CommonRetireSchool>
      <div className="relative mb-[20px] hidden h-[355.7px] w-[299px] xl:absolute xl:mb-0 xl:block xl:h-[410px] xl:w-[291px]">
        <Image
          src="/images/careers/career/article/retire.png"
          alt="Do you want to Retire Early from your job?"
          fill
          className="object-contain object-center"
        />
      </div>
      <div className="relative mb-[20px] block h-[355.7px] w-[299px] xl:absolute xl:mb-0 xl:hidden xl:h-[410px] xl:w-[291px]">
        <Image
          src="/images/careers/career/article/retire-mobile.png"
          alt="Do you want to Retire Early from your job?"
          fill
          className="object-contain object-center"
        />
      </div>
      <div className="w-full xl:pb-[50px] xl:pl-[345px]">
        <h2 className="text-[24px] font-bold leading-[1.4] tracking-[0.24px] text-neutral90 xl:text-[32px] xl:leading-[1.4] xl:tracking-[-0.32px]">
          {text}
        </h2>
        <p className="mt-[24px] text-[14px] font-bold leading-normal text-neutral20 xl:mt-[32px] xl:text-[16px]">
          Brawn can help to you make it real
        </p>
        <div className="mt-[20px] flex flex-col gap-[12px]">
          {data.map((el) => (
            <div key={el.id} className="flex items-center gap-[18px]">
              {el.icon}
              <p className="text-[14px] leading-normal text-[#121212] xl:text-[16px]">{el.text}</p>
            </div>
          ))}
        </div>
        <Link href="https://brawnhq.typeform.com/to/dGqJYsmZ?product_slug=retireearly">
          <Button className="mt-[32px] w-full xl:w-[150px]">Show more</Button>
        </Link>
      </div>
    </CommonRetireSchool>
  );
}

function UpSellSchoolLoan({ name }: UpSellProps) {
  const text = `Need a loan for ${name} School?`;
  return (
    <CommonRetireSchool>
      <div className="absolute bottom-0 hidden h-[300px] w-[291px] xl:block xl:h-[321px]">
        <Image
          src="/images/careers/career/article/school-loan.png"
          alt="Do you want to Retire Early from your job?"
          fill
          className="object-contain object-top"
        />
      </div>
      <div className="relative mb-[20px] block h-[355.7px] w-[299px] xl:hidden">
        <Image
          src="/images/careers/career/article/retire-mobile.png"
          alt="Do you want to Retire Early from your job?"
          fill
          className="object-contain object-center"
        />
      </div>
      <div className="w-full xl:pb-[50px] xl:pl-[345px]">
        <h2 className="text-[24px] font-bold leading-[1.4] tracking-[0.24px] text-neutral90 xl:text-[32px] xl:leading-[1.4] xl:tracking-[-0.32px]">
          {text}
        </h2>
        <p className="mt-[24px] text-[14px] leading-normal text-neutral20 xl:mt-[32px] xl:text-[16px]">
          Brawn can help to you get financing for your schooling
        </p>
        <Button className="mt-[32px]">Learn more</Button>
      </div>
    </CommonRetireSchool>
  );
}

function UpSellToolResumeAndGetRich({ name, type }: UpSellProps) {
  const heading =
    type === UpSellTypes.ToolResumeBuilder ? `Create a killer ${name} Resume In Minutes` : '';
  const buttonText = type === UpSellTypes.ToolResumeBuilder ? 'Create Free Resume' : '';
  return (
    <div className="global-container pb-[32px] xl:pb-[52px]">
      <div className="relative overflow-hidden rounded-[4px] bg-[#ffede5] py-[40px] xl:min-h-[246px]">
        <Image
          src="/images/careers/construction/brawn-lines.png"
          alt="logo background"
          width={150}
          height={97}
          className="absolute right-0 top-[-30px] hidden xl:inline-block"
        />
        <Image
          src="/images/careers/construction/brawn-lines.png"
          alt="logo background"
          width={150}
          height={97}
          className="absolute bottom-[-30px] left-[-5px] hidden xl:inline-block"
        />
        <Image
          src="/images/careers/construction/brawn-lines-mobile.png"
          alt="logo background"
          width={65}
          height={97}
          className="absolute right-0 top-[-30px] inline-block xl:hidden"
        />
        <Image
          src="/images/careers/construction/brawn-lines-mobile.png"
          alt="logo background"
          width={65}
          height={97}
          className="absolute bottom-[-30px] left-[-5px] inline-block xl:hidden"
        />
        <div className="mx-auto flex flex-col items-center justify-center gap-[32px]">
          <h2 className="max-w-[500px] text-center text-[24px] font-bold leading-[1.2] tracking-[-0.36px] text-neutral90 xl:text-[36px]">
            {heading}
          </h2>
          <Button>{buttonText}</Button>
        </div>
      </div>
    </div>
  );
}

export function UpSell({ ...rest }: UpSellProps) {
  switch (rest.type) {
    case UpSellTypes.RetireEarly:
      return <UpSellRetireEarly {...rest} />;
    case UpSellTypes.SchoolLoan:
      return <UpSellSchoolLoan {...rest} />;
    case UpSellTypes.ToolFindSchool:
      return <UpSellClient {...rest} />;
    case UpSellTypes.ToolLicenseLookup:
      return <UpSellClient {...rest} />;
    case UpSellTypes.ToolResumeBuilder:
      return <UpSellToolResumeAndGetRich {...rest} />;
    default:
      return <UpSellSchoolLoan {...rest} />;
  }
}
