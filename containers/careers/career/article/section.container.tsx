import { Block } from '@/entities/articles.entity';
import clsx from 'clsx';

interface SectionProps extends Block {
  isLargeHeading?: boolean;
  hideMargin?: boolean;
}

export function Section({ title, body, isLargeHeading = false, hideMargin = false }: SectionProps) {
  const common =
    'font-bold leading-[1.4] tracking-[-0.32px] text-neutral90 xl:leading-[1.2] xl:tracking-[-0.48px]';
  return (
    <div className={clsx({ 'mb-[32px] xl:mb-[52px]': !hideMargin })}>
      {isLargeHeading ? (
        <h1 className={clsx(common, 'text-[32px] xl:text-[48px]')}>{title}</h1>
      ) : (
        <h2 className={clsx(common, 'text-[32px]')}>{title}</h2>
      )}
      <div className="mdx-body mt-[12px] text-[16px] leading-normal text-neutral20 xl:mt-[16px] xl:text-[18px]">
        {body}
      </div>
    </div>
  );
}
