import Link from 'next/link';
import { getCareerWithSalaries } from '@/api/careers';
import { Table, TableBody, TableCell, TableHead, TableHeader, TableRow } from '@/components';

import { kebabize, numberWithCommas, sortSalariesByState } from '@/lib/utils';

const data = [
  {
    state: 'Massachusetts',
    career: { data: { id: 0, attributes: { name: 'Welder' } } },
    annualAvg: 99520,
  },
  {
    state: 'Alaska',
    annualAvg: 99190,
  },
  {
    state: 'New Jersey',
    annualAvg: 98680,
  },
];

export async function SalaryByState({ slug }: { slug: string }) {
  const res = await getCareerWithSalaries(slug);
  const salaries = sortSalariesByState(res);
  return (
    <div className="flex flex-col gap-[32px]">
      <h3 className="text-[32px] font-bold text-neutral90">
        {data[0].career?.data.attributes.name}
        &nbsp;pay by state
      </h3>
      <Table className="mb-[32px] xl:mb-[52px]">
        <TableHeader>
          <TableRow>
            <TableHead>State Name</TableHead>
            <TableHead>Average Salary</TableHead>
          </TableRow>
        </TableHeader>
        <TableBody>
          {salaries?.map((item) => (
            <TableRow key={item.attributes.state}>
              <TableCell>
                <Link
                  prefetch={false}
                  className="link"
                  href={`/careers/${slug}/salary/${kebabize(item.attributes.state)}`}
                >
                  {item.attributes.state}
                </Link>
              </TableCell>
              <TableCell>{`$${numberWithCommas(item.attributes.annualAvg)}`}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </div>
  );
}
