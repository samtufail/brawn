import { getCareerWithSalaries } from '@/api/careers';
import clsx from 'clsx';

import { calculateLowAvgHighSalaries, nFormatter } from '@/lib/utils';

function Bar({ align = 'left' }) {
  return (
    <div
      className={clsx('min-h-[32px] rounded-[2px]', {
        'bg-[linear-gradient(to_right,_#8958fe_0%,_#b354a0_22%,_#fd7044_63%,_#fb4d00_94%)]':
          align === 'left',
        'bg-[linear-gradient(to_right,_#8958fe_0%,_#b354a0_0%,_#fd7044_63%,_#fb4d00_94%)]':
          align === 'center',
        'bg-[linear-gradient(to_right,_#8958fe_0%,_#b354a0_0%,_#fd7044_0%,_#fb4d00_94%)]':
          align === 'right',
      })}
    />
  );
}

function Data({ value = 0, title = '', align = 'left' }) {
  return (
    <div
      className={clsx('mt-[16px] w-full', {
        'text-right': align === 'right',
        'text-center': align === 'center',
      })}
    >
      <h3 className="text-[20px] font-[600] leading-[1.35] tracking-[-0.54px] text-neutral90 xl:text-[36px]">
        {`$${nFormatter(value, 1)}`}
      </h3>
      <p className="mt-[8px] text-[12px] leading-normal text-neutral70 xl:text-[16px]">{title}</p>
    </div>
  );
}

export async function AvgSalaryAnnual({ slug }: { slug: string }) {
  const salaries = await getCareerWithSalaries(slug);
  const data = calculateLowAvgHighSalaries(salaries);
  return (
    <div className="mb-[32px] xl:mb-[52px]">
      {/* Lines */}
      <div className="grid grid-cols-[1fr_2fr_1fr] gap-[4px] xl:grid-cols-[1fr_390px_1fr]">
        <div>
          <Bar />
          <Data title="Bottom 20%" value={data.totalLow} />
        </div>
        <div>
          <Bar align="center" />
          <Data title="Average" value={data.totalAvg} align="center" />
        </div>
        <div>
          <Bar align="right" />
          <Data title="Top 20%" value={data.totalHigh} align="right" />
        </div>
      </div>
    </div>
  );
}
