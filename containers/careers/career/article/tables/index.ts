import dynamic from 'next/dynamic';

export const AvgSalaryAnnual = dynamic(() =>
  import('./avg-salary-annual.container').then((mod) => mod.AvgSalaryAnnual),
);
export const AvgSalaryPerHour = dynamic(() =>
  import('./avg-salary-per-hour.container').then((mod) => mod.AvgSalaryPerHour),
);
export const Certifications = dynamic(() =>
  import('./certifications.container').then((mod) => mod.Certifications),
);
export const SalaryByState = dynamic(() =>
  import('./salary-by-state.container').then((mod) => mod.SalaryByState),
);
export const SchoolsTable = dynamic(() =>
  import('./schools-table.container').then((mod) => mod.SchoolsTable),
);
export const ToolsEquipmentTable = dynamic(() =>
  import('./tools-equipment.container').then((mod) => mod.ToolsEquipmentTable),
);
