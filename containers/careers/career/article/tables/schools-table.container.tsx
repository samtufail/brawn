import Link from 'next/link';
import { getCareerSchools, getCareerSchoolsByState } from '@/api/careers';
import {
  Pagination,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableHeader,
  TableRow,
} from '@/components';

import { formatRating, kebabize, truncateString } from '@/lib/utils';

export async function SchoolsTable({
  slug,
  page,
  state = '',
}: {
  slug: string;
  page: string;
  state?: string;
}) {
  const {
    data,
    meta: {
      pagination: { pageCount },
    },
  } = state ? await getCareerSchoolsByState(slug, page, state) : await getCareerSchools(slug, page);

  return (
    <section id="schools-table">
      {data.length ? (
        <Table className="mb-[20px]">
          <TableHeader>
            <TableRow>
              <TableHead>School Name</TableHead>
              <TableHead>City</TableHead>
              <TableHead>State</TableHead>
              <TableHead>Rating</TableHead>
            </TableRow>
          </TableHeader>
          <TableBody>
            {data?.map(({ id, attributes }) => (
              <TableRow key={id}>
                <TableCell title={attributes.name}>{truncateString(attributes.name, 30)}</TableCell>
                <TableCell title={attributes.city}>{truncateString(attributes.city, 18)}</TableCell>
                <TableCell>
                  <Link
                    prefetch={false}
                    href={`/careers/${slug}/schools/${kebabize(attributes.state)}`}
                    className="link"
                  >
                    {attributes.state}
                  </Link>
                </TableCell>
                <TableCell>{formatRating(attributes.rating)}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      ) : null}
      {pageCount > 0 ? (
        <div className="mb-[32px] flex items-center justify-end xl:mb-[52px]">
          <Pagination pageCount={pageCount} />
        </div>
      ) : null}
    </section>
  );
}
