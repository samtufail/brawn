import Link from 'next/link';
import { getCareerCertifications } from '@/api/careers';
import { Button } from '@/components';
import { Certificate } from '@/entities/certifications.entity';

import { nFormatter } from '@/lib/utils';

function MemberFeeCard({ title = '', value = 0 }) {
  return (
    <div className="flex w-full flex-col gap-[28px] rounded-[8px] bg-neutral60 p-[24px]">
      <h3 className="text-[16px] leading-normal text-neutral90">{title}</h3>
      <p className="text-[36px] font-[600] leading-[1.35] tracking-[-0.54px] text-neutral90">
        {`$${nFormatter(value, 2)}`}
      </p>
    </div>
  );
}

function CertificateComponent({ ...data }: Certificate) {
  return (
    <div className="mb-[32px] xl:mb-[52px]">
      <h2 className="text-[32px] font-bold tracking-[-0.32px] xl:text-[32px]">
        {data.attributes.name}
      </h2>
      <p className="my-3 text-[16px] leading-normal xl:my-4 xl:text-[16px]">
        {data.attributes.description}
      </p>

      <Link className="link" href={data.attributes.programUrl}>
        View Program Details
      </Link>

      <h3 className="mt-8 text-[24px] font-bold tracking-[-0.32px] xl:text-[24px]">
        Cost of&nbsp;
        {data.attributes.name}
      </h3>
      <p className="my-3 text-[16px] leading-normal xl:my-4 xl:text-[18px]">
        The&nbsp;
        {data.attributes.name}
        &nbsp;program costs&nbsp;
        {`$${nFormatter(data.attributes.memberCost, 2)}`}
        &nbsp;for members and&nbsp;
        {data.attributes.memberCost ? `$${nFormatter(data.attributes.memberCost, 2)}` : 'free'}
        &nbsp;for non members.
      </p>
      <Link className="link" href={data.attributes.priceUrl}>
        View Program Details
      </Link>

      <div className="mt-6 grid grid-cols-1 gap-[20px] xl:grid-cols-2">
        <MemberFeeCard title="Member Fee" value={data.attributes.memberCost} />
        <MemberFeeCard title="Non Member Fee" value={data.attributes.nonMemberCost} />
      </div>

      <div className="my-auto mt-10 w-full xl:flex xl:justify-center">
        <Link href={data.attributes.formUrl}>
          <Button className="w-full xl:w-[142.48px]">Apply Now</Button>
        </Link>
      </div>
    </div>
  );
}

export async function Certifications({ slug }: { slug: string }) {
  const certifications = await getCareerCertifications(slug);
  if (!certifications.length) {
    return null;
  }
  return certifications.map((certificate) => (
    <CertificateComponent key={certificate.id} {...certificate} />
  ));
}
