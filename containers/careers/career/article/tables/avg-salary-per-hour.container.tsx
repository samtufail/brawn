import { getCareerWithSalaries } from '@/api/careers';

import { calculateLowAvgHighSalariesHourly, nFormatter } from '@/lib/utils';

function HourlySalaryCard({ title = '', value = 0 }) {
  return (
    <div className="flex flex-col gap-[28px] rounded-[8px] bg-neutral60 p-[24px]">
      <h3 className="text-[16px] leading-normal text-neutral90">{title}</h3>
      <p className="text-[36px] font-[600] leading-[1.35] tracking-[-0.54px] text-neutral90">
        {`$${nFormatter(value, 2)}`}
        &nbsp;
        <span className="text-[16px] font-normal leading-normal text-neutral70">/hr</span>
      </p>
    </div>
  );
}

export async function AvgSalaryPerHour({ slug }: { slug: string }) {
  const salaries = await getCareerWithSalaries(slug);
  const data = calculateLowAvgHighSalariesHourly(salaries);
  return (
    <div className="mb-[32px] grid grid-cols-1 gap-[20px] xl:mb-[52px] xl:grid-cols-3">
      <HourlySalaryCard title="Low End" value={data.totalLow} />
      <HourlySalaryCard title="Average" value={data.totalAvg} />
      <HourlySalaryCard title="High End" value={data.totalHigh} />
    </div>
  );
}
