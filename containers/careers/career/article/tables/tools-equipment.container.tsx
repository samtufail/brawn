import Image from 'next/image';
import Link from 'next/link';
import { getTools } from '@/api/careers';
import { Button } from '@/components';
import { ToolAttributes } from '@/entities/careers.entity';
import clsx from 'clsx';

import { capitalizeUnderscore } from '@/lib/utils';

function Tools({ ...rest }: ToolAttributes) {
  const { photoUrl, type, name, description, photoAltText, photoSourceUrl } = rest;
  return (
    <div className="border-b-solid mb-[32px] grid grid-cols-1 gap-[60px] border-b border-b-neutral30 pb-[40px] xl:mb-[52px] xl:grid-cols-[196px_1fr]">
      <div className="relative h-[186px] w-full rounded-[8px] xl:size-[196px]">
        <Image
          alt={photoAltText || ''}
          src={photoUrl}
          fill
          className="rounded-[8px] object-contain object-center xl:object-cover"
        />
      </div>
      <div>
        <div
          className={clsx(
            'mb-[12px] w-fit rounded-[80px] px-[12px] py-[2px] text-[12px] font-[500] capitalize leading-[1.7] text-white',
            { 'bg-[#8958FE]': type === 'tool' },
            { 'bg-[#46b8ff]': type === 'equipment' },
            { 'bg-[#0FC38D]': type === 'accessory' },
            { 'bg-primary': type === 'protective_equipment' },
          )}
        >
          {capitalizeUnderscore(type || '')}
        </div>
        <h3 className="mb-[6px] text-[18px] font-bold leading-[1.4] tracking-[-0.2px] text-neutral90 xl:text-[20px]">
          {name}
        </h3>
        <p className="mb-[32px] text-[14px] leading-[1.7] text-neutral70">{description}</p>
        <div className="flex flex-col gap-[12px] xl:flex-row">
          <Link href={photoSourceUrl} target="_blank">
            <Button>Learn more</Button>
          </Link>
        </div>
      </div>
    </div>
  );
}

export async function ToolsEquipmentTable({ slug, type }: { slug: string; type: string }) {
  const res = await getTools(slug, type || '');
  return res.map((tool) => <Tools key={tool.id} {...tool.attributes} />);
}
