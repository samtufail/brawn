import dynamic from 'next/dynamic';

export const Table = dynamic(() => import('./table.container').then((mod) => mod.Table));
export const FrequentlyAskedQuestions = dynamic(() =>
  import('./faqs.container').then((mod) => mod.FrequentlyAskedQuestions),
);
export const MediaContainer = dynamic(() =>
  import('./media.container').then((mod) => mod.MediaContainer),
);
export const PageLink = dynamic(() => import('./page-link.container').then((mod) => mod.PageLink));
export const Quote = dynamic(() => import('./quote.container').then((mod) => mod.Quote));
export const Section = dynamic(() => import('./section.container').then((mod) => mod.Section));
export const TOC = dynamic(() => import('./toc.container').then((mod) => mod.TOC));
export const UpSell = dynamic(() => import('./up-sell.container').then((mod) => mod.UpSell));
