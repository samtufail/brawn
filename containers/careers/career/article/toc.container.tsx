import { Block, BlockTypes } from '@/entities/articles.entity';
import { LogoArrowRight } from '@/icons';

import { kebabize } from '@/lib/utils';

export function TOC({ blocks }: { blocks: Block[] }) {
  return (
    <div className="mb-[32px] rounded-[4px] bg-[rgba(137,_88,_254,_0.08)] p-[24px] xl:mb-[52px]">
      <h3 className="text-[20px] font-[600] leading-[1.4] tracking-[-0.2px] text-neutral90">
        In this article we'll cover
      </h3>
      <div className="mt-[18px] flex flex-col gap-[12px]">
        {blocks.map((el, index) => {
          if (index === 0) return null;
          if (el.title === undefined || el.title === null) return null;
          if (el.__component !== BlockTypes.SECTION) return null;
          if (index > 0) {
            return (
              <a
                key={el.id}
                href={`#${kebabize(el.title)}`}
                className="grid grid-cols-[17px_1fr] gap-[12px] text-[16px] leading-normal text-[#8958fe] underline"
              >
                <LogoArrowRight />
                {el.title}
              </a>
            );
          }
          return null;
        })}
      </div>
    </div>
  );
}
