import Link from 'next/link';
import { Block, LinkTypes } from '@/entities/articles.entity';
import { LogoArrowRight } from '@/icons';
import clsx from 'clsx';

function InternalLink({ title, linkText, linkURL }: Block) {
  return (
    <section className="mb-[32px] rounded-[8px] bg-[rgba(137,_88,_254,_0.08)] p-[24px]">
      <h2 className="text-[18px] font-[600] leading-[1.4] tracking-[-0.18px] text-neutral90 xl:text-[20px] xl:tracking-[-0.2px]">
        {title}
      </h2>
      <Link
        href={`${linkURL}`}
        prefetch={false}
        className={clsx('flex items-center gap-[12px] leading-normal text-[#8958fe] underline', {
          'mt-[18px]': title,
        })}
      >
        <LogoArrowRight />
        {linkText}
      </Link>
    </section>
  );
}

function UpNext({ title, linkText, linkURL }: Block) {
  return (
    <div className="mb-[32px]">
      <p className="text-[18px] text-neutral70">Up Next</p>
      <div className="mt-[12px] rounded-[8px] bg-[rgba(137,_88,_254,_0.08)] px-[20px] py-[32px] xl:px-[40px] xl:py-[56px]">
        <h2 className="max-w-[428px] text-[20px] font-bold leading-[1.4] tracking-[-0.2px] text-neutral90 xl:text-[32px] xl:tracking-[-0.32px]">
          {title}
        </h2>
        <Link
          href={`${linkURL}`}
          prefetch={false}
          className="mt-[42px] flex items-center gap-[12px] leading-normal text-[#8958fe] underline xl:mt-[48px]"
        >
          <LogoArrowRight />
          {linkText}
        </Link>
      </div>
    </div>
  );
}

export function PageLink({ title, linkText, linkURL, type, ...rest }: Block) {
  if (type === LinkTypes.INTERNAL || type === LinkTypes.EXTERNAL) {
    return <InternalLink title={title} linkText={linkText} linkURL={linkURL} {...rest} />;
  }
  if (type === LinkTypes.UP_NEXT) {
    return <UpNext title={title} linkText={linkText} linkURL={linkURL} {...rest} />;
  }
}
