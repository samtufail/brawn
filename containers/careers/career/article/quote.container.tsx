import Image from 'next/image';
import { Block } from '@/entities/articles.entity';
import { QuoteIcon } from '@/icons';

export function Quote({ quote, author, source }: Block) {
  return (
    <div className="relative mb-[32px] rounded-[8px] bg-neutral60 px-[24px] py-[40px] xl:mb-[52px] xl:px-[64px]">
      <QuoteIcon />
      <blockquote className="mt-[32px] max-w-[556px] text-[24px] font-[500] leading-[1.4] text-neutral90">
        {quote}
      </blockquote>
      <p className="mt-[24px] text-[16px] font-[500] leading-[1.4]">
        —&nbsp;
        {author}
      </p>
      {source ? (
        <p className="mt-[6px] text-[16px] leading-normal text-[#969bab]">{source}</p>
      ) : null}
      <div className="absolute bottom-0 right-0 h-[40px] w-[140px] xl:h-[80px] xl:w-[288px]">
        <Image
          src="/images/careers/career/article/quote.png"
          alt="quote logo background"
          fill
          className="object-contain object-center"
        />
      </div>
    </div>
  );
}
