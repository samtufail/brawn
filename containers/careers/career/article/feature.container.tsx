import Image from 'next/image';

const data = {
  image: 'https://i.ibb.co/YB7by9H/Feature-text.png',
  heading: 'What is a pipeline welder?',
  description:
    'Pipeliners, also known as Pipeline Welders, join and repair tubular products and metallic pipe components and assemblies as part of the construction of buildings, vessels, structures, and stand-alone pipelines. Pipeline welders fix, build and repair pipelines in many different environments, like on a mountain or underwater.',
};

export function Feature() {
  return (
    <div className="mb-[52px] grid grid-cols-[320px_1fr] gap-[40px]">
      <div className="relative h-[331px] w-full">
        <Image src={data.image} alt="Image" fill className="object-cover object-center" />
      </div>
      <div className="max-w-[460px] py-[32px]">
        <h4 className="text-[32px] font-bold leading-[1.4] tracking-[-0.32px] text-neutral90">
          {data.heading}
        </h4>
        <p className="mt-[16px] flex self-stretch text-[18px] leading-normal text-neutral20">
          {data.description}
        </p>
      </div>
    </div>
  );
}
