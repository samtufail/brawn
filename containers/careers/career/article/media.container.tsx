import { Block } from '@/entities/articles.entity';
import clsx from 'clsx';

interface MediaProps extends Block {
  name: string;
  isBlog?: boolean;
}

export function MediaContainer({ ...rest }: MediaProps) {
  const altText = rest?.file?.data?.attributes?.alternativeText;
  const url = rest?.file?.data?.attributes?.formats?.medium?.url;
  const customAlt = `An image regarding ${rest.name}`;
  return url ? (
    <section className="mb-[32px] xl:mb-[52px]">
      <div className="relative h-[300px] w-full rounded-[8px] xl:h-[600px]">
        <img
          src={url}
          alt={altText || customAlt}
          loading="eager"
          className={clsx(
            'absolute top-0 h-full w-full rounded-[8px] object-center',
            rest.isBlog && 'object-contain',
            !rest.isBlog && 'object-cover',
          )}
        />
      </div>
    </section>
  ) : null;
}
