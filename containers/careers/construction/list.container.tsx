import { ReactElement } from 'react';
import Image from 'next/image';
import Link from 'next/link';
import { getCareers } from '@/api/careers';
import { Career } from '@/entities';
import { CashIcon, ClockIcon, StarIcon } from '@/icons';
import clsx from 'clsx';

import { numberWithCommas } from '@/lib/utils';

function Label({ type }: { type: string }) {
  return (
    <p
      className={clsx(
        { 'bg-[#8958fe]': type === 'white', 'bg-[#46b8ff]': type === 'blue' },
        'flex min-h-[24px] max-w-[90px] items-center justify-center rounded-[80px] p-[2px_8px] text-[12px] font-[500] leading-[1.5] text-white',
      )}
    >
      {type === 'white' ? 'White Collar' : 'Blue Collar'}
    </p>
  );
}

function IconBox({
  icon,
  text,
  ratingText,
}: {
  icon: ReactElement;
  text: string;
  ratingText?: string;
}) {
  return (
    <div className="flex items-center gap-2">
      <div>{icon}</div>
      <p className="text-[14px] leading-normal text-neutral90 xl:text-[16px]">
        {text}
        &nbsp;
        <span className="hidden xl:inline">{ratingText}</span>
      </p>
    </div>
  );
}

function JobCard({ career }: { career: Career }) {
  return (
    <Link prefetch={false} href={`/careers/${career.attributes.slug}`}>
      <div className="grid w-full max-w-[956px] grid-cols-1 items-center gap-[24px] xl:grid-cols-[412px_1fr] xl:gap-[60px]">
        <div className="relative h-[186px] w-[335px] rounded-[8px] xl:h-[302px] xl:w-[412px]">
          <Image
            src={career.attributes.thumbnail.data?.attributes.url}
            fill
            alt="Building Worker Pointing at Construction Crane"
            className="rounded-[8px] object-cover object-center"
          />
        </div>
        <div className="flex flex-col gap-[32px]">
          <div className="flex flex-col px-1">
            <Label type="blue" />
            <h3 className="mb-[6px] mt-[12px] text-[18px] font-bold leading-[1.4] tracking-[-0.2px] text-neutral90 xl:mt-[14px] xl:text-[20px]">
              {career.attributes.name}
            </h3>
            <p className="text-[12px] leading-[1.7] text-neutral70 xl:text-[14px]">
              {career.attributes.description}
            </p>
          </div>
          <div className="flex items-center justify-between gap-[14px]">
            <IconBox
              icon={<CashIcon />}
              text={`$${numberWithCommas(career.attributes.annualPay)}/yr`}
            />
            <IconBox icon={<ClockIcon />} text={`$${career.attributes.hourlyRate}/hr`} />
            <IconBox
              icon={<StarIcon />}
              text={`${career.attributes.rating}/5`}
              ratingText={`(${career.attributes.jobSatisfaction})`}
            />
          </div>
        </div>
      </div>
    </Link>
  );
}

export async function ListContainer() {
  const careers = await getCareers();
  return (
    <section className="global-container flex flex-col items-center px-[20px] py-[40px] xl:mb-[160px] xl:mt-[80px]">
      <div className="max-w-[621px] text-center">
        <h2 className="px-[20px] text-[32px] font-bold text-neutral90 xl:px-0 xl:text-[48px] xl:leading-[1.2] xl:tracking-[-0.48px]">
          List of Top&nbsp;
          {careers.length}
          &nbsp;Construction Jobs
        </h2>
        <p className="mt-[16px] text-[14px] leading-[1.7] text-neutral20 xl:text-[20px] xl:leading-[1.4]">
          We have compiled a list of the top&nbsp;
          {careers.length}
          &nbsp;construction careers in 2024. Each career has a dedicated page which helps answer
          questions like how to get started and how much do they make?
        </p>
      </div>
      <div className="mt-[40px]">
        {careers?.map((career, idx) => (
          <div key={career?.id}>
            <JobCard career={career} />
            {idx + 1 !== careers?.length ? <hr className="my-[40px] border-neutral30" /> : null}
          </div>
        ))}
      </div>
    </section>
  );
}
