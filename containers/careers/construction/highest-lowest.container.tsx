import Image from 'next/image';
import { getCareers } from '@/api/careers';
import { Button } from '@/components';
import { Career } from '@/entities';

import { nFormatter } from '@/lib/utils';

function Box({
  attributes: {
    name,
    description,
    hourlyRate,
    annualPay,
    thumbnail: {
      data: {
        attributes: { url },
      },
    },
  },
}: Career) {
  return (
    <div className="min-height-[357px] w-full rounded-[8px] bg-neutral60 p-[24px]">
      <div className="relative size-[64px]">
        <Image src={url} alt="logo-icon" fill className="object-cover object-center" />
      </div>
      <h3 className="mt-[40px] text-[18px] font-[600] leading-[1.4] tracking-[-0.2px] text-neutral90 xl:text-[20px]">
        {name}
      </h3>
      <p
        title={description}
        className="mt-[12px] line-clamp-3 text-[14px] leading-[1.7] text-neutral20 xl:text-[16px] xl:leading-normal"
      >
        {description}
      </p>
      <div className="mt-[28px] grid grid-cols-2">
        <div>
          <h4 className="text-[36px] font-[600] leading-[1.35] tracking-[-0.54px]">
            {`$${hourlyRate}`}
          </h4>
          <p className="mt-[4px] text-[14px] leading-[1.7] text-neutral70 xl:text-[16px] xl:leading-normal">
            per hour
          </p>
        </div>
        <div>
          <h4 className="text-[36px] font-[600] leading-[1.35] tracking-[-0.54px]">{`$${nFormatter(annualPay, 2)}`}</h4>
          <p className="mt-[4px] text-[16px] leading-normal text-neutral70">per year</p>
        </div>
      </div>
    </div>
  );
}

export async function HighestLowestContainer({ isHighest = false }: { isHighest: boolean }) {
  const listType = isHighest ? 'highest' : 'lowest';
  const careers = await getCareers(listType);
  return (
    <section className="global-container px-[20px] py-[40px] xl:mb-[160px] xl:p-0">
      <div className="mb-[25px] flex max-w-[621px] flex-col gap-[12px] xl:mb-[60px] xl:gap-[16px]">
        <h2 className="text-[32px] font-bold leading-[1.4] tracking-[-0.32px] text-neutral90 xl:text-[48px] xl:leading-[1.2] xl:tracking-[-0.48px]">
          {isHighest ? 'Highest' : 'Lowest'}
          &nbsp; paying construction jobs
        </h2>
        <p className="pr-[20px] text-[16px] leading-[1.4] text-neutral20 xl:pr-0 xl:text-[20px]">
          We have put together a list of the {listType} paying construction jobs.
        </p>
      </div>
      {/* Boxes */}
      <div className="grid grid-cols-1 gap-[24px] xl:grid-cols-3">
        {careers?.map((career, index) => {
          if (index < 3) {
            return <Box key={career.id} {...career} />;
          }
          return null;
        })}
      </div>
      {/* Button */}
      <div className="mt-[40px] flex justify-center">
        <Button>
          See &nbsp;
          {isHighest ? 'Highest' : 'Lowest'}
          &nbsp; Paying Jobs
        </Button>
      </div>
    </section>
  );
}
