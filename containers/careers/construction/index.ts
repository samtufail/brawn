import dynamic from 'next/dynamic';

export const HeaderContainer = dynamic(() =>
  import('./header.container').then((mod) => mod.HeaderContainer),
);
export const HighestLowestContainer = dynamic(() =>
  import('./highest-lowest.container').then((mod) => mod.HighestLowestContainer),
);
export const ListContainer = dynamic(() =>
  import('./list.container').then((mod) => mod.ListContainer),
);
export const RetireContainer = dynamic(() =>
  import('./retire.container').then((mod) => mod.RetireContainer),
);
export const ShareContainer = dynamic(() =>
  import('./share.container').then((mod) => mod.ShareContainer),
);
export const WhyContainer = dynamic(() =>
  import('./why.container').then((mod) => mod.WhyContainer),
);
