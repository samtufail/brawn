import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components';
import { CheckListIcon, DollarChartIcon, TargetIcon } from '@/icons';

const data = [
  {
    icon: <DollarChartIcon />,
    text: 'Maximize your earnings',
  },
  {
    icon: <TargetIcon />,
    text: 'Set your retirement goals',
  },
  {
    icon: <CheckListIcon />,
    text: 'Put your retirement plan into action',
  },
];

export function RetireContainer() {
  return (
    <section className="global-container px-[20px] py-[40px]">
      <div className="relative bg-[#ffede5] px-[18px] py-[24px]">
        <Image
          src="/images/careers/construction/brawn-lines.png"
          alt="logo background"
          width={200}
          height={97}
          className="absolute right-0 top-0 hidden xl:inline-block"
        />
        <Image
          src="/images/careers/construction/brawn-lines-mobile.png"
          alt="logo background"
          width={50}
          height={43}
          className="absolute right-0 top-0 xl:hidden"
        />
        <div className="max-w-[928px mx-auto flex flex-col items-center justify-center gap-[20px] xl:flex-row xl:gap-[80px]">
          <div className="relative h-[356px] w-full xl:h-[578px] xl:w-[484px]">
            <Image
              src="/images/careers/construction/retire.png"
              alt="Do you want to Retire Early from a job in construction?"
              fill
              className="object-contain object-center"
            />
          </div>
          <div className="max-w-[363px]">
            <h2 className="text-[24px] font-bold leading-[1.4] tracking-[0.24px] text-neutral90 xl:text-[36px] xl:leading-[1.2] xl:tracking-[-0.36px]">
              Do you want to Retire Early from a job in construction?
            </h2>
            <p className="mt-[24px] text-[14px] font-bold leading-normal text-neutral20 xl:mt-[32px] xl:text-[16px]">
              Brawn can help to you make it real
            </p>
            <div className="mt-[20px] flex flex-col gap-[12px]">
              {data.map((el) => (
                <div key={el.text} className="flex items-center gap-[18px]">
                  {el.icon}
                  <p className="text-[14px] leading-normal text-[#121212] xl:text-[16px]">
                    {el.text}
                  </p>
                </div>
              ))}
            </div>
            <Link href="https://brawnhq.typeform.com/to/dGqJYsmZ?product_slug=retireearly">
              <Button className="mt-[32px] w-full">Show more</Button>
            </Link>
          </div>
        </div>
      </div>
    </section>
  );
}
