import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components';
import { commonData } from '@/data/common';

const { waitlistLink, waitlistButtonText } = commonData;

export function HeaderContainer() {
  return (
    <header className="relative min-h-[630px] bg-[url(/images/careers/construction/bg.jpg)] bg-cover bg-center bg-no-repeat pt-[85.4px] text-white">
      <div className="absolute bottom-0 left-0 h-[63px] w-[403px]">
        <Image
          src="/images/careers/construction/bl.png"
          alt="brawn logo elements for enhancing background look."
          fill
          style={{ objectFit: 'contain' }}
        />
      </div>
      <div className="absolute right-0 top-1/2 hidden h-[356px] w-[92px] -translate-y-1/2 xl:block">
        <Image
          src="/images/careers/construction/mr.png"
          alt="brawn logo elements for enhancing background look."
          fill
          style={{ objectFit: 'contain' }}
        />
      </div>
      <div className="global-container px-[20px] xl:px-0">
        <div className="max-w-[426px]">
          <h1 className="mt-[112px] text-[48px] font-[800] leading-[1.1] tracking-[-0.56px] xl:mt-[66.6px] xl:text-[56px]">
            Construction Careers
          </h1>
          <p className="mt-[12px] text-[16px] leading-normal">
            Curious about a career in Construction. We have put together a list of the top
            construction careers in 2024.
          </p>
          <Link href={waitlistLink} target="_blank" className="visited:outline-none">
            <Button className="mb-[169px] mt-[40px] xl:mb-0">{waitlistButtonText}</Button>
          </Link>
        </div>
      </div>
    </header>
  );
}
