import { getCareer } from '@/api/careers';
import { HelpExplainerContainer, ShareButtons } from '@/containers/common';

const keyPoints = [
  {
    title: 'Build a financial safety net',
    description:
      "We'll help you build a financial safety net that you can use for emergencies. If you're laid off, or have a family emergency you can “tap” your financial safety net.",
    image: '/images/home/help/help-1.png',
  },
  {
    title: 'Make smarter purchase decisions',
    description:
      "Our Smart Purchase tool, will allow you to ask us questions about your large purchase decisions. We'll let you know if it makes financial sense for you. We'll keep you in check and on track to meet your financial goals.",
    image: '/images/home/help/help-2.png',
  },
  {
    title: 'Earn great cash back',
    description:
      "Get up to 3% Cashback when you use your Brawn debit card at select brands. We've partnered with some amazing brands that will help you save money on everyday purchases.",
    image: '/images/home/help/help-3.png',
  },
];

export async function WhyContainer({ slug }: { slug: string }) {
  const career = await getCareer(slug);
  return (
    <section className="global-container flex flex-col items-center px-[20px] py-[40px] xl:my-[80px] xl:p-0 xl:px-[30px]">
      <div className="mb-[60px] flex w-full justify-between">
        <div className="hidden w-[24px] xl:flex">
          <div className="absolute">
            <ShareButtons description={career.attributes.description} />
          </div>
        </div>
        <div className="max-w-[621px] text-left xl:text-center">
          <h2 className="text-[32px] font-bold leading-[1.2] tracking-[-0.48px] text-neutral90 xl:text-[48px]">
            Why start a career in construction?
          </h2>
          <p className="mt-[12px] text-[20px] leading-[1.4] text-neutral20 xl:mt-[16px]">
            Quickly design and customize responsive mobile-first sites with Bootstrap, the world's
            most popular front-end open source toolkit, featuring Sass variables.
          </p>
        </div>
        <div />
      </div>
      <HelpExplainerContainer points={keyPoints} />
    </section>
  );
}
