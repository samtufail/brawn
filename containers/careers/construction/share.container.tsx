import { FacebookIcon, LinkedinIcon, TwitterIcon } from '@/icons';

export function ShareContainer() {
  return (
    <div className="mb-[40px] flex items-center justify-center xl:my-[92px]">
      <div className="flex items-center gap-[24px]">
        <p className="text-[16px] leading-normal text-neutral20">Share</p>
        <div className="flex items-center gap-[16px]">
          <FacebookIcon />
          <LinkedinIcon />
          <TwitterIcon />
        </div>
      </div>
    </div>
  );
}
