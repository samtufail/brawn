import Image from 'next/image';
import { Thumbnail } from '@/entities/careers.entity';

export async function Header({ cover, name }: { cover: Thumbnail; name: string }) {
  return (
    <header className="min-h-[420px] bg-[url(/images/blog-article/bg.png)] bg-cover bg-center bg-no-repeat pb-[28px] pt-[12px] xl:min-h-[872px] xl:pb-0 xl:pt-[80px]">
      <div className="global-container px-[20px] xl:px-[60px]">
        <div className="relative min-h-[420px] rounded-[8px] xl:min-h-[656px]">
          <Image
            alt={`An Image representing ${name}.`}
            src={cover.data.attributes.formats?.large?.url || ''}
            fill
            className="rounded-[8px] object-cover object-center"
          />
        </div>
      </div>
    </header>
  );
}
