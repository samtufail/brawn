import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components';

export async function ListContainer() {
  return (
    <section className="global-container flex flex-col items-center px-[20px] py-[40px] xl:mb-[160px] xl:mt-[80px]">
      <div className="max-w-[621px] text-center">
        <h2 className="px-[20px] text-[32px] font-bold text-neutral90 xl:px-0 xl:text-[48px] xl:leading-[1.2] xl:tracking-[-0.48px]">
          Construction Calculators
        </h2>
        <p className="mt-[16px] text-[14px] leading-[1.7] text-neutral20 xl:text-[20px] xl:leading-[1.4]">
          Below is a list of FREE construction calculators.
        </p>
      </div>
      <div className="mt-[60px]">
        <div className="grid w-full max-w-[956px] grid-cols-1 items-center gap-[24px] xl:grid-cols-[412px_1fr] xl:gap-[60px]">
          <div className="relative h-[186px] w-[335px] rounded-[8px] xl:h-[302px] xl:w-[412px]">
            <Image
              src="/images/calculators/brick/card.jpg"
              fill
              alt="Building Worker Laying Bricks for Building a wall."
              className="rounded-[8px] object-cover object-center"
            />
          </div>
          <div className="flex flex-col gap-[32px]">
            <div className="flex flex-col px-1">
              <h3 className="mb-[6px] mt-[12px] text-[18px] font-bold leading-[1.4] tracking-[-0.2px] text-neutral90 xl:mt-[14px] xl:text-[20px]">
                Brick Calculator
              </h3>
              <p className="mb-[12px] text-[12px] leading-[1.7] text-neutral70 xl:text-[14px]">
                our Free Brick Calculator tool, designed to help you accurately estimate the number
                of bricks and bags of mortar needed for your construction project..
              </p>
              <Link href="/calculators/brick">
                <Button>Use Brick Calculator</Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
