import Image from 'next/image';

export function HeaderContainer() {
  return (
    <header className="relative min-h-[630px] bg-black bg-[url(/images/calculators/main-bg.jpg)] bg-cover bg-center bg-no-repeat pt-[85.4px] text-white">
      <div className="absolute bottom-0 left-0 h-[63px] w-[403px]">
        <Image
          src="/images/careers/construction/bl.png"
          alt="brawn logo elements for enhancing background look."
          fill
          style={{ objectFit: 'contain' }}
        />
      </div>
      <div className="absolute right-0 top-1/2 hidden h-[356px] w-[92px] -translate-y-1/2 xl:block">
        <Image
          src="/images/careers/construction/mr.png"
          alt="brawn logo elements for enhancing background look."
          fill
          style={{ objectFit: 'contain' }}
        />
      </div>
      <div className="global-container px-[20px] xl:px-0">
        <div className="max-w-[426px]">
          <h1 className="mt-[112px] text-[48px] font-[800] leading-[1.1] tracking-[-0.56px] xl:mt-[66.6px] xl:text-[56px]">
            Construction Calculators
          </h1>
          <p className="mt-[12px] text-[16px] leading-normal">
            We have assembled a list of FREE construction calculators.
          </p>
        </div>
      </div>
    </header>
  );
}
