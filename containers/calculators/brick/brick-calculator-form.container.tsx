'use client';

import { Fragment, useState } from 'react';
import { Button } from '@/components';
import {
  brickCalculatorData,
  estimates,
  initialPrices,
  initialQuantities,
  stepsGenerator,
} from '@/data/calculators';
import { Prices, Quantities } from '@/entities';
import { BrickCalculatorSchema } from '@/schemas';
import { Form, Formik } from 'formik';

import { brickCalculator, initialBrickCalculatorValues } from '@/lib/calculators';

export function BrickCalculatorForm() {
  const [quantities, setQuantities] = useState<Quantities>(initialQuantities);
  const [prices, setPrices] = useState<Prices>(initialPrices);
  const finalEstimates = estimates(quantities, prices);
  return (
    <Formik
      initialValues={initialBrickCalculatorValues}
      validationSchema={BrickCalculatorSchema}
      onSubmit={(values) => {
        const {
          numberOfBricksNeeded,
          numberOfBags,
          sandCbFt,
          bricksPrice,
          mortarBagsPrice,
          sandPrice,
        } = brickCalculator(values);
        // Set quantitiesٍ
        setQuantities({ numberOfBricksNeeded, numberOfBags, sandCbFt });
        // Set prices
        setPrices({ bricksPrice, mortarBagsPrice, sandPrice });
      }}
    >
      {({ isValid, values }) => (
        <Form className="flex flex-col gap-[20px] py-[30px]">
          {brickCalculatorData.steps.map((step) => (
            <Fragment key={step.title}>{stepsGenerator(step)}</Fragment>
          ))}
          <div className="flex items-center gap-[12px]">
            <Button className="max-w-[200px]" type="submit">
              {brickCalculatorData.submitButtonText}
            </Button>
            <Button className="max-w-[250px]" type="reset" variant="secondary">
              {brickCalculatorData.resetButtonText}
            </Button>
          </div>
          {!isValid && <div className="error mt-[-10px]">{brickCalculatorData.errorMessage}</div>}
          <div className="mb-[24px]">
            <h2 className="text-2xl font-bold">{brickCalculatorData.recommendationTitle}</h2>
            <p className="mb-[30px]">{brickCalculatorData.recommendationDescription}</p>
            {finalEstimates?.length &&
              values.wallHeight > 0 &&
              finalEstimates.map((estimate) => (
                <div key={estimate.title} className="mb-[24px]">
                  <h3 className="mb-[24px] text-xl font-bold">{estimate.title}</h3>
                  <div className="grid grid-cols-[300px_1fr] gap-[20px]">
                    <div />
                    <div className="font-semibold">{estimate.table.title}</div>
                    {estimate.table.rows.map((row) => (
                      <>
                        <div>{row.label}</div>
                        <div>{row.value}</div>
                      </>
                    ))}
                  </div>
                </div>
              ))}
          </div>
        </Form>
      )}
    </Formik>
  );
}
