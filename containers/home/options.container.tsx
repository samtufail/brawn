import { OptionCard } from '@/components';
import { optionsData } from '@/data/home';

const { title, options } = optionsData;

export function OptionsContainer() {
  return (
    <section>
      {/* Option Cards */}
      <div className="global-container px-[30px] py-[40px] md:mt-[48px] md:p-0">
        {/* Heading */}
        <h2 className="text-center text-[18px] font-[600] leading-[1.4] tracking-[-0.24px] md:text-[24px]">
          {title}
        </h2>
        {/* Cards */}
        <div className="mt-[24px] grid items-center gap-[16px] px-[20px] md:grid-cols-3 xl:px-0">
          {options?.map((option) => (
            <OptionCard
              key={option.title}
              image={option.image}
              title={option.title}
              alt={`An Icon Representing ${option.title}`}
            />
          ))}
        </div>
        <div className="mt-9 flex justify-center">
          <span className="rounded-[32px] border border-neutral50 px-4 py-[11px] text-center text-lg leading-[1.4]">
            And many more
          </span>
        </div>
      </div>
    </section>
  );
}
