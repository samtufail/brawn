import dynamic from 'next/dynamic';

import { AdHeaderContainer } from './ad-header.container';

export const NewsLetterContainer = dynamic(() =>
  import('../common/news-letter.container').then((mod) => mod.NewsLetterContainer),
);
export const FeaturesContainer = dynamic(() =>
  import('./features.container').then((mod) => mod.FeaturesContainer),
);
export const HeaderContainer = dynamic(() =>
  import('./header.container').then((mod) => mod.HeaderContainer),
);
export const HelpContainer = dynamic(() =>
  import('./help.container').then((mod) => mod.HelpContainer),
);
export const MainFeatureContainer = dynamic(() =>
  import('./main-feature.container').then((mod) => mod.MainFeatureContainer),
);
export const OptionsContainer = dynamic(() =>
  import('./options.container').then((mod) => mod.OptionsContainer),
);
export const TeamContainer = dynamic(() =>
  import('./team.container').then((mod) => mod.TeamContainer),
);

export { AdHeaderContainer };
