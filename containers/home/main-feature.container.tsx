import Image from 'next/image';
import { mainFeatureData } from '@/data/home';

const { title, description, mainImage, arrowImage } = mainFeatureData;

export function MainFeatureContainer() {
  return (
    <section className="global-container relative grid auto-cols-[1fr] grid-rows-[auto] gap-[35px] px-[30px] py-[40px] md:grid-cols-[max-content_1fr] xl:mt-[172px] xl:gap-x-[98px] xl:px-[80px] xl:py-0">
      {/* Image */}
      <div className="relative h-[419px] w-[94%] md:h-[560px] md:w-[440px]">
        <Image
          src={mainImage}
          alt="Two blue collar workers talking with each other while working."
          fill
          sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
          style={{ objectFit: 'contain', objectPosition: 'right' }}
        />
      </div>
      {/* Text */}
      <div className="max-w-[486px]">
        <h1 className="mt-[20px] text-[18px] font-[600] leading-[1.35] tracking-[-0.54px] text-neutral90 md:mt-[83px] md:text-[36px]">
          {title}
        </h1>
        <p className="mt-[16px] text-[16px] leading-normal text-neutral70 md:text-[18px]">
          {description}
        </p>
      </div>
      {/* Arrows */}
      <div className="absolute bottom-[40px] right-[30px] size-[30px] md:right-[80px] md:size-[90px] md:w-[154.09px] xl:h-[134.54px]">
        <Image
          src={arrowImage}
          alt="A placeholder logo icon to match branding."
          fill
          sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
          style={{ objectFit: 'contain', objectPosition: 'right' }}
        />
      </div>
    </section>
  );
}
