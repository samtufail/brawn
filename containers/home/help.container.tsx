import { helpData } from '@/data/home';

import { HelpExplainerContainer } from '../common';

const { title, keyPoints } = helpData;

export function HelpContainer() {
  return (
    <section className="global-container flex flex-col items-center px-[30px] py-[40px] md:mt-[172px] xl:p-0">
      <h2 className="mb-[72px] text-center text-[26px] font-semibold leading-[1.35] tracking-[-0.54px] md:text-[36px]">
        {title}
      </h2>

      <HelpExplainerContainer points={keyPoints} />
    </section>
  );
}
