import Image from 'next/image';
import { teamData } from '@/data/home';

const { title, companies } = teamData;

export function TeamContainer() {
  return (
    <section className="global-container px-[30px] py-[40px] xl:mt-[72px] xl:px-[152px] xl:py-0">
      {/* Heading */}
      <h2 className="text-center text-[18px] font-[600] leading-[1.4] tracking-[-0.2px] text-neutral90 xl:text-[20px]">
        {title}
      </h2>
      {/* Icons */}
      <div className="mt-[21px] grid grid-cols-1 items-center justify-center gap-[24px] px-0 md:grid-cols-2 md:px-5 xl:grid-cols-4 xl:px-0">
        {companies?.map(({ width, height, name, image }) => (
          <div
            key={name}
            className="flex min-h-[86px] min-w-[220px] items-center justify-center rounded-lg border border-solid border-neutral30 xl:min-h-[104px] xl:min-w-[unset]"
          >
            <div className="relative flex w-full items-center justify-center">
              <Image src={image} alt={`Logo of ${name}`} width={width} height={height} />
            </div>
          </div>
        ))}
      </div>
    </section>
  );
}
