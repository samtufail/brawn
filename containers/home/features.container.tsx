import Image from 'next/image';
import { featuresData } from '@/data/home';
import clsx from 'clsx';

const { title, description, images } = featuresData;

function Images() {
  return (
    <div
      className={clsx(
        'global-container space-between mt-[40px] flex w-full flex-col gap-[24px] md:mt-[-186px] md:flex-row md:justify-center',
      )}
    >
      {images?.map((el, index) => (
        <div
          key={el}
          className={clsx('relative h-[300px] w-full md:h-[516px]', {
            'md:w-[30%] xl:w-[35%]': index === 0,
            'md:w-[30%] xl:w-[37%]': index === 1,
            'md:w-[30%] xl:w-[50%]': index === 2,
          })}
        >
          <Image
            fill
            style={{ objectFit: 'cover' }}
            sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
            src={el}
            alt="Construction image representing who we help."
          />
        </div>
      ))}
    </div>
  );
}

export function FeaturesContainer() {
  return (
    <section>
      <div className="relative flex min-h-[525px] w-full flex-col bg-primary20 bg-[url(/images/home/features/features-br.png),_url(/images/home/features/features-tl.png)] bg-[length:110px,110px] bg-[position:100%_106%,-5%_-6%] bg-no-repeat px-[30px] py-[40px] text-white md:bg-[length:auto,auto] md:bg-[position:100%_136%,0%_-40%] md:px-0 md:pb-0 md:pt-[80px]">
        <div className="global-container">
          <div className="flex flex-col items-center gap-4 text-center">
            <h2 className="max-w-[652px] text-[26px] font-[600] leading-[1.35] tracking-[-0.54px] text-white md:text-[36px]">
              {title}
            </h2>
            <p className="max-w-[692px] text-[16px] text-neutral50 md:text-[18px]">{description}</p>
          </div>
        </div>
        <div className="block md:hidden">
          <Images />
        </div>
      </div>
      <div className="hidden md:block">
        <Images />
      </div>
    </section>
  );
}
