import Image from 'next/image';
import Link from 'next/link';
import { Button, HeroPoint } from '@/components';
import { commonData } from '@/data/common';
import { headerData } from '@/data/home';
import clsx from 'clsx';

const { title, description, points } = headerData;
const { waitlistButtonText, waitlistLink } = commonData;

export function HeaderContainer() {
  return (
    <header className="global-container flex flex-col items-center gap-[25px] px-[30px] py-[40px] xl:grid xl:grid-cols-[767px_1fr] xl:items-start xl:py-[63px] xl:pr-0">
      <div>
        {/* Heading */}
        <div className="max-w-[698px]">
          <h1 className="text-[32px] font-bold leading-[42px] tracking-[-.48px] text-[#121212] md:mt-[49px] md:text-[56px] md:leading-tight md:tracking-[-1.68px]">
            {title}
          </h1>
        </div>
        <p className="mt-[16px] text-[16px] leading-[1.4] text-[#393c47] md:text-[28px]">
          {description}
        </p>
        <ul className="mt-4 md:text-[20px]">
          <li>- Personalized to your needs</li>
          <li>- Completely free to sign up</li>
          <li>- No account minimums</li>
        </ul>
        <Link href={waitlistLink} target="_blank" className="visited:outline-none">
          <Button className="mt-[32px]">{waitlistButtonText}</Button>
        </Link>
      </div>
      {/* Picture */}
      <div className="relative flex h-[400px] w-full justify-center overflow-visible md:h-[591px] md:w-[420px]">
        {points.map((el) => (
          <HeroPoint
            key={el.title}
            title={el.title}
            icon={<el.icon />}
            className={clsx({
              'absolute right-[-4%] top-[49px] md:right-[-78px]': el.variant === 'primary',
              'absolute bottom-[200px] left-[-5%] md:bottom-[271px] md:left-[-99px]':
                el.variant === 'secondary',
              'absolute bottom-[40px] left-[40px]': el.variant === 'trinity',
            })}
            variant={el.variant}
          />
        ))}
        <Image
          fill
          sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
          style={{ objectFit: 'cover', zIndex: -1 }}
          src="/images/home/header/header-image.jpg"
          alt="Construction image representing who we help."
          priority
        />
      </div>
    </header>
  );
}
