import { Table, TableBody, TableCell, TableHead, TableHeader, TableRow } from '@/components';
import { CheckIcon, LogoIcon } from '@/icons';

const tableColumns = [
  {
    id: 0,
    data: '',
  },
  {
    id: 1,
    data: <LogoIcon />,
  },
  {
    id: 2,
    data: 'Fidelity',
  },
  {
    id: 3,
    data: 'Charles Schwab',
  },
  {
    id: 4,
    data: 'Vanguard',
  },
];

const mobileColumns = [
  {
    id: 0,
    data: '',
  },
  {
    id: 1,
    data: <LogoIcon width={80} />,
  },
  {
    id: 2,
    data: 'Other 401(K) providers',
  },
];

const tableData = [
  { id: 0, data: 'Real-Time AI Guidance', available: ['brawn'] },
  {
    id: 1,
    data: 'Investments Tracking & Brokerage',
    available: ['brawn', 'fidelity', 'charles', 'vanguard'],
  },
  { id: 2, data: 'Networth tracking and budgeting', available: ['brawn'] },
  { id: 3, data: 'Financial planner & roadmap', available: ['brawn'] },
  { id: 4, data: 'Smart Savings Account (4% APY)', available: ['brawn'] },
  { id: 5, data: 'Cash back rewards', available: ['brawn'] },
  { id: 6, data: 'On demand learning', available: ['brawn'] },
];

function DesktopTable() {
  return (
    <Table className="hidden border border-solid border-neutral60 xl:table">
      <TableHeader>
        <TableRow>
          {tableColumns.map((column) => (
            <TableHead key={column.id}>{column.data || ''}</TableHead>
          ))}
        </TableRow>
      </TableHeader>
      <TableBody>
        {tableData.map(({ id, data, available }) => (
          <TableRow key={id}>
            <TableCell>{data}</TableCell>
            <TableCell>{available.includes('brawn') ? <CheckIcon /> : null}</TableCell>
            <TableCell>{available.includes('fidelity') ? <CheckIcon /> : null}</TableCell>
            <TableCell>{available.includes('charles') ? <CheckIcon /> : null}</TableCell>
            <TableCell>{available.includes('vanguard') ? <CheckIcon /> : null}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}

function MobileTable() {
  return (
    <Table className="border border-solid border-neutral60 xl:hidden">
      <TableHeader>
        <TableRow>
          {mobileColumns.map((column) => (
            <TableHead className="px-[10px]" key={column.id}>
              {column.data || ''}
            </TableHead>
          ))}
        </TableRow>
      </TableHeader>
      <TableBody>
        {tableData.map(({ id, data, available }) => (
          <TableRow key={id}>
            <TableCell className="text-wrap">{data}</TableCell>
            <TableCell>{available.includes('brawn') ? <CheckIcon size={20} /> : null}</TableCell>
            <TableCell>
              {available.includes('fidelity' || 'charles' || 'vanguard') ? (
                <CheckIcon size={20} />
              ) : null}
            </TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}

export function SolutionContainer() {
  return (
    <section className="global-container px-[20px] py-[40px] xl:px-0 xl:py-[80px]">
      <h2 className="mx-auto mb-[16px] max-w-[666px] text-center text-[32px] font-bold leading-[1.16] tracking-[-0.32px] text-neutral90 xl:mb-5 xl:text-[36px] xl:leading-[1.2] xl:tracking-[-0.36px]">
        A great solution even if you're already offering retirement benefits
      </h2>
      <p className="mx-auto mb-[16px] max-w-[794px] text-[16px] text-neutral20 xl:text-center">
        Brawn is still very useful, even if you are already offering a 401(k) or other retirement
        benefit. Traditional benefits providers only scratch the surface of the pain points
        construction workers face.
      </p>
      <p className="mx-auto mb-[32px] max-w-[794px] text-[16px] text-neutral20 xl:mb-[28px] xl:text-center">
        We are a dedicated solution for construction workers. We know the challenges they face
        day-to-day and our solutions are personalized to address their needs.
      </p>
      <div className="mb-[32px] grid gap-4 xl:mb-[60px] xl:grid-cols-2 xl:gap-5">
        <div className="flex flex-col gap-[25px] rounded-lg border border-solid border-neutral60 px-[20px] py-[24px] xl:min-h-[88px] xl:flex-row xl:items-center xl:px-[26px] xl:py-0">
          <CheckIcon />
          <h3 className="text-[18px] font-[600] leading-[1.4] tracking-[-0.18px] text-neutral90 xl:text-[20px] xl:tracking-[-0.2px]">
            You don't need to switch 401(k) providers
          </h3>
        </div>
        <div className="flex flex-col gap-[25px] rounded-lg border border-solid border-neutral60 px-[20px] py-[24px] xl:min-h-[88px] xl:flex-row xl:items-center xl:px-[26px] xl:py-0">
          <CheckIcon />
          <h3 className="text-[18px] font-[600] leading-[1.4] tracking-[-0.18px] text-neutral90 xl:text-[20px] xl:tracking-[-0.2px]">
            The one money membership that does it all
          </h3>
        </div>
      </div>
      <DesktopTable />
      <MobileTable />
    </section>
  );
}
