import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components';
import { commonData } from '@/data/common';
import { BarsIcon, StatusUpIcon } from '@/icons';

function FeaturePercentage({
  percentage,
  description,
  icon,
}: {
  percentage: string;
  description: string;
  icon: React.ReactElement;
}) {
  return (
    <div className="mb-[24px] xl:mb-[46px]">
      <div className="mb-[8px] grid grid-cols-[32px_1fr] items-center gap-[20px]">
        {icon}
        <h3 className="text-[36px] font-bold leading-[1.2] tracking-[-0.36px] text-neutral90">
          {percentage}
        </h3>
      </div>
      <p className="max-w-[280px] text-[18px] text-neutral20 xl:max-w-[unset] xl:text-[20px] xl:leading-[1.4]">
        {description}
      </p>
    </div>
  );
}

export function Header() {
  return (
    <header className="global-container mt-[12px] flex flex-col-reverse px-[20px] xl:mt-[60px] xl:grid xl:grid-cols-2 xl:gap-[60px] xl:px-0">
      <div className="mt-[28px] flex flex-col xl:mt-0">
        <h1 className="mb-[12px] text-[48px] font-bold leading-tight tracking-[-1.68px] text-neutral90 xl:mb-4 xl:text-[56px]">
          Retain your workers and attract new talent
        </h1>
        <p className="mb-[40px] max-w-[561px] text-[16px] leading-[1.4] text-neutral20 xl:mb-[32px] xl:text-[20px]">
          Brawn makes it easy for you to retain your workforce, and attract new talent, by keeping
          them happy and financially secure.
        </p>
        <Link href={commonData.employerLink} target="_blank" className="visited:outline-none">
          <Button className="mb-[50px] xl:mb-[60px] xl:max-w-[177px]">Get Started</Button>
        </Link>
        <FeaturePercentage
          percentage="40%"
          description="Improve employee happiness by 40%"
          icon={<BarsIcon />}
        />
        <FeaturePercentage
          percentage="30%"
          description="Increase retention by 30%"
          icon={<StatusUpIcon />}
        />
      </div>
      <div className="relative h-[420px] w-full xl:h-[600px]">
        <Image
          src="/images/employer/header.jpg"
          alt="header image for employers page"
          fill
          className="object-contain object-right"
        />
      </div>
    </header>
  );
}
