import Image from 'next/image';

const data = [
  {
    title: 'Financial stress',
    description:
      'Financial stress is the #1 stress in U.S. It ranks higher than job stress, health stress and relationship combined.',
  },
  {
    title: 'Stressed employees',
    description: 'Financially stressed employees are 2.2 times more likely to quit your company.',
  },
  {
    title: 'Worrying about finances',
    description: 'Your workers are spending 3+ hours per week worrying about their finances.',
  },
];

function CareCard({
  title,
  description,
  index,
}: {
  title: string;
  description: string;
  index: number;
}) {
  return (
    <div className="flex min-h-[609px] flex-col justify-between rounded-lg bg-neutral60 px-[18px] py-[24px] xl:min-h-[651px] xl:px-[26px] xl:py-[32px]">
      <div>
        {/* Text Circle */}
        <div className="mb-[32px] size-[40px] rounded-[80px] bg-primary20 px-[12px] py-[8px] text-white">
          {`0${index}`}
        </div>
        <h4 className="mb-[12px] text-[20px] font-[600] leading-[1.4] tracking-[-0.2px] text-neutral90">
          {title}
        </h4>
        <p className="mb-[32px] text-[18px] leading-normal text-neutral90">{description}</p>
      </div>
      {/* Image */}
      <div className="relative h-[309px] w-full xl:h-[362px]">
        <Image
          src={`/images/employer/care-${index}.png`}
          alt="An Image Representing Financial Stress"
          fill
          className="object-contain object-center"
        />
      </div>
    </div>
  );
}

export function CareContainer() {
  return (
    <section className="global-container px-[20px] py-[40px] xl:px-0 xl:py-[80px]">
      <div className="mb-[32px] flex flex-col items-start justify-between xl:mb-[60px] xl:flex-row">
        <h2 className="mb-[32px] max-w-[499px] text-[36px] font-bold leading-[1.2] tracking-[-0.36px] text-neutral90 xl:mb-0">
          Why should you care about your employees' financial health?
        </h2>
        <div className="max-w-[569px] xl:my-[18.5px]">
          <h3 className="text-[20px] font-bold leading-[1.4] tracking-[-0.2px] text-neutral90">
            A financially secure worker — is a happy worker!
          </h3>
          <p className="mt-[16px] text-[16px] leading-normal text-neutral20">
            They will sleep better, have improved mental health, and perform better on the job.
          </p>
        </div>
      </div>
      <div className="grid gap-[20px] xl:grid-cols-3">
        {data?.map((el, index) => <CareCard key={el.title} {...el} index={index + 1} />)}
      </div>
    </section>
  );
}
