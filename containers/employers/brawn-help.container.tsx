import { MessageIcon } from '@/icons/message.icon';
import { PuzzleIcon } from '@/icons/puzzle.icon';
import { TrustIcon } from '@/icons/trust.icon';
import { WalletIcon } from '@/icons/wallet.icon';
import clsx from 'clsx';

const data = [
  {
    icon: <MessageIcon />,
    text: 'Get expert advice on their financial situation',
    description:
      'Your workers will get their financial questions answered from an expert financial advisor. We provide them with personalized advice to improve their financial situation.',
  },
  {
    icon: <WalletIcon />,
    text: 'Earn cashback when spending smartly',
    description:
      'Your workers can earn up to 6% in cashback when they use their existing debit card or credit card at one of the many merchants we partner',
  },
  {
    icon: <TrustIcon />,
    text: 'Save smarter & build a safety net',
    description:
      'Your workers will get access to our smart saving account. Which will help them put more dollars away for hard times.',
  },
  {
    icon: <PuzzleIcon />,
    text: 'Build a retirement plan that works for them',
    description:
      'Your workers will get access to expert retirement planning advice and advance investing solutions which will give them confidence about their retirement.',
  },
];

function BrawnHelpCard({
  icon,
  text,
  description,
  index,
}: {
  icon: React.ReactElement;
  text: string;
  description: string;
  index: number;
}) {
  return (
    <div className="flex flex-col justify-between rounded-[4px] border border-solid border-[#e0e2eb] px-[20px] py-[24px] xl:flex-row xl:items-center xl:px-[32px] xl:py-[40px]">
      <div className="flex flex-col gap-[40px] xl:flex-row xl:gap-[24px]">
        {icon}
        <h3
          className={clsx(
            'mb-[16px] text-[18px] font-[600] leading-[1.4] tracking-[-0.2px] text-neutral90 xl:mb-0 xl:text-[20px]',
            {
              'max-w-[326px]': index === 0,
              'max-w-[293px]': index === 1,
              'max-w-[223px]': index === 2,
              'max-w-[258px]': index === 3,
            },
          )}
        >
          {text}
        </h3>
      </div>
      <p className="max-w-[696px] text-[14px] leading-normal text-neutral90 xl:text-[16px]">
        {description}
      </p>
    </div>
  );
}

export function BrawnHelpContainer() {
  return (
    <section className="global-container px-[20px] py-[40px] xl:px-0 xl:py-[80px]">
      <div className="mb-[32px] flex flex-col items-center xl:mb-[60px]">
        <h2 className="mb-[16px] max-w-[825px] text-[32px] font-bold leading-[1.16] tracking-[-0.32px] text-neutral90 xl:mb-[20px] xl:text-center xl:text-[36px] xl:leading-[1.2] xl:tracking-[-0.36px]">
          How Brawn can help you and your workforce?
        </h2>
        <p className="max-w-[749px] text-[16px] leading-normal text-neutral20 xl:text-center">
          We care deeply about improving the lives of construction workers and tradesmen and women.
          That&apos;s why we are building the number one personal finance app for blue collar
          workers. We&apos;ll help your workers save more, spend smarter and invest in their future.
          They&apos;ll get access to world class financial tools and dedicated financial coaching.
        </p>
      </div>
      <div className="mb-[32px] xl:mb-[60px]">
        <h2 className="mb-[18px] text-[24px] font-bold leading-[1.4] tracking-[-0.24px] text-neutral90 xl:mb-[32px]">
          Ways we improve the lives of your employees
        </h2>
        <div className="flex flex-col gap-[16px]">
          {data.map((el, index) => (
            <BrawnHelpCard key={el.text} {...el} index={index} />
          ))}
        </div>
      </div>
    </section>
  );
}
