import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components';
import { commonData } from '@/data/common';

export function UpsellContainer() {
  return (
    <section className="global-container px-[20px] py-[40px] xl:px-0 xl:py-[80px]">
      <div className="mb-[40px] flex flex-col justify-between xl:mb-[60px] xl:flex-row">
        <h2 className="mb-[16px] text-[32px] font-bold leading-[1.16] tracking-[-0.32px] text-neutral90 xl:mb-0 xl:text-[36px] xl:leading-[1.2] xl:tracking-[-0.36px]">
          Onboard within minutes
        </h2>
        <div className="max-w-[689px]">
          <p className="mb-[8px] text-[16px] text-neutral20 xl:mb-[16px]">
            We're not like those old, clunky benefit providers. Brawn is easy to set up. It only
            takes a few minutes to get your team integrated.
          </p>
          <p className="text-[16px] text-neutral20">
            Give us a try. Your employees will love you for it!
          </p>
        </div>
      </div>
      <div className="relative grid overflow-hidden rounded-[4px] bg-[#ffede5] px-[18px] py-[24px] xl:grid-cols-[484px_1fr] xl:gap-[80px] xl:py-0 xl:pl-[170px] xl:pr-[82px]">
        <Image
          src="/images/careers/construction/brawn-lines.png"
          alt="logo background"
          width={150}
          height={97}
          className="absolute right-0 top-[-30px] hidden h-[120px] xl:inline-block"
        />
        <div className="relative h-[356px] w-full xl:h-[577px]">
          <Image
            src="/images/employer/up-sell.png"
            alt="An Image of construction worker with a smile"
            fill
            className="object-cover object-center"
          />
        </div>
        <div className="pt-[20px] xl:pt-[146px]">
          <h3 className="mb-[20px] text-[24px] font-bold leading-[1.28] tracking-[-0.24px] text-neutral90 xl:text-[32px] xl:leading-[1.4] xl:tracking-[-0.32px]">
            Want to learn more about how we can help?
          </h3>
          <p className="mb-[32px] text-[14px] leading-[1.7] text-neutral20 xl:text-[16px] xl:leading-normal">
            We are here to help you and your workforce. Book a short call so we can learn more about
            your company, and show you how Brawn can help you retain your workforce and attract new
            talent.
          </p>
          <Link href={commonData.employerLink} target="_blank" className="visited:outline-none">
            <Button className="w-full xl:w-[unset]">Get Started</Button>
          </Link>
        </div>
      </div>
    </section>
  );
}
