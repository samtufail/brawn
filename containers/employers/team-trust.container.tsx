import { BOAIcon, MainStreetIcon, MerrillIcon, MudflapIcon, RBCIcon } from '@/icons';

const logos = [
  { id: '1', icon: <BOAIcon /> },
  { id: '2', icon: <MudflapIcon /> },
  { id: '3', icon: <RBCIcon /> },
  { id: '4', icon: <MerrillIcon /> },
  { id: '5', icon: <MainStreetIcon /> },
];

export function TeamTrust() {
  return (
    <section className="global-container px-[20px] py-[40px] xl:px-0 xl:py-[80px]">
      <h2 className="mb-[16px] text-[32px] font-bold leading-[1.16] tracking-[-0.32px] text-neutral90 xl:mb-[20px] xl:text-[36px] xl:leading-[1.2] xl:tracking-[-0.36px]">
        A team you can trust
      </h2>
      <p className="mb-[20px] text-[16px] text-neutral20 xl:mb-[24px]">
        Our team is composed of financial and technology experts that come from Wall Street, Silicon
        Valley and main street. We have years of experience managing billions of dollars and
        providing financial tooling to tens of thousands of consumers.
      </p>
      <h3 className="mb-[16px] text-[32px] font-bold leading-[1.16] tracking-[-0.32px] text-neutral90 xl:mb-[20px] xl:text-[24px] xl:leading-[1.4] xl:tracking-[-0.24px]">
        The trades run through our blood
      </h3>
      <p className="mb-[32px] text-[16px] text-neutral20 xl:mb-[60px]">
        We come from a family of laborers, carpenters, welders and other trades. We've seen the
        struggles your workers go through first hand. We are dedicated to improving their
        livelihood. This in turn will help improve your construction business!
      </p>
      <p className="mb-[16px] text-[14px] text-[#969bab] xl:text-[16px]">
        Our team comes from these great companies
      </p>
      <div className="grid gap-[12px] xl:grid-cols-5 xl:gap-0">
        {logos.map(({ id, icon }) => (
          <div
            key={id}
            className="flex h-[104px] w-full items-center justify-center rounded-lg border border-solid border-neutral30 xl:w-[236.8px]"
          >
            {icon}
          </div>
        ))}
      </div>
    </section>
  );
}
