import Link from 'next/link';

export function MentalContainer() {
  return (
    <section className="global-container grid px-[20px] py-[40px] xl:grid-cols-2 xl:gap-[64px] xl:px-0 xl:py-[80px]">
      <div>
        <h2 className="mb-[16px] max-w-[340px] text-[32px] font-bold leading-[1.16] tracking-[-0.32px] xl:mb-[20px] xl:max-w-[557px] xl:text-[36px] xl:leading-[1.2] xl:tracking-[-0.36px]">
          Mental health is a big problem in construction today
        </h2>
        <p className="mb-[32px] max-w-[544px] text-[16px] leading-normal text-neutral20 xl:mb-0">
          Employers can help by offering a great personal finance solution that eliminates the
          financial stress a worker may be going through.
        </p>
      </div>
      <div className="flex flex-col gap-[20px]">
        <p className="rounded-[8px] border border-solid border-neutral60 px-[26px] py-[28px] text-[16px] leading-normal text-neutral90 xl:text-[18px]">
          The construction industry has the&nbsp;
          <Link className="link" href="/">
            second-highest
          </Link>
          &nbsp;suicide rate out of all major industries in the U.S.
        </p>
        <p className="rounded-[8px] border border-solid border-neutral60 px-[26px] py-[28px] text-[16px] leading-normal text-neutral90 xl:text-[18px]">
          The rate of suicide among male construction workers is&nbsp;
          <Link className="link" href="/">
            four times
          </Link>
          &nbsp;higher than the general population.
        </p>
        <p className="rounded-[8px] border border-solid border-neutral60 px-[26px] py-[28px] text-[16px] leading-normal text-neutral90 xl:text-[18px]">
          The construction industry is facing an enormous growth in mental health related issues for
          the workforce. Financial strain is a&nbsp;
          <Link className="link" href="/">
            major-factor
          </Link>
          &nbsp;that contributes to this crisis.
        </p>
      </div>
    </section>
  );
}
