import Image from 'next/image';
import Link from 'next/link';

function Challenge({
  percentage,
  description,
  source,
  sourceLink,
}: {
  percentage: string;
  description: string;
  source?: string;
  sourceLink?: string;
}) {
  return (
    <div className="relative flex min-h-[320px] flex-col justify-between rounded-[8px] bg-[#f1faff] px-[26px] py-[32px]">
      <Image
        src="/images/employer/challenges-bg.png"
        alt="Challenges Background Lines"
        width={74}
        height={185}
        className="absolute right-0 top-0 h-[113.5px] w-[43px] xl:h-[185px] xl:w-[74px]"
      />
      <h1 className="text-[48px] font-[600] leading-[1.1] tracking-[-0.72px] text-neutral90 xl:text-[72px]">
        {percentage}
      </h1>
      <div>
        <p className="mb-[12px] text-[16px] text-neutral20 xl:leading-[1.7]">{description}</p>
        {source && sourceLink && (
          <p className="text-[14px] leading-[1.7] text-[#969bab]">
            Source:&nbsp;
            <Link className="underline" href={sourceLink}>
              {source}
            </Link>
          </p>
        )}
      </div>
    </div>
  );
}

export function ChallengesContainer() {
  return (
    <section className="global-container px-[20px] py-[40px] xl:px-0 xl:py-[80px]">
      <h2 className="mb-[32px] text-[32px] font-bold leading-[1.16] tracking-[-0.32px] text-neutral90 xl:mb-[40px] xl:text-[36px] xl:leading-[1.2] xl:tracking-[-0.36px]">
        Challenges construction companies face today
      </h2>
      <div className="grid gap-[24px] xl:grid-cols-2">
        <Challenge
          percentage="73%"
          description="of Americans rank financial stress as the number one source of stress. This means workers
          are Twice as likely to leave your organization."
          source="CNBC"
          sourceLink="https://cnbc.com"
        />
        <Challenge
          percentage="68%"
          description="of employees turnover in construction costing you $10k/worker."
          source="Homebase"
          sourceLink="https://cnbc.com"
        />
        <Challenge
          percentage="63%"
          description="of Americans rank financial stress as the number one source of stress. This means workers
          are Twice as likely to leave your organization."
          source="TIAA"
          sourceLink="https://cnbc.com"
        />
        <Challenge
          percentage="500k"
          description="of Americans rank financial stress as the number one source of stress. This means workers
          are Twice as likely to leave your organization."
        />
      </div>
    </section>
  );
}
