import dynamic from 'next/dynamic';

export const BottomLineContainer = dynamic(() =>
  import('./bottom-line.container').then((mod) => mod.BottomLineContainer),
);
export const BrawnHelpContainer = dynamic(() =>
  import('./brawn-help.container').then((mod) => mod.BrawnHelpContainer),
);
export const CareContainer = dynamic(() =>
  import('./care.container').then((mod) => mod.CareContainer),
);
export const ChallengesContainer = dynamic(() =>
  import('./challenges.container').then((mod) => mod.ChallengesContainer),
);
export const Header = dynamic(() => import('./header.container').then((mod) => mod.Header));
export const MentalContainer = dynamic(() =>
  import('./mental.container').then((mod) => mod.MentalContainer),
);
export const SolutionContainer = dynamic(() =>
  import('./solution.container').then((mod) => mod.SolutionContainer),
);
export const TeamTrust = dynamic(() =>
  import('./team-trust.container').then((mod) => mod.TeamTrust),
);
export const UpsellContainer = dynamic(() =>
  import('./up-sell.container').then((mod) => mod.UpsellContainer),
);
