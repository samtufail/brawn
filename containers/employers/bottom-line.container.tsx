import Image from 'next/image';
import Link from 'next/link';
import { Button } from '@/components';
import { commonData } from '@/data/common';

function ImprovedRetention() {
  return (
    <div className="relative rounded-[8px] border border-solid border-[#f2f0f6] bg-[#f6f2ff] px-[20px] pt-[24px] xl:col-span-2 xl:min-h-[320px] xl:p-0">
      <Image
        src="/images/employer/bottom-line-1.png"
        alt="Savings Graph"
        className="absolute bottom-0 right-0 hidden object-cover xl:inline"
        width={625}
        height={288}
      />
      <div className="flex h-full min-h-[318px] flex-col xl:justify-between xl:px-[24px] xl:py-[28px]">
        <h3 className="mb-[40px] text-[36px] font-[600] leading-[1.35] tracking-[-0.54px] xl:mb-0 xl:text-[72px] xl:leading-[1.1] xl:tracking-[-0.72px]">
          30%
        </h3>
        <div className="mb-[24px] flex flex-col gap-[14px] xl:mb-0 xl:gap-[12px]">
          <h4 className="text-[18px] font-[600] leading-[1.4] tracking-[-0.18px] text-neutral90 xl:text-[20px] xl:tracking-[-0.2px]">
            Improve retention rate by 30%+
          </h4>
          <p className="max-w-[295px] text-[14px] leading-[1.7] text-neutral20 xl:max-w-[328px] xl:text-[16px] xl:leading-normal">
            Minimize employee turnover and spend less money trying to find new talent.
          </p>
        </div>
        <Image
          src="/images/employer/bottom-line-1-mob.png"
          alt="Savings Graph"
          className="inline-block object-cover xl:hidden"
          width={295}
          height={508}
        />
      </div>
    </div>
  );
}

function ImproveHappiness() {
  return (
    <div className="flex flex-col-reverse justify-between rounded-[8px] border border-solid border-[#edf3f6] bg-[#f1faff] px-[24px] py-[28px] xl:col-span-1 xl:row-span-2 xl:min-h-[740px] xl:flex-col">
      <div>
        <div className="mb-[12px] flex max-w-[312px] justify-between">
          <div className="flex items-center gap-[12px]">
            <h4 className="text-[16px] font-[600] leading-[1.4] tracking-[-0.16px] text-neutral90">
              Brawn
            </h4>
            <p className="flex min-h-[20px] items-center rounded-[4px] bg-[#46b8ff] px-[10px] text-[12px] font-[500] text-white">
              APP
            </p>
          </div>
          <p className="text-[14px] leading-[1.7] text-neutral70">Today at 3:45 PM</p>
        </div>
        <div className="relative mb-[8px] h-[203px] w-[300px] xl:w-[312px]">
          <Image
            src="/images/employer/bottom-line-2.png"
            alt="Happiness Graph"
            fill
            className="object-contain object-center"
          />
        </div>
        <p className="rounded-[8px] bg-white p-[16px] text-[14px] leading-normal text-neutral90 xl:py-[20px] xl:pl-[16px] xl:text-[15.5px]">
          Last week you had&nbsp;
          <span className="font-[500]">330 happy employee!</span>
          &nbsp;Great job! That's an all time high. Keep going.
        </p>
      </div>
      <div className="mb-[24px] xl:mb-0">
        <h3 className="mb-[40px] text-[36px] font-[600] leading-[1.35] tracking-[-0.54px] xl:mb-0 xl:text-[72px] xl:leading-[1.1] xl:tracking-[-0.72px]">
          40%
        </h3>
        <div className="flex flex-col gap-[12px]">
          <h4 className="text-[18px] font-[600] leading-[1.4] tracking-[-0.18px] text-neutral90 xl:text-[20px] xl:tracking-[-0.2px]">
            Improve happiness by up to 40%
          </h4>
          <p className="max-w-[295px] text-[14px] leading-[1.7] text-neutral20 xl:max-w-[328px] xl:text-[16px] xl:leading-normal">
            Improve your employees' mental health and show them that you care about them.
          </p>
        </div>
      </div>
    </div>
  );
}

function ImprovedProductivity() {
  return (
    <div className="rounded-[8px] border border-solid border-[#f5eeeb] bg-[#fff8f5] px-[20px] py-[24px] xl:min-h-[360px] xl:pb-0 xl:pl-[26px] xl:pr-[15px] xl:pt-[32px]">
      <h3 className="mb-[12px] text-[18px] font-[600] leading-[1.4] tracking-[-0.18px] text-neutral90 xl:mb-0 xl:text-[20px] xl:tracking-[-0.2px]">
        Improved Productivity
      </h3>
      <p className="mb-[24px] max-w-[358px] text-[14px] leading-[1.7] text-neutral20 xl:mb-0 xl:max-w-[328px] xl:text-[16px] xl:leading-normal">
        Reduce your employees' financial stress. They'll sleep better and work harder at work.
      </p>
      <div className="relative min-h-[188px] w-full xl:-mt-2 xl:min-h-[240px]">
        <Image
          src="/images/employer/bottom-line-3.png"
          alt="Improved Productivity Graph"
          fill
          className="hidden object-contain object-bottom xl:inline"
        />
        <Image
          src="/images/employer/bottom-line-3-mob.png"
          alt="Improved Productivity Graph"
          fill
          className="object-contain object-bottom xl:hidden"
        />
      </div>
    </div>
  );
}

function AttractTalent() {
  return (
    <div className="rounded-[8px] border border-solid border-[#eaecf7] bg-[#eaecf7] xl:min-h-[360px] xl:pl-[26px] xl:pt-[32px]">
      <div className="px-[20px] pt-[24px] xl:p-0">
        <h3 className="mb-[12px] text-[18px] font-[600] leading-[1.4] tracking-[-0.18px] text-neutral90 xl:mb-0 xl:text-[20px] xl:tracking-[-0.2px]">
          Attract new talent
        </h3>
        <p className="mb-[24px] max-w-[358px] text-[14px] leading-[1.7] text-neutral20 xl:mb-0 xl:max-w-[328px] xl:text-[16px] xl:leading-normal">
          Grow your workforce by offering meaningful benefits without any extra burden.
        </p>
      </div>
      <div className="flex xl:justify-end">
        <div className="relative mb-[24px] ml-[20px] h-[188px] w-full rounded-[8px] xl:mb-0 xl:ml-0 xl:mt-[52px] xl:w-[407px]">
          <Image
            src="/images/employer/bottom-line-4.png"
            alt="Attract Talent Graph"
            fill
            className="hidden rounded-[8px] rounded-tr-none object-cover object-bottom xl:inline"
          />
          <Image
            src="/images/employer/bottom-line-4-mob.png"
            alt="Attract Talent Graph"
            fill
            className="rounded-[8px] rounded-tr-none object-cover object-bottom xl:hidden"
          />
        </div>
      </div>
    </div>
  );
}

export function BottomLineContainer() {
  return (
    <section className="global-container px-[20px] py-[40px] xl:px-0 xl:py-[80px]">
      <h2 className="mb-[32px] text-[20px] font-bold leading-[1.4] tracking-[-0.2px] text-neutral90 xl:text-[24px] xl:tracking-[-0.24px]">
        What can Brawn do for your bottom line?
      </h2>
      <div className="mb-[40px] grid gap-[20px] xl:grid-cols-2">
        <ImprovedRetention />
        <ImproveHappiness />
        <div className="xl:col-span-1 xl:row-span-2">
          <div className="mb-[20px]">
            <ImprovedProductivity />
          </div>
          <AttractTalent />
        </div>
      </div>
      <div className="flex w-full items-center justify-center">
        <Link href={commonData.employerLink} target="_blank" className="visited:outline-none">
          <Button className="w-full xl:w-[unset]">Get Started</Button>
        </Link>
      </div>
    </section>
  );
}
