'use client';

import { useEffect, useState } from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { FacebookIcon, LinkedinIcon, TwitterIcon } from '@/icons';
import clsx from 'clsx';

export function ShareButtons({
  description,
  isBlog = false,
}: {
  description: string;
  isBlog?: boolean;
}) {
  const [scrollPosition, setScrollPosition] = useState(0);
  const handleScroll = () => {
    const position = window.scrollY;
    setScrollPosition(position);
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll, { passive: true });
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  const pathname = usePathname();

  const uri = `${process.env.NEXT_PUBLIC_APP_URL}${pathname}`;
  const encoded = encodeURI(uri);

  const scrollPoint = isBlog ? 930 : 632;

  return (
    <div
      className={clsx('flex flex-col gap-1', { 'fixed top-[20px]': scrollPosition > scrollPoint })}
    >
      <p className="text-[12px] leading-[20.4px] text-neutral70">Share</p>
      <Link href={`https://www.facebook.com/share.php?u=${encoded}`} target="_blank">
        <FacebookIcon />
      </Link>
      <Link href={`https://www.linkedin.com/shareArticle?mini=true&url=${encoded}`} target="_blank">
        <LinkedinIcon />
      </Link>
      <Link
        href={`https://twitter.com/intent/tweet?url=${encoded}&text=${description}`}
        target="_blank"
      >
        <TwitterIcon />
      </Link>
    </div>
  );
}
