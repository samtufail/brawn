import dynamic from 'next/dynamic';

export const HelpExplainerContainer = dynamic(() =>
  import('./help-explainer.container').then((mod) => mod.HelpExplainerContainer),
);
export const NewsLetterContainer = dynamic(() =>
  import('./news-letter.container').then((mod) => mod.NewsLetterContainer),
);
export const SecurityContainer = dynamic(() =>
  import('./security.container').then((mod) => mod.SecurityContainer),
);

export { NewsLetterContainerV2 } from './news-letter-v2.container';
export { ShareButtons } from './share-buttons.container';
