import Link from 'next/link';
import { Button } from '@/components';
import { commonData, newsLetterData } from '@/data/common';

const { title, description } = newsLetterData;
const { waitlistButtonText, waitlistLink } = commonData;

export function NewsLetterContainer() {
  return (
    <section className="global-container flex min-h-[382px] flex-col items-center justify-center bg-[#021E4A] bg-[url(/images/common/news-letter/bg.png)] bg-contain bg-no-repeat px-[30px] py-[40px] text-center xl:my-[172px] xl:p-0">
      <h2 className="text-[36px] font-[600] leading-[1.35] tracking-[-0.54px] text-white">
        {title}
      </h2>
      <p className="mt-[16px] max-w-[609px] text-[18px] leading-normal text-neutral40">
        {description}
      </p>
      <Link href={waitlistLink} target="_blank" className="visited:outline-none">
        <Button className="mt-[32px]">{waitlistButtonText}</Button>
      </Link>
    </section>
  );
}
