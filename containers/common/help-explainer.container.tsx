import { HelpExplainer } from '@/components';
import { helpData } from '@/data/home';

const { keyPoints } = helpData;

export function HelpExplainerContainer({ points }: { points: typeof keyPoints }) {
  return (
    <div className="flex w-full max-w-[1018px] flex-col gap-[50px] md:gap-[100px]">
      {points?.map((keyPoint, index) => (
        <HelpExplainer
          key={keyPoint.title}
          title={keyPoint.title}
          description={keyPoint.description}
          image={keyPoint.image}
          right={(index + 1) % 2 === 0}
        />
      ))}
    </div>
  );
}
