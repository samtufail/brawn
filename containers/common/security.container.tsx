import Image from 'next/image';
import { securityData } from '@/data/home/security.data';
import clsx from 'clsx';

const { title, points } = securityData;

export function SecurityContainer() {
  return (
    <section className="global-container px-[20px] py-[40px] xl:px-[144px] xl:pb-[60px] xl:pt-[80px]">
      <h2 className="text-center text-[26px] font-[600] leading-[1.35] tracking-[-0.54px] text-neutral90 md:px-[80px] md:text-[36px]">
        {title}
      </h2>
      <div className="grid items-center md:mt-[32px] md:grid-cols-2 xl:mt-[60px]">
        <div className="relative flex min-h-[355px] items-center justify-center bg-[#ffede5] xl:mr-[52px] xl:min-h-[unset] xl:p-[100px]">
          <Image
            src="/images/employer/security.png"
            alt="Lock Image representing security"
            width={270}
            height={250}
            sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
            className="h-[252px] w-[238px] object-contain xl:w-[269px]"
          />
        </div>
        <div className="mt-[25px] flex flex-col gap-12">
          {/* Card */}
          {points.map((point, idx) => (
            <div key={point.title} className="grid grid-cols-[32px_1fr] items-start gap-5">
              <div className="relative size-8">
                <Image
                  src={point.image}
                  alt={`An Icon Representing ${point.title}`}
                  fill
                  sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
                  style={{ objectFit: 'contain' }}
                />
              </div>
              <div
                className={clsx('flex flex-col gap-[8px]', {
                  'max-w-[398px]': idx === 0,
                  'max-w-[371px]': idx === 1,
                  'max-w-[434px]': idx === 2,
                })}
              >
                <h3 className="text-[16px] font-[600] leading-[1.4] tracking-[-0.18px] text-neutral90 xl:text-[18px]">
                  {point.title}
                </h3>
                <p className="text-[14px] leading-normal text-neutral70 xl:text-[16px]">
                  {point.description}
                </p>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
}
