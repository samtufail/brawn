'use client';

import { FormEvent } from 'react';
import { Button } from '@/components';

export function NewsLetterContainerV2() {
  async function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    console.log(formData);
  }
  return (
    <section className="global-container  px-[20px] py-[40px] text-center xl:mb-[172px] xl:p-0">
      <div className="flex min-h-[376px] flex-col bg-[#021E4A] bg-[url(/images/common/news-letter-v2/bg-mobile.png)] bg-contain bg-no-repeat px-[18px] py-[40px] xl:min-h-[382px] xl:items-center xl:justify-center xl:bg-[url(/images/common/news-letter-v2/bg.png)] xl:p-0">
        <h2 className="text-[24px] font-[600] leading-[1.35] tracking-[-0.54px] text-white xl:text-[36px]">
          Ready to maximize your construction career?
        </h2>
        <p className="mt-[16px] max-w-[609px] text-[14px] leading-normal text-neutral40 xl:text-[18px]">
          Subscribe to get more great career tips and tricks. We'll help you make the most out of
          your construction career.
        </p>
        <form
          onSubmit={onSubmit}
          className="mt-[32px] flex max-h-[48px] w-full flex-col gap-[12px] xl:w-[586px] xl:flex-row "
        >
          <input
            type="text"
            name="name"
            className="w-full rounded-[4px] px-[16px] py-[12px] text-[16px] leading-normal text-neutral20 focus:outline-none xl:w-[400px] xl:grow"
          />
          <Button type="submit" className="w-full grow-0 xl:w-[172px]">
            Subscribe
          </Button>
        </form>
      </div>
    </section>
  );
}
