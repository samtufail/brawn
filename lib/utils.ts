import { Article, Career } from '@/entities';
import { Salary } from '@/entities/salaries.entity';
import { clsx, type ClassValue } from 'clsx';
import { twMerge } from 'tailwind-merge';

export function cn(...inputs: ClassValue[]) {
  return twMerge(clsx(inputs));
}

export function numberWithCommas(x: number) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

export function nFormatter(num: number, digits: number) {
  const lookup = [
    { value: 1, symbol: '' },
    { value: 1e3, symbol: 'k' },
    { value: 1e6, symbol: 'M' },
    { value: 1e9, symbol: 'G' },
    { value: 1e12, symbol: 'T' },
    { value: 1e15, symbol: 'P' },
    { value: 1e18, symbol: 'E' },
  ];
  const regexp = /\.0+$|(?<=\.[0-9]*[1-9])0+$/;
  const item = lookup.findLast((el) => num >= el.value);
  return item ? (num / item.value).toFixed(digits).replace(regexp, '').concat(item.symbol) : '0';
}

export function sortHighestCareersFirst(careers: Career[]) {
  const res = careers.sort((a, b) => b.attributes.hourlyRate - a.attributes.hourlyRate);
  return res;
}

export function sortLowestCareersFirst(careers: Career[]) {
  const res = careers.sort((a, b) => a.attributes.hourlyRate - b.attributes.hourlyRate);
  return res;
}

export const capitalizeSlug = (str: string) => {
  const newString = str.replace(/-/g, ' ');
  return `${newString.charAt(0).toUpperCase()}${newString.slice(1)}`;
};

export const kebabize = (string: string) => {
  const returnString = string
    .replace(/([a-z])([A-Z])/g, '$1-$2')
    .replace(/[\s_]+/g, '-')
    .toLowerCase();
  return returnString;
};

export const capitalizeUnderscore = (str: string) => {
  const newString = str.replace(/_/g, ' ');
  return `${newString.charAt(0).toUpperCase()}${newString.slice(1)}`;
};

export const articlesOrder = {
  overview: 1,
  'how-to-become': 2,
  salary: 3,
  schools: 4,
  'certifications-licenses': 5,
  'tools-equipment': 6,
};

export const calculateLowAvgHighSalaries = (salaries: Salary[]) => {
  let totalLow: number = 0;
  let totalAvg: number = 0;
  let totalHigh: number = 0;
  salaries.forEach((salary) => {
    totalLow += salary.attributes.annualLow;
    totalAvg += salary.attributes.annualAvg;
    totalHigh += salary.attributes.annualHigh;
  });
  return {
    totalLow: totalLow / salaries.length,
    totalAvg: totalAvg / salaries.length,
    totalHigh: totalHigh / salaries.length,
  };
};

export const calculateLowAvgHighSalariesHourly = (salaries: Salary[]) => {
  const data = calculateLowAvgHighSalaries(salaries);
  return {
    totalLow: data.totalLow / 52 / 40,
    totalAvg: data.totalAvg / 52 / 40,
    totalHigh: data.totalHigh / 52 / 40,
  };
};

export const sortSalariesByState = (salaries: Salary[]) => {
  const sorted = salaries.sort((a, b) => a.attributes.state.localeCompare(b.attributes.state));
  return sorted;
};

export function truncateString(str: string, maxLength: number): string {
  if (str.length === 0) return str;
  if (str.length > maxLength) {
    return `${str.slice(0, maxLength)} ...`;
  }
  return str;
}

export function toTitleCase(str: string) {
  return str.replace(/\w\S*/g, (txt) => txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase());
}

export function formatRating(rating: number): string {
  if (rating === undefined || rating === null) return '-';
  return `${rating}/5.0`;
}

export function sortAPILinks(a: Article, b: Article) {
  const first = articlesOrder[a.attributes.careerPage as keyof typeof articlesOrder];
  const second = articlesOrder[b.attributes.careerPage as keyof typeof articlesOrder];
  return first - second;
}

export function formatDate(date: string) {
  return new Date(date || '2019-02-19T06:00:00Z').toLocaleDateString('en-us', {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  });
}
