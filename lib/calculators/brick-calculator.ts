import { BrickCalculator, BrickType, UnitType, WallThickness } from '@/entities';

// Brick Sizes
const brickSizes = {
  Standard: { width: 203.2, height: 57.15, depth: 92.075 },
  Queensize: { width: 193.675, height: 69.85, depth: 69.85 },
  Modular: { width: 193.675, height: 57.15, depth: 92.075 },
  Engineer: { width: 203.2, height: 69.85, depth: 92.075 },
  Utility: { width: 292.1, height: 88.9, depth: 92.075 },
  Closure: { width: 193.675, height: 88.9, depth: 92.075 },
  Kingsize: { width: 244.475, height: 61.6857142857, depth: 76.2 },
};

// Pricing
const priceValues = {
  Standard: 1.0,
  Queensize: 1.35,
  Modular: 0.65,
  Engineer: 1.63,
  Utility: 1.65,
  Closure: 1.68,
  Kingsize: 1.7,
  bagOfMortar: 7.0,
  sand: 4.0,
};

export const initialBrickCalculatorValues: BrickCalculator = {
  projectType: '',
  levelOfExpertise: '',
  unit: UnitType.Feet,
  wallHeight: 0,
  wallWidth: 0,
  wallThickness: WallThickness.Single,
  brickType: BrickType.Standard,
  jointThickness: 10,
  mixRatio: 0.8,
};

// Unit conversion constants
const FT_TO_METER = 3.281;
const MM_TO_METER = 1000;
const CUBIC_METER_TO_CUBIC_FEET = 35.315;
const BAG_VOLUME_CUBIC_METERS = 0.025;

export const brickCalculator = (values: BrickCalculator) => {
  // Wall dimensions
  const wallHeight = values.unit === 'Ft' ? values.wallHeight / FT_TO_METER : values.wallHeight;
  const wallWidth = values.unit === 'Ft' ? values.wallWidth / FT_TO_METER : values.wallWidth;

  // Brick and mortar dimensions
  const brickSize = brickSizes?.[values.brickType as keyof typeof brickSizes];
  const brickPrice = priceValues?.[values.brickType as keyof typeof priceValues];
  const mortarThickness = values.jointThickness / MM_TO_METER;
  const brickHeight = brickSize.height / MM_TO_METER;
  const brickWidth = brickSize.width / MM_TO_METER;
  const brickDepth = brickSize.depth / MM_TO_METER;

  // Adjusted brick dimensions
  const adjustedBrickHeight = brickHeight + mortarThickness;
  const adjustedBrickWidth = brickWidth + mortarThickness;
  const wallThickness = values.wallThickness === 'Single Brick' ? brickDepth : brickDepth * 2;

  // Volume calculations
  const adjustedBrickVolume = adjustedBrickWidth * adjustedBrickHeight * wallThickness;
  const brickVolume = brickWidth * brickHeight * brickDepth;
  const mortarVolumePerBrick = adjustedBrickVolume - brickVolume;
  const wallArea = wallHeight * wallWidth;
  const adjustedBrickArea = adjustedBrickHeight * adjustedBrickWidth;

  // Quantities calculations
  const numberOfBricksNeeded = wallArea / adjustedBrickArea;
  const totalMortarVolume = numberOfBricksNeeded * mortarVolumePerBrick;
  const numberOfBags = totalMortarVolume / BAG_VOLUME_CUBIC_METERS;
  const sandCbMeter = totalMortarVolume * values.mixRatio;
  const sandCbFt = sandCbMeter * CUBIC_METER_TO_CUBIC_FEET;

  // Price calculations
  const bricksPrice = numberOfBricksNeeded * brickPrice;
  const mortarBagsPrice = numberOfBags * priceValues.bagOfMortar;
  const sandPrice = sandCbFt * priceValues.sand;

  return {
    numberOfBricksNeeded,
    numberOfBags,
    sandCbFt,
    bricksPrice,
    mortarBagsPrice,
    sandPrice,
  };
};
