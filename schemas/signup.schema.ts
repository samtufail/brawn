import * as Yup from 'yup';

export const BrickCalculatorSchema = Yup.object().shape({
  projectType: Yup.string().required('You must select the Type of project.'),
  levelOfExpertise: Yup.string().required('You must select your level of expertise.'),
  wallHeight: Yup.number()
    .min(1, 'You must enter a valid wall height.')
    .required('You must enter a valid wall height.'),
  wallWidth: Yup.number()
    .min(1, 'You must enter a valid wall width.')
    .required('You must enter a valid wall width.'),
});
