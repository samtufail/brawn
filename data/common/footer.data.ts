export const footerData = {
  copyright: '© Brawn Technologies, Inc. 2024 - All Right Reserved',
  disclosure:
    'Nothing on this site should be considered an offer, solicitation of an offer, or advice to buy or sell securities or investment products. Past performance is no guarantee of future results. Any historical returns, expected returns, or probability projections are hypothetical in nature and may not reflect actual future performance. Account holdings and other information provided are for illustrative purposes only and are not to be considered investment recommendations. The content on this site is for informational purposes only and does not constitute a comprehensive description of Brawn’s advisory services. Brawn is not yet a registered financial advisor. Services offered are subject to change.',
  links: [
    {
      title: 'Menu',
      links: [
        { title: 'Home', href: '/', target: '_self' },
        { title: 'Construction Careers', href: '/careers/construction', target: '_self' },
        { title: 'Calculators', href: '/calculators', target: '_self' },
        { title: 'Blog', href: '/blog', target: '_self' },
        { title: 'Contact', href: '/contact', target: '_self' },
      ],
    },
    {
      title: 'Company',
      links: [
        { title: 'Privacy Policy', href: '/privacy', target: '_self' },
        { title: 'Term of Use', href: '/terms', target: '_self' },
      ],
    },
    {
      title: 'Follow',
      links: [
        { title: 'Instagram', href: 'https://www.instagram.com/brawnhq/', target: '_blank' },
        { title: 'Linkedin', href: 'https://www.linkedin.com/company/brawn/', target: '_blank' },
        {
          title: 'Crunchbase',
          href: 'https://www.crunchbase.com/organization/brawn-ea4b',
          target: '_blank',
        },
      ],
    },
  ],
};
