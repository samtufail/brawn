export const newsLetterData = {
  title: 'Sign up for early access',
  description:
    'You deserve a better financial partner. Get early access to Brawn by joining our waitlist.',
};
