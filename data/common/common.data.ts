export const commonData = {
  waitlistLink: 'https://brawnhq.typeform.com/to/dGqJYsmZ?product_slug=all',
  employerLink: 'https://brawnhq.typeform.com/to/wdH4QhUJ',
  waitlistButtonText: 'Get Started',
};
