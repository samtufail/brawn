export { commonData } from './common.data';
export { footerData } from './footer.data';
export { navbarData } from './navbar.data';
export { newsLetterData } from './news-letter.data';
