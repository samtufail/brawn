export const navbarData = {
  homeLinks: [
    { href: '/', title: 'Home' },
    { href: '/employers', title: 'Employers' },
    { href: '/about', title: 'About' },
    // { href: '/blog', title: 'Blog' }, # Uncomment after blog goes live
    { href: '/careers', title: 'Construction Careers' },
    { href: '/calculators', title: 'Calculators' },
    { href: '/contact', title: 'Contact' },
  ],
  careerDesktopLinks: [
    { title: 'Financial Tools', href: '/' },
    {
      title: 'Careers',
      href: '#',
      links: [{ title: 'Construction', href: '/careers/construction' }],
    },
  ],
  careerMobileLinks: [
    { title: 'Financial Tools', href: '/' },
    {
      title: 'Construction Careers',
      href: '/careers/construction',
    },
  ],
  calculatorLinks: [
    { title: 'Financial Tools', href: '/' },
    {
      title: 'Construction Careers',
      href: '/careers/construction',
    },
    {
      title: 'Calculators',
      href: '/calculators',
    },
  ],
};
