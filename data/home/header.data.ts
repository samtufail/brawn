import { BarsIcon, StatusUpIcon, TagIcon } from '@/icons';

export interface PointType {
  title: string;
  icon: () => React.JSX.Element;
  variant: 'primary' | 'secondary' | 'trinity';
}

export interface HeaderDataType {
  title: string;
  description: string;
  points: PointType[];
  waitlistLink: string;
}

export const headerData: HeaderDataType = {
  title: 'The financial wingman for blue collar workers',
  description:
    "With Brawn, you can take meaningful steps towards becoming the person you've always wanted to be. We are a saving and retirement app that helps you achieve your life goals and financial security.",
  points: [
    {
      title: 'Build a financial safety net',
      icon: BarsIcon,
      variant: 'primary',
    },
    {
      title: 'Make smarter purchase decisions',
      icon: TagIcon,
      variant: 'secondary',
    },
    {
      title: 'Plan & grow your retirement',
      icon: StatusUpIcon,
      variant: 'trinity',
    },
  ],
  waitlistLink: '',
};
