import { BarsIcon, StatusUpIcon, TagIcon } from '@/icons';

import { HeaderDataType, PointType } from './header.data';

export interface AdHeaderDataType {
  [key: string]: HeaderDataType;
}

export enum ProductType {
  CREDIT_BUILDER = 'CREDIT_BUILDER',
  RETIREMENT = 'RETIREMENT',
  BUDGETING = 'BUDGETING',
  CASH_BACK = 'CASH_BACK',
}

export const adHeaderData: AdHeaderDataType = {
  CREDIT_BUILDER: {
    title: 'Build Your Credit Score',
    description:
      "Don't let a low credit score hold you back. Our product is designed to help hard-working construction pros like you build a better financial future. Start improving your credit score today and pave the way for a brighter tomorrow. Sign up now and take control of your financial health!",
    points: [],
    waitlistLink: 'https://brawnhq.typeform.com/to/dGqJYsmZ?product_slug=credit-builder',
  },
  RETIREMENT: {
    title: 'Retirement Built for the Hard Worker',
    description:
      "You've put in the hard work on the job, now let us help you plan for an easy retirement. Build your savings with us and ensure a financially secure future. Sign up now!",
    points: [],
    waitlistLink: 'https://brawnhq.typeform.com/to/dGqJYsmZ?product_slug=retirement',
  },
  BUDGETING: {
    title: 'Nail down your finances, Save more!',
    description:
      'Tired of feeling like your money slips through your fingers? Nail down your finances with our budgeting tools designed for construction workers. Start planning and saving today for a more secure tomorrow!',
    points: [],
    waitlistLink: 'https://brawnhq.typeform.com/to/dGqJYsmZ?product_slug=budgeting',
  },
  CASH_BACK: {
    title: 'Get up to 8% cash back on your purchases!',
    description:
      'Link your existing debit card and enjoy cash back rewards at select locations. Earn up to 8% cash back and save more on every purchase. Join today and start saving!',
    points: [],
    waitlistLink: 'https://brawnhq.typeform.com/to/dGqJYsmZ?product_slug=cash-back',
  },
};
