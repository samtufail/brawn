export const featuresData = {
  title: 'We are a financial company that serves working class Americans',
  description: `The working class has fallen behind. We're building a new financial services company
  that helps construction and trades men and woman save more, spend smarter, and
  invest for the future.`,
  images: [
    '/images/home/features/features-portrait-1.jpg',
    '/images/home/features/features-portrait-2.jpg',
    '/images/home/features/features-portrait-3.jpg',
  ],
};
