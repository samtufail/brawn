export const securityData = {
  title: 'Security & support you can trust',
  image: '/images/home/security/security-1.png',
  points: [
    {
      title: 'Serious security',
      description:
        'Brawn uses secure processes to protect your information and help prevent unauthorized use',
      image: '/images/home/security/icon-1.png',
    },
    {
      title: 'Protection and peace of mind',
      description: 'Any saving account deposits are FDIC insured up to $250,000.',
      image: '/images/home/security/icon-2.png',
    },
    {
      title: 'Anytime, anywhere support',
      description:
        "If you need help, Brawn's support channels are standing by 24/7. Reach our friendly team by phone, email, in the app, or check out the Help Center.",
      image: '/images/home/security/icon-3.png',
    },
  ],
};
