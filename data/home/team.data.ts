export const teamData = {
  title: 'Our team comes from these great companies',
  companies: [
    { width: 184, height: 32, name: 'mudflap', image: '/images/home/team/team-1.png' },
    { width: 177, height: 66, name: 'rbc royal bank', image: '/images/home/team/team-2.png' },
    { width: 176, height: 65, name: 'merrill', image: '/images/home/team/team-3.png' },
    { width: 66, height: 66, name: 'mainstreet advisors', image: '/images/home/team/team-4.png' },
  ],
};
