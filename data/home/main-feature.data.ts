export const mainFeatureData = {
  mainImage: '/images/home/main-feature/main-feature.jpg',
  arrowImage: '/images/home/main-feature/arrows.png',
  title: 'The trades run through our blood',
  description:
    "We come from a family of laborers, carpenters, welders and other trades. We've seen the struggles you go through first hand. Our team is comprised of experts with decades of years of experience in financial management and banking.",
};
