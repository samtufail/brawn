export const optionsData = {
  title: 'The perfect finance app for',
  options: [
    { title: 'Construction Workers', image: '/images/home/features/options-1.jpg' },
    { title: 'Electricians', image: '/images/home/features/options-2.jpg' },
    { title: 'Iron Workers', image: '/images/home/features/options-3.jpg' },
    { title: 'Carpenters', image: '/images/home/features/options-4.jpg' },
    { title: 'Plumbers', image: '/images/home/features/options-5.jpg' },
    { title: 'Heavy Equipment Operators', image: '/images/home/features/options-6.jpg' },
    { title: 'Drywallers', image: '/images/home/features/options-7.jpg' },
    { title: 'Welders', image: '/images/home/features/options-8.jpg' },
    { title: 'Pipefitters', image: '/images/home/features/options-9.jpg' },
  ],
};
