export const helpData = {
  title: 'We will help you',
  keyPoints: [
    {
      title: 'Improve your credit score',
      description:
        "We'll help you improve your credit score so you can have a brighter future tomorrow. Our Credit Builder is easy to use and can help you unlock loans in the future.",
      image: '/images/home/help/help-0.jpg',
    },
    {
      title: 'Build a financial safety net',
      description:
        "We'll help you build a financial safety net that you can use for emergencies. If you're laid off, or have a family emergency you can “tap” your financial safety net.",
      image: '/images/home/help/help-1.png',
    },
    {
      title: 'Make smarter purchase decisions',
      description:
        "Our Smart Purchase tool, will allow you to ask us questions about your large purchase decisions. We'll let you know if it makes financial sense for you. We'll keep you in check and on track to meet your financial goals.",
      image: '/images/home/help/help-2.png',
    },
    {
      title: 'Earn great cash back',
      description:
        "Get up to 8% Cashback at select stores when you link your debit card. We've partnered with some amazing brands that will help you save money on everyday purchases.",
      image: '/images/home/help/help-3.png',
    },
    {
      title: 'Plan and grow your retirement',
      description:
        "Retirement can sometimes seem impossible. But we're confident we can help you build a plan that works for your retirement goals. Regardless of how old you are, or how much you already have saved, we'll come up with a perfect plan and help keep you on track.",
      image: '/images/home/help/help-4.png',
    },
  ],
};
