import { InputWithSelect, Select } from '@/components';
import {
  BrickType,
  JointThickness,
  LevelOfExpertise,
  MixRatio,
  Prices,
  ProjectType,
  Quantities,
  UnitType,
  WallThickness,
} from '@/entities';
import _ from 'lodash';

import { numberWithCommas } from '@/lib/utils';

const projectTypeOptions = _.map(ProjectType, (value: string) => ({ label: value, value }));
const expertiseOptions = _.map(LevelOfExpertise, (value: string) => ({
  label: value,
  value,
}));
const unitOptions = _.map(UnitType, (value: string) => ({ label: value, value }));
const brickTypeOptions = _.map(BrickType, (value: string) => ({ label: value, value }));
const wallThicknessOptions = _.map(WallThickness, (value: string) => ({ label: value, value }));
const jointThicknessOptions = [
  { label: '6 mm (1/4 inch)', value: JointThickness['6 mm (1/4 inch)'] },
  { label: '10 mm (3/8 inch)', value: JointThickness['10 mm (3/8 inch)'] },
  { label: '12 mm (1/2 inch)', value: JointThickness['12 mm (1/2 inch)'] },
];
const mixRatioOptions = [{ label: '1:4 cement to sand', value: MixRatio['1:4 cement to sand'] }];

export enum InputType {
  SELECT = 'select',
  INPUT_SELECT = 'input-select',
}

export interface CalculatorDataType {
  steps: {
    title: string;
    description: string;
    images?: string[];
    fields: {
      type: InputType;
      name?: string;
      placeholder?: string;
      label: string;
      inputName?: string;
      inputPlaceholder?: string;
      selectName?: string;
      options: any;
    }[];
  }[];
  submitButtonText: string;
  resetButtonText: string;
  errorMessage: string;
  recommendationTitle: string;
  recommendationDescription: string;
}

export const initialQuantities: Quantities = {
  numberOfBricksNeeded: 0,
  numberOfBags: 0,
  sandCbFt: 0,
};

export const initialPrices: Prices = {
  bricksPrice: 0,
  mortarBagsPrice: 0,
  sandPrice: 0,
};

export const brickCalculatorData: CalculatorDataType = {
  steps: [
    {
      title: 'Step 1 - Basic details about the project',
      description: 'Tell us a little bit about the project so we can improve our calculations.',
      fields: [
        {
          type: InputType.SELECT,
          name: 'projectType',
          label: 'Type of Project',
          options: projectTypeOptions,
        },
        {
          type: InputType.SELECT,
          name: 'levelOfExpertise',
          label: 'What is your level of expertise',
          options: expertiseOptions,
        },
      ],
    },
    {
      title: 'Step 2 - Project dimensions',
      description: "Now tell us about the dimensions of the wall or patio you're building",
      fields: [
        {
          type: InputType.INPUT_SELECT,
          label: 'Wall height',
          inputName: 'wallHeight',
          inputPlaceholder: 'Numbers only',
          selectName: 'unit',
          options: unitOptions,
        },
        {
          type: InputType.INPUT_SELECT,
          label: 'Wall width',
          inputName: 'wallWidth',
          inputPlaceholder: 'Numbers only',
          selectName: 'unit',
          options: unitOptions,
        },
        {
          type: InputType.SELECT,
          name: 'wallThickness',
          label: 'Wall Thickness',
          options: wallThicknessOptions,
        },
      ],
    },
    {
      title: 'Step 3 - Brick Type',
      description:
        "Most projects only require the standard brick type. But you can choose from other sizes below. As you change the Brick Type you'll see the description and image update.\n\nIf you're unsure, use the Standard brick size.",
      fields: [
        {
          type: InputType.SELECT,
          label: 'Brick Type',
          name: 'brickType',
          options: brickTypeOptions,
        },
      ],
      images: [1, 2, 3, 4, 5, 6, 7].map((el) => `/images/calculators/brick/${el}.jpg`),
    },
    {
      title: 'Step 4 - Mortar Specifications',
      description:
        'Enter the mortar thickness and mortar mix ratio. If you are unsure, you can use the default setting.',
      fields: [
        {
          type: InputType.SELECT,
          label: 'Joint Thickness',
          name: 'jointThickness',
          options: jointThicknessOptions,
        },
        {
          type: InputType.SELECT,
          label: 'Mix Ratio',
          name: 'mixRatio',
          options: mixRatioOptions,
        },
      ],
    },
  ],
  submitButtonText: 'Calculate',
  resetButtonText: 'Reset',
  errorMessage:
    'You must correctly enter all of the fields above. Scroll above and make the correction.',
  recommendationTitle: 'Our Recommendation',
  recommendationDescription:
    "We'll recommend how many bricks, bags of mortar and sand you will need for your project. Complete the form above, and you'll the results here.",
};

export const estimates = (quantities: Quantities, prices: Prices) => {
  if (quantities.numberOfBags > 0) {
    return [
      {
        title: 'Material Estimates',
        table: {
          title: 'Estimated Cost',
          rows: [
            {
              label: 'Number of Bricks',
              value: `~${numberWithCommas(Math.round(quantities.numberOfBricksNeeded))}`,
            },
            {
              label: 'Bags of mortar',
              value: `~${numberWithCommas(Math.round(quantities.numberOfBags))}`,
            },
            {
              label: 'Sand (Cubic Feet)',
              value: `~${numberWithCommas(Math.round(quantities.sandCbFt))}`,
            },
          ],
        },
      },
      {
        title: 'Cost Estimate',
        table: {
          title: 'Quantity',
          rows: [
            {
              label: 'Number of Bricks',
              value: `~ $${numberWithCommas(Math.round(prices.bricksPrice))}`,
            },
            {
              label: 'Bags of mortar',
              value: `~ $${numberWithCommas(Math.round(prices.mortarBagsPrice))}`,
            },
            {
              label: 'Sand (Cubic Feet)',
              value: `~ $${numberWithCommas(Math.round(prices.sandPrice))}`,
            },
            {
              label: 'Total Estimated Cost',
              value: `~ $${numberWithCommas(Math.round(prices.sandPrice + prices.bricksPrice + prices.mortarBagsPrice))}`,
            },
          ],
        },
      },
    ];
  }
  return null;
};

export const stepsGenerator = (step = brickCalculatorData.steps[0]) => (
  <>
    <div>
      <h2 className="text-2xl font-bold">{step.title}</h2>
      <p className="mb-[30px]">{step.description}</p>
    </div>
    {step.fields.map((field) => {
      switch (field.type) {
        case InputType.SELECT:
          return (
            <Select
              key={field.label}
              name={field.name || ''}
              label={field.label}
              options={field.options}
            />
          );
        case InputType.INPUT_SELECT:
          return (
            <InputWithSelect
              key={field.label}
              inputName={field.inputName || ''}
              selectName={field.selectName || ''}
              label={field.label}
              options={field.options}
            />
          );
        default:
          return null;
      }
    })}
    {step.images?.length && (
      <div className="flex flex-wrap items-center gap-[12px]">
        {step.images.map((el) => (
          <img key={el} src={el} alt="brick types" width={120} />
        ))}
      </div>
    )}
  </>
);
