import { Block, BlockTypes, LinkTypes, TableTypes, UpSellTypes } from '@/entities';

export const constructSchoolByStateBlocks = (
  career: string,
  slug: string,
  state: string,
  ogBlocks: Block[],
): Block[] => [
  {
    id: 1,
    title: `${career} Schools in ${state}`,
    body: `In this article we summarize the top ${state}'s top ${career} schools.`,
    __component: BlockTypes.SECTION,
  },
  {
    id: 2,
    title: `The top ${career} schools in ${state}`,
    body: `${state} has many great schools to choose from if you're looking to start a career as a ${career}.`,
    __component: BlockTypes.SECTION,
  },
  {
    id: 3,
    __component: BlockTypes.TABLE,
    type: TableTypes.SCHOOL_LIST,
  },
  {
    id: 4,
    __component: BlockTypes.UP_SELL,
    type: UpSellTypes.SchoolLoan,
  },
  {
    id: 5,
    title: `Learn more about a career in ${career}`,
    body: ogBlocks[0].body,
    __component: BlockTypes.SECTION,
  },
  {
    id: 6,
    __component: BlockTypes.PAGE_LINK,
    linkText: `How to become a ${career}`,
    linkURL: `/careers/${slug}/how-to-become`,
    type: LinkTypes.INTERNAL,
  },
  {
    id: 6,
    __component: BlockTypes.PAGE_LINK,
    linkText: `${career} Salary`,
    linkURL: `/careers/${slug}/salary`,
    type: LinkTypes.INTERNAL,
  },
  {
    id: 7,
    __component: BlockTypes.PAGE_LINK,
    title: `${career} Certifications & Licenses`,
    linkText: `Learn which certifications are needed to become a ${career}`,
    linkURL: `/careers/${slug}/certifications-licenses`,
    type: LinkTypes.UP_NEXT,
  },
];
