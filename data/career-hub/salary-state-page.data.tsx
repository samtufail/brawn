import { Block, BlockTypes, LinkTypes, TableTypes, UpSellTypes } from '@/entities';

import { numberWithCommas } from '@/lib/utils';

interface Params extends Block {
  isLargeHeading?: boolean;
  hideMargin?: boolean;
}

export const constructSalaryByStateBlocks = (
  career: string,
  slug: string,
  state: string,
  ogBlocks: Block[],
  totalAverage: number,
  avgSalary: number,
): Params[] => [
  {
    id: 1,
    title: `${career} Salary in ${state}`,
    body: `In this article, we cover everything you need to know about ${career} salaries in ${state}.`,
    __component: BlockTypes.SECTION,
  },
  {
    id: 2,
    title: `What is the salary of a ${career} in ${state}?`,
    body: `A ${career} working in ${state} can make around $${numberWithCommas(Math.round(avgSalary))} per year.\n\nPay in ${state} for a ${career} is ${avgSalary < totalAverage ? 'lower' : 'higher'} than the national average salary of $${numberWithCommas(Math.round(totalAverage))}.`,
    __component: BlockTypes.SECTION,
  },
  {
    id: 3,
    title: `${career} Salary Range`,
    body: `The chart below shows a range of ${career} salaries nationwide.`,
    __component: BlockTypes.SECTION,
  },
  {
    id: 4,
    __component: BlockTypes.TABLE,
    type: TableTypes.SALARY_RANGE_ANNUAL,
  },
  {
    id: 5,
    title: `Hourly rate of a ${career} in ${state}`,
    body: (
      <div>
        <p>{`The average hourly rate for a ${career} working in ${state} is $${(avgSalary / 52 / 40).toFixed(2)} per hour`}</p>
        <h3>{`${career} Hourly Rate Range`}</h3>
        <p>
          The chart below shows a range of&nbsp;
          {career}
          &nbsp;hourly rates nationwide.
        </p>
      </div>
    ),
    __component: BlockTypes.SECTION,
  },
  {
    id: 6,
    __component: BlockTypes.TABLE,
    type: TableTypes.SALARY_RANGE_HOURLY,
  },
  {
    id: 7,
    title: `Learn more about a career in ${career}`,
    body: (
      <div>
        {ogBlocks[0].body}
        <p className="mb-[32px] mt-[40px]">
          Checkout these other articles we have about career's for&nbsp;
          {career}
        </p>
      </div>
    ),
    __component: BlockTypes.SECTION,
    hideMargin: true,
  },
  {
    id: 8,
    __component: BlockTypes.PAGE_LINK,
    linkText: `How to become a ${career}`,
    linkURL: `/careers/${slug}/how-to-become`,
    type: LinkTypes.INTERNAL,
  },
  {
    id: 9,
    __component: BlockTypes.PAGE_LINK,
    linkText: `${career} Schools`,
    linkURL: `/careers/${slug}/schools`,
    type: LinkTypes.INTERNAL,
  },
  {
    id: 10,
    __component: BlockTypes.UP_SELL,
    type: UpSellTypes.RetireEarly,
  },
  {
    id: 11,
    __component: BlockTypes.PAGE_LINK,
    title: `How to become a ${career}?`,
    linkText: `Click here to learn about how to become a ${career}.`,
    linkURL: `/careers/${slug}/how-to-become`,
    type: LinkTypes.UP_NEXT,
  },
];
