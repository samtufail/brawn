<p align="center">
  <img src="https://i.ibb.co/k3t9KZY/apple-icon.png" alt="icon" border="0" width="96px">
</p>
<h2 align="center">Brawn - Saving. Retirement. Advice.</h2>

## :beginner: About

Brawn is designed specifically to empower construction and trades workers in America. Our platform offers tailored financial tools and expert advice to help these hardworking individuals save more, spend wisely, and invest for a secure future. We're committed to enhancing the financial well-being of the working class through personalized solutions and actionable insights.

<hr />

### :notebook: Pre-Requisites

You will need following extensions/tools setup in your dev environment i.e. VS Code to work on this project efficiently.

- Prettier
- ESLint

### :wrench: Development Environment

To setup the development environment you can use following step by step guide:

- Clone the repository with <code>git clone https://github.com/brawnhq/brawn-web.git</code>.
- Open the terminal in root directory of the project and run `npm install --save`.
- Create a file in root directory named `.env.local` and add this line `NEXT_PUBLIC_APP_URL=http://localhost:3000/` before starting development server.
- Make sure to add `NEXT_PUBLIC_STRAPI_API="STRAPI_URL"` in `.env.local` as well and replace `STRAPI_URL` with your Strapi URL i.e. `my-strapi.strapiapp.com/api`
- Run `npm run dev` to start the local server at PORT 3000.
- If all done, you can view the project at `http://localhost:3000` and start working on modules.

### :rocket: Production Environment

To setup the production environment on your local machine you can use following step by step guide:

- Clone the repository with <code>git clone https://github.com/brawnhq/brawn-web.git</code>.
- Open the terminal in root directory of the project and run `npm install --save` (This step is optional and only required if you just cloned the repository or just pulled latest code from repository).
- Create a file in root directory named `.env.production` and add this line `NEXT_PUBLIC_APP_URL=http://localhost:3000/` before build.
- - Make sure to add `NEXT_PUBLIC_STRAPI_API="STRAPI_URL"` in `.env.production` as well and replace `STRAPI_URL` with your Strapi URL i.e. `my-strapi.strapiapp.com/api`
- Run `npm run build` in the root directory of the project.
- Once done, you can run `npm run start` to start the local production server at PORT 3000.
- If all done, you can view the project at `http://localhost:3000` and start testing in production mode.

### :file_folder: File Structure

```
└── 📁brawn-web
 └── 📁app (app router)
 └── 📁components (contains all components, please use 'component-name.component.tsx' convention to create components)
 └── 📁containers (contains all containers, please use 'container-name.container.tsx' convention to create containers)
 └── 📁data (contains all data, please use 'component/container-name.data.ts' convention to create data objects)
 └── 📁icons (contains all icons, please use 'icon-name.icon.tsx' convention to create icons)
 └── 📁lib (contains all library functions)
 └── 📁public (next.js public directory)
 └── 📁styles (contains all styles)
 └── .editorconfig
 └── .gitignore
 └── .eslintrc.json
 └── .prettierignore
 └── .prettierrc.json
 └── README.md
 └── commitlint.config.cjs
 └── next-env.d.ts
 └── next.config.mjs
 └── package-lock.json
 └── package.json
 └── postcss.config.cjs
 └── tailwind.config.ts
 └── tsconfig.json
```

This project uses **Next.js 14**, **next/navigation**, and **next/font** to optimize and load Poppins, a custom Google Font designed for enhanced readability and style. Additionally, the project adheres to standard settings with **eslint**, **prettier**, **commitlint**, **editorconfig**, **husky**, and includes default configurations for **tailwindcss**. Please look into above mentioned files for more details.
