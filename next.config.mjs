import createMDX from '@next/mdx'

/** @type {import('next').NextConfig} */
const nextConfig = {
  // Configure `pageExtensions` to include markdown and MDX files
  pageExtensions: ['js', 'jsx', 'md', 'mdx', 'ts', 'tsx'],
  images: {
    remotePatterns: [{ protocol: 'https', hostname: 'truthful-champion-44ccd1b4ef.media.strapiapp.com' }, { protocol: "https", hostname: "**" }],
  },
  optimizeFonts: true,
  experimental: { optimizeCss: true },
  redirects: async () => [
    {
      source: '/blog/how-to-become-a-roofer',
      destination: '/careers/roofer/how-to-become',
      permanent: false,
    },
  ]
}

const withMDX = createMDX({
})

// Merge MDX config with Next.js config
export default withMDX(nextConfig)