import { Metadata } from 'next';
import { Footer, MobileNav, Navbar } from '@/components';
import { HeaderContainer, ListContainer } from '@/containers/calculators/main';
import { ShareContainer } from '@/containers/careers/construction';
import { NewsLetterContainerV2 } from '@/containers/common';
import { navbarData } from '@/data/common';

export const metadata: Metadata = {
  metadataBase: new URL(`${process.env.NEXT_PUBLIC_APP_URL}`),
  title: 'Construction Calculators',
  description: 'We have assembled a list of FREE construction calculators.',
  openGraph: {
    title: 'Construction Calculators',
    description: 'We have assembled a list of FREE construction calculators.',
  },
  twitter: {
    title: 'Construction Calculators',
    description: 'We have assembled a list of FREE construction calculators.',
  },
};

export default function Calculators() {
  return (
    <>
      <Navbar transparent links={navbarData.calculatorLinks} />
      <MobileNav transparent links={navbarData.calculatorLinks} />
      <main className="mt-[-85px]">
        <HeaderContainer />
        <ListContainer />
        <ShareContainer />
        <NewsLetterContainerV2 />
      </main>
      <Footer />
    </>
  );
}
