import { Metadata } from 'next';
import { Breadcrumb } from '@/components';
import { BrickCalculatorForm } from '@/containers/calculators/brick';

export async function generateMetadata(): Promise<Metadata> {
  return {
    title: 'Brick Calculator - Estimate quantity & cost',
    description:
      'Our free brick calculator will help you quickly calculate the quantity and cost of bricks, mortar and sand needed for your project.',
    openGraph: {
      title: 'Brick Calculator - Estimate quantity & cost',
      description:
        'Our free brick calculator will help you quickly calculate the quantity and cost of bricks, mortar and sand needed for your project.',
    },
  };
}

export default function BrickCalculator() {
  return (
    <main className="global-container px-[20px] py-[60px] xl:px-0">
      <header>
        <h1 className="text-3xl">Free Brick Calculator</h1>
        <Breadcrumb />
        <p className="mt-[20px]">
          Welcome to our Free Brick Calculator tool, designed to help you accurately estimate the
          number of bricks and bags of mortar needed for your construction project. Whether you're
          building a new wall, adding an extension, or tackling a renovation, our easy-to-use
          calculator takes the guesswork out of planning.
          <br />
          <br />
          Simply enter your project dimensions, brick size, and mortar details, and let our tool do
          the rest. This page provides step-by-step instructions, practical tips, and additional
          resources to ensure your project is a success. Start your calculation now and build with
          confidence!
        </p>
      </header>
      <BrickCalculatorForm />
    </main>
  );
}
