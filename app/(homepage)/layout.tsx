import { Footer, MobileNav, Navbar } from '@/components';
import { navbarData } from '@/data/common';

const { homeLinks } = navbarData;

export default function MainLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <>
      <div itemScope itemType="https://schema.org/WebSite">
        <meta itemProp="url" content="https://www.brawnhq.com/" />
        <meta itemProp="name" content="Brawn" />
      </div>
      <Navbar links={homeLinks} />
      <MobileNav links={homeLinks} />
      {children}
      <Footer />
    </>
  );
}
