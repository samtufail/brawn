import { NewsLetterContainer } from '@/containers/common';

export default function AboutPage() {
  return (
    <main className="global-container">
      <div className="mx-[15px] max-w-[1140px] pt-[60px] xl:mx-auto">
        {/* About */}
        <div className="mb-[60px]">
          <h1 className="text-center text-[36px] font-[700] leading-[52px] tracking-[-.48px] text-neutral90 md:text-left md:text-[56px] md:leading-[125%]">
            Contact Us
          </h1>
          <p className="mb-[10px] text-center text-[16px] font-[400] leading-[28px] text-neutral90 opacity-60 md:text-left md:text-[20px] md:leading-[34px]">
            We are here to support you. Please send us an email&nbsp;
            <strong>hello@brawnfinancial.com</strong>
            &nbsp;with any questions.
            <br />
            <br />
            We'll typically respond within 24 hours.
          </p>
        </div>
        {/* Team Companies */}
        <div className="mt-[40px]">
          <NewsLetterContainer />
        </div>
      </div>
    </main>
  );
}
