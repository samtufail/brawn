import { SecurityContainer } from '@/containers/common';
import {
  BottomLineContainer,
  BrawnHelpContainer,
  CareContainer,
  ChallengesContainer,
  Header,
  MentalContainer,
  SolutionContainer,
  TeamTrust,
  UpsellContainer,
} from '@/containers/employers';

export default function EmployersPage() {
  return (
    <main>
      <Header />
      <ChallengesContainer />
      <CareContainer />
      <MentalContainer />
      <BrawnHelpContainer />
      <BottomLineContainer />
      <TeamTrust />
      <SolutionContainer />
      <UpsellContainer />
      <SecurityContainer />
    </main>
  );
}
