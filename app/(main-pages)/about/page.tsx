import { NewsLetterContainer } from '@/containers/common';
import { TeamContainer } from '@/containers/home';

export default function AboutPage() {
  return (
    <main className="global-container">
      <div className="mx-[15px] max-w-[1140px] pt-[60px] xl:mx-auto">
        {/* About */}
        <div className="mb-[60px]">
          <h1 className="text-center text-[36px] font-[700] leading-[52px] tracking-[-.48px] text-neutral90 md:text-left md:text-[56px] md:leading-[125%]">
            About Brawn
          </h1>
          <p className="mb-[10px] text-center text-[16px] font-[400] leading-[28px] text-neutral90 opacity-60 md:text-left md:text-[20px] md:leading-[34px]">
            Blue collar workers deserve a better bank. They are the backbone of our economy. They
            build the roads we drive on, the schools our children attend, the offices we work in and
            the homes we live in.
            <br />
            <br />
            We're building a new financial services company that helps construction and trades men
            and woman manage their spending, save more money, and invest for the future.
          </p>
        </div>
        {/* Company */}
        <div>
          <h2 className="text-center text-[26px] font-[600] leading-[52px] tracking-[-.16px] text-neutral90 md:text-left md:text-[36px] md:leading-[135%]">
            You're in good company
          </h2>
          <p className="mb-[10px] text-center text-[16px] font-[400] leading-[28px] text-neutral90 opacity-60 md:text-left md:text-[20px] md:leading-[34px]">
            We come from a family of laborers, carpenters, welders and other trades. We&apos;ve seen
            the struggles you go through first hand. Our team is comprised of experts with decades
            of years of experience in financial management and banking.
          </p>
        </div>
        {/* Team Companies */}
        <TeamContainer />
        <div className="mt-[40px]">
          <NewsLetterContainer />
        </div>
      </div>
    </main>
  );
}
