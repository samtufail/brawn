import { Footer, MobileNav, Navbar } from '@/components';
import { navbarData } from '@/data/common';

const { homeLinks } = navbarData;

export default function MainLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <>
      <Navbar links={homeLinks} />
      <MobileNav links={homeLinks} />
      {children}
      <Footer />
    </>
  );
}
