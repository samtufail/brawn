import Link from 'next/link';
import Script from 'next/script';

const script = `
  // Tiktok
  ttq.track('CompleteRegistration', {
    "contents": [
      {
        "content_id": "wealth", // string. ID of the product. Example: "1077218".
        "content_type": "product", // string. Either product or product_group.
        "content_name": "Wealth" // string. The name of the page or product. Example: "shirt".
      }
    ]
  });
  // Reddit
  rdt('track', 'SignUp');
  `;

export default function SignUpCompletePage() {
  return (
    <main className="global-container">
      <Script dangerouslySetInnerHTML={{ __html: script }} id="trackConversion" />
      <div className="mx-[15px] max-w-[1140px] pt-[60px] xl:mx-auto">
        {/* About */}
        <div className="mb-[60px]">
          <h1 className="text-center text-[36px] font-[700] leading-[52px] tracking-[-.48px] text-neutral90 md:text-left md:text-[56px] md:leading-[125%]">
            Sign Up Complete
          </h1>
          <p className="mb-[10px] mt-10 text-center text-[16px] font-[400] leading-[28px] text-neutral90 opacity-60 md:text-left md:text-[20px] md:leading-[34px]">
            Thank you for your interest in Brawn. Our team will reach out shortly with next steps.
          </p>
          <p className="mb-[400px] text-center text-[16px] font-[400] leading-[28px] text-neutral90 opacity-60 md:text-left md:text-[20px] md:leading-[34px]">
            Should you have any questions please feel free to
            {' '}
            <Link href="/contact">contact us</Link>
            .
          </p>
        </div>
      </div>
    </main>
  );
}
