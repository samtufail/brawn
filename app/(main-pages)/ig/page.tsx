import Link from 'next/link';
import { Button } from '@/components';

export default function AboutPage() {
  return (
    <main className="global-container">
      <div className="mx-[15px] max-w-[1140px] pt-[60px] xl:mx-auto">
        {/* Instagram Social Page */}
        <div className="mb-[60px]">
          <h1 className="text-center text-[36px] font-[700] leading-[52px] tracking-[-.48px] text-neutral90 md:text-left md:text-[56px] md:leading-[125%]">
            We help blue collar workers achieve the American dream
          </h1>
          <div className="mt-10">
            <Link href="https://forms.gle/ut8Rk5oXnaeaemGWA">
              <Button variant="secondaryFullWidth">Submit Video</Button>
            </Link>
            <Link href="/">
              <Button variant="secondaryFullWidth">Retire Early In Construction</Button>
            </Link>
            <Link href="https://brawnhq.typeform.com/to/L1No6PmE">
              <Button variant="secondaryFullWidth">Join Our Newsletter</Button>
            </Link>
          </div>
        </div>
      </div>
    </main>
  );
}
