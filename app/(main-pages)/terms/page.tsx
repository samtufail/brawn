import { Metadata } from 'next';
import { getTermsOfUse } from '@/api/static';
import { MDXRemote } from 'next-mdx-remote/rsc';

export async function generateMetadata(): Promise<Metadata> {
  const termsOfUse = await getTermsOfUse();
  return {
    title: termsOfUse.attributes.metadata.metaTitle,
    description: termsOfUse.attributes.metadata.metaDescription,
  };
}

export default async function TermsOfUsePage() {
  const termsOfUse = await getTermsOfUse();
  return (
    <main className="global-container">
      <div className="mx-[15px] max-w-[1140px] pt-[60px] xl:mx-auto">
        {/* Privacy Policy */}
        <div className="mb-[60px]">
          <h1 className="text-center text-[36px] font-[700] leading-[52px] tracking-[-.48px] text-neutral90 md:text-left md:text-[56px] md:leading-[125%]">
            {termsOfUse.attributes.title}
          </h1>
          <p>
            Last Updated At:
            {termsOfUse.attributes.updatedAt.toString()}
          </p>
          <div className="mdx-body mt-[12px] text-[16px] leading-normal text-neutral20 xl:mt-[16px] xl:text-[18px]">
            <MDXRemote source={termsOfUse.attributes.body as any} />
          </div>
        </div>
      </div>
    </main>
  );
}
