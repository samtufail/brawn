import { NewsLetterContainer, SecurityContainer } from '@/containers/common';
import {
  AdHeaderContainer,
  FeaturesContainer,
  HelpContainer,
  MainFeatureContainer,
  OptionsContainer,
  TeamContainer,
} from '@/containers/home';
import { ProductType } from '@/data/home';

export default function CashBackPage() {
  return (
    <main>
      <AdHeaderContainer productType={ProductType.CASH_BACK} />
      <FeaturesContainer />
      <OptionsContainer />
      <HelpContainer />
      <MainFeatureContainer />
      <TeamContainer />
      <SecurityContainer />
      <NewsLetterContainer />
    </main>
  );
}
