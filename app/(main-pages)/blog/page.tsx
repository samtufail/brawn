import { Articles, Categories, Header } from '@/containers/blog';

export default function BlogPage() {
  return (
    <main>
      <Header />
      <Categories />
      <Articles />
    </main>
  );
}
