import { Fragment } from 'react';
import { Metadata } from 'next';
import { getArticle } from '@/api/blog';
import { Breadcrumb } from '@/components';
import { Header } from '@/containers/blog-article';
import {
  MediaContainer,
  PageLink,
  Quote,
  Section,
  Table,
  TOC,
  UpSell,
} from '@/containers/careers/career/article';
import { ShareButtons } from '@/containers/common';
import { Article, Block, BlockTypes } from '@/entities/articles.entity';
import clsx from 'clsx';
import { MDXRemote } from 'next-mdx-remote/rsc';

import { formatDate, kebabize } from '@/lib/utils';

type Props = {
  params: { slug: string; article: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const article = await getArticle(params.slug);
  return {
    title: `${article.attributes.title}`,
    description: article.attributes.description,
    openGraph: {
      images: `${article.attributes.cover.data.attributes.formats?.medium?.url}`,
    },
  };
}

function getBlock(article: Article, block: Block, page: string, index: number, state: string) {
  switch (block.__component) {
    case BlockTypes.SECTION:
      return (
        <Section
          key={block.id}
          {...block}
          body={<MDXRemote source={block.body as any} />}
          isLargeHeading={index === 0}
        />
      );
    case BlockTypes.QUOTE:
      return <Quote {...block} key={block.id} />;
    case BlockTypes.PAGE_LINK:
      return <PageLink {...block} key={block.id} />;
    case BlockTypes.UP_SELL:
      return (
        <UpSell
          name={
            article?.attributes?.career?.data?.attributes?.name ||
            article?.attributes?.category?.data?.attributes?.name
          }
          {...block}
          key={block.id}
        />
      );
    case BlockTypes.TABLE:
      return <Table {...block} {...article} key={block.id} page={page} state={state} />;
    case BlockTypes.MEDIA:
      return (
        <MediaContainer
          {...block}
          name={
            article?.attributes?.career?.data?.attributes?.name ||
            article?.attributes?.category?.data?.attributes?.name
          }
          key={block.id}
          isBlog
        />
      );
    default:
      return <div key={block.id} />;
  }
}

export default async function BlogArticlePage({
  params,
  searchParams: { page },
}: {
  params: { slug: string };
  searchParams: { page: string };
}) {
  const article = await getArticle(params.slug);
  const pageNumber = page || '1';
  return (
    <>
      <Header cover={article.attributes.cover} name={article.attributes.title} />
      <div className="global-container relative grid gap-[74px] px-[20px] pb-[40px] xl:grid-cols-[35px_1fr] xl:px-[60px] xl:pb-[80px]">
        <div className="hidden xl:block">
          <ShareButtons description={article.attributes.description} isBlog />
        </div>
        <div>
          <div
            className={clsx('mb-[24px] w-fit rounded-[2px] bg-opacity-[0.08] px-[16px] py-[6px]', {
              'bg-[#46b8ff]': article.attributes.category.data.attributes.name === 'workers',
              'bg-[#8958FE]': article.attributes.category.data.attributes.name === 'employers',
            })}
          >
            <span
              className={clsx('text-[14px] font-[600] capitalize tracking-[-0.14px]', {
                'text-[#46b8ff]': article.attributes.category.data.attributes.name === 'workers',
                'text-[#8958FE]': article.attributes.category.data.attributes.name === 'employers',
              })}
            >
              {article.attributes.category.data.attributes.name}
            </span>
          </div>
          {article.attributes.blocks.map((block, index) => {
            if (index === 0) {
              return (
                <Fragment key={block.id}>
                  <section id={kebabize(block.title || '')}>
                    {getBlock(article, block, pageNumber, index, '')}
                  </section>
                  <p className="mb-[52px] mt-[-28px] text-[14px] uppercase text-[#969BAB]">
                    UPDATED AT:&nbsp;
                    {formatDate(article.attributes.updatedAt)}
                  </p>
                </Fragment>
              );
            }
            if (index === 1) {
              return (
                <Fragment key={block.id}>
                  <div className="mb-[24px]">
                    <Breadcrumb isBlog />
                  </div>
                  <TOC blocks={article.attributes.blocks} />
                  <section id={kebabize(block.title || '')}>
                    {getBlock(article, block, pageNumber, index, '')}
                  </section>
                </Fragment>
              );
            }
            return (
              <section key={block.id} id={kebabize(block.title || '')}>
                {getBlock(article, block, pageNumber, index, '')}
              </section>
            );
          })}
        </div>
      </div>
    </>
  );
}
