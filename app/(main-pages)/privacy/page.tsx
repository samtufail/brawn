import { Metadata } from 'next';
import { getPrivacyPolicy } from '@/api/static';
import { MDXRemote } from 'next-mdx-remote/rsc';

export async function generateMetadata(): Promise<Metadata> {
  const privacyPolicy = await getPrivacyPolicy();
  return {
    title: privacyPolicy.attributes.metadata.metaTitle,
    description: privacyPolicy.attributes.metadata.metaDescription,
  };
}

export default async function PrivacyPage() {
  const privacyPolicy = await getPrivacyPolicy();
  return (
    <main className="global-container">
      <div className="mx-[15px] max-w-[1140px] pt-[60px] xl:mx-auto">
        {/* Privacy Policy */}
        <div className="mb-[60px]">
          <h1 className="text-center text-[36px] font-[700] leading-[52px] tracking-[-.48px] text-neutral90 md:text-left md:text-[56px] md:leading-[125%]">
            {privacyPolicy.attributes.title}
          </h1>
          <p>
            Last Updated At:
            {privacyPolicy.attributes.updatedAt.toString()}
          </p>
          <div className="mdx-body mt-[12px] text-[16px] leading-normal text-neutral20 xl:mt-[16px] xl:text-[18px]">
            <MDXRemote source={privacyPolicy.attributes.body as any} />
          </div>
        </div>
      </div>
    </main>
  );
}
