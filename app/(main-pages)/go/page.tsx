import { NewsLetterContainer, SecurityContainer } from '@/containers/common';
import {
  FeaturesContainer,
  HeaderContainer,
  HelpContainer,
  MainFeatureContainer,
  OptionsContainer,
  TeamContainer,
} from '@/containers/home';

export default function GoPage() {
  return (
    <main>
      <HeaderContainer />
      <FeaturesContainer />
      <OptionsContainer />
      <HelpContainer />
      <MainFeatureContainer />
      <TeamContainer />
      <SecurityContainer />
      <NewsLetterContainer />
    </main>
  );
}
