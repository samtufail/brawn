import { MetadataRoute } from 'next';
import { getAllBlogArticles } from '@/api/blog';
import { getCareers } from '@/api/careers';
import { states } from '@/data/career-hub';

const subPages = [
  'overview',
  'salary',
  'how-to-become',
  'schools',
  'certifications-licenses',
  'tools-equipment',
];

interface SitemapFileProps {
  url: string;
  lastModified?: string | Date | undefined;
  changeFrequency?:
    | 'always'
    | 'hourly'
    | 'daily'
    | 'weekly'
    | 'monthly'
    | 'yearly'
    | 'never'
    | undefined;
  priority?: number | undefined;
}

const list = [
  '/',
  '/about',
  '/contact',
  '/privacy',
  '/blog',
  '/terms',
  '/careers/construction',
  '/calculators',
  '/calculators/brick',
];

export default async function sitemap(): Promise<MetadataRoute.Sitemap> {
  const prefix = `${process.env.NEXT_PUBLIC_APP_URL}`;
  const careers = await getCareers();
  const articles = await getAllBlogArticles();
  const staticLinks: SitemapFileProps[] = list.map((el) => ({
    url: `${prefix}${el}`,
    lastModified: new Date().toISOString(),
    changeFrequency: el === '/careers/construction' ? 'always' : 'monthly',
  }));
  const dynamicLinks = careers?.map((career) => {
    const links: SitemapFileProps[] = subPages.map((subPage) => ({
      url: `${prefix}/careers/${career.attributes.slug}/${subPage}`,
      lastModified: career.attributes.updatedAt,
      changeFrequency: 'always',
    }));
    return links;
  });
  const schoolLinks = careers?.map((career) => {
    const links: SitemapFileProps[] = states.map((state) => ({
      url: `${prefix}/careers/${career.attributes.slug}/schools/${state.value}`,
      lastModified: career.attributes.updatedAt,
      changeFrequency: 'always',
    }));
    return links;
  });
  const salaryLinks = careers?.map((career) => {
    const links: SitemapFileProps[] = states.map((state) => ({
      url: `${prefix}/careers/${career.attributes.slug}/salary/${state.value}`,
      lastModified: career.attributes.updatedAt,
      changeFrequency: 'always',
    }));
    return links;
  });
  const blogLinks: SitemapFileProps[] = articles?.data?.map((article) => ({
    url: `${prefix}/blog/${article.attributes.slug}`,
    lastModified: article.attributes.updatedAt,
    changeFrequency: 'always',
  }));
  const mergedCareerLinksArray = dynamicLinks.reduce((result, array) => result.concat(array), []);
  const mergedSchoolLinksArray = schoolLinks.reduce((result, array) => result.concat(array), []);
  const mergedSalaryLinksArray = salaryLinks.reduce((result, array) => result.concat(array), []);
  const finalLinks = [
    ...staticLinks,
    ...blogLinks,
    ...mergedCareerLinksArray,
    ...mergedSchoolLinksArray,
    ...mergedSalaryLinksArray,
  ];
  return finalLinks;
}
