import { Fragment } from 'react';
import { getArticleByCareerPage } from '@/api/careers';
import {
  FrequentlyAskedQuestions,
  MediaContainer,
  PageLink,
  Quote,
  Section,
  Table,
  TOC,
  UpSell,
} from '@/containers/careers/career/article';
import { Article, Block, BlockTypes } from '@/entities/articles.entity';
import { MDXRemote } from 'next-mdx-remote/rsc';

import { kebabize } from '@/lib/utils';

function getBlock(article: Article, block: Block, page: string, index: number, state: string) {
  switch (block.__component) {
    case BlockTypes.SECTION:
      return (
        <Section
          key={block.id}
          {...block}
          body={<MDXRemote source={block.body as any} />}
          isLargeHeading={index === 0}
        />
      );
    case BlockTypes.QUOTE:
      return <Quote {...block} key={block.id} />;
    case BlockTypes.PAGE_LINK:
      return <PageLink {...block} key={block.id} />;
    case BlockTypes.UP_SELL:
      return (
        <UpSell name={article.attributes.career.data.attributes.name} {...block} key={block.id} />
      );
    case BlockTypes.TABLE:
      return <Table {...block} {...article} key={block.id} page={page} state={state} />;
    case BlockTypes.MEDIA:
      return (
        <MediaContainer
          {...block}
          name={article.attributes.career.data.attributes.name}
          key={block.id}
        />
      );
    default:
      return <div key={block.id} />;
  }
}

export default async function CareerPage({
  params,
  searchParams: { page },
}: {
  params: { slug: string; article: string };
  searchParams: { page: string };
}) {
  const article = await getArticleByCareerPage(params.slug, params.article);
  const faqs = article.attributes.career.data.attributes.faqs.data.filter(
    (el) => el.attributes.section === params.article,
  );
  const pageNumber = page || '1';
  return (
    <div>
      {article.attributes.blocks.map((block, index) => {
        if (index === 1) {
          return (
            <Fragment key={block.id}>
              <TOC blocks={article.attributes.blocks} />
              <section id={kebabize(block.title || '')}>
                {getBlock(article, block, pageNumber, index, '')}
              </section>
            </Fragment>
          );
        }
        return (
          <section key={block.id} id={kebabize(block.title || '')}>
            {getBlock(article, block, pageNumber, index, '')}
          </section>
        );
      })}
      <FrequentlyAskedQuestions faqs={{ data: faqs }} article={params.article} />
    </div>
  );
}
