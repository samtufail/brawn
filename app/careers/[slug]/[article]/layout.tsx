import { Metadata } from 'next';
import { getArticleByCareerPage } from '@/api/careers';
import { Breadcrumb } from '@/components';
import { HeaderContainer } from '@/containers/careers/career/header.container';
import { Sidebar } from '@/containers/careers/career/sidebar.container';
import { ShareButtons } from '@/containers/common';

type Props = {
  params: { slug: string; article: string };
};

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const article = await getArticleByCareerPage(params.slug, params.article);

  const {
    title,
    description,
    career: {
      data: {
        attributes: {
          thumbnail: {
            data: {
              attributes: { url },
            },
          },
        },
      },
    },
  } = article.attributes;

  return {
    title,
    description,
    openGraph: {
      images: `${url}`,
    },
  };
}

export default async function Layout({
  children,
  params,
}: {
  children: React.ReactNode;
  params: { slug: string; article: string };
}) {
  const article = await getArticleByCareerPage(params.slug, params.article);
  const { description } = article.attributes;
  return (
    <>
      <div className="mt-[-85px] xl:mt-0">
        <HeaderContainer article={article} />
      </div>
      <div className="relative flex flex-col-reverse xl:mt-[48px] xl:grid xl:grid-cols-[1fr_270px]">
        <div className="absolute">
          <ShareButtons description={description} />
        </div>
        {/* Left  Side */}
        <div className="px-[20px] py-[40px] xl:px-[74px] xl:py-0">
          {/* Breadcrumbs */}
          <div className="mb-[32px]">
            <Breadcrumb />
          </div>
          {/* Page Elements */}
          {children}
        </div>
        {/* Right Side */}
        <div>
          <Sidebar slug={params.slug} article={article} />
        </div>
      </div>
    </>
  );
}
