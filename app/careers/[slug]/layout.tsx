import { MobileNav, Navbar } from '@/components';
import { ShareContainer } from '@/containers/careers/construction';
import { NewsLetterContainerV2 } from '@/containers/common';
import { navbarData } from '@/data/common';

export default async function Layout({ children }: { children: React.ReactNode }) {
  return (
    <>
      <Navbar hideBorder links={navbarData.careerDesktopLinks} />
      <MobileNav transparent links={navbarData.careerMobileLinks} />
      <main className="global-container">
        {children}
        <ShareContainer />
        <NewsLetterContainerV2 />
      </main>
    </>
  );
}
