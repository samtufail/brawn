import { redirect } from 'next/navigation';

export default async function CareerPage({ params }: { params: { slug: string } }) {
  redirect(`/careers/${params.slug}/overview`);
}
