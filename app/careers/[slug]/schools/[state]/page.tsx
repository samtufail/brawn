import { Fragment } from 'react';
import { Metadata } from 'next';
import { getArticleByCareerPage, getCareerSchoolsByState } from '@/api/careers';
import {
  MediaContainer,
  PageLink,
  Quote,
  Section,
  Table,
  TOC,
  UpSell,
} from '@/containers/careers/career/article';
import { constructSchoolByStateBlocks } from '@/data/career-hub';
import { Article } from '@/entities';
import { Block, BlockTypes } from '@/entities/articles.entity';

import { capitalizeSlug, kebabize, toTitleCase } from '@/lib/utils';

type Props = {
  params: { slug: string; state: string };
};

export async function generateMetadata({ params: { slug, state } }: Props): Promise<Metadata> {
  const career = toTitleCase(capitalizeSlug(slug));
  const stateString = toTitleCase(capitalizeSlug(state));
  return {
    title: `${career} Schools in ${stateString}`,
    description: `In this article we summarize the top ${stateString}'s top ${career} schools.`,
  };
}

function getBlock(article: Article, block: Block, page: string, index: number, state: string) {
  switch (block.__component) {
    case BlockTypes.SECTION:
      return (
        <Section
          key={block.id}
          {...block}
          body={<div className="mdx-body">{block.body}</div>}
          isLargeHeading={index === 0}
        />
      );
    case BlockTypes.QUOTE:
      return <Quote {...block} key={block.id} />;
    case BlockTypes.PAGE_LINK:
      return <PageLink {...block} key={block.id} />;
    case BlockTypes.UP_SELL:
      return (
        <UpSell name={article.attributes.career.data.attributes.name} {...block} key={block.id} />
      );
    case BlockTypes.TABLE:
      return <Table {...block} {...article} key={block.id} page={page} state={state} />;
    case BlockTypes.MEDIA:
      return (
        <MediaContainer
          {...block}
          name={article.attributes.career.data.attributes.name}
          key={block.id}
        />
      );
    default:
      return <div key={block.id} />;
  }
}

export default async function SchoolsState({
  params: { slug, state },
  searchParams: { page },
}: {
  params: { slug: string; state: string };
  searchParams: { page: string };
}) {
  const article = await getArticleByCareerPage(slug, 'overview');
  const pageNumber = page || '1';
  const career = toTitleCase(capitalizeSlug(slug));
  const stateString = toTitleCase(capitalizeSlug(state));
  const schools = await getCareerSchoolsByState(slug, pageNumber, stateString);
  const blocks = constructSchoolByStateBlocks(career, slug, stateString, article.attributes.blocks);
  return (
    <section>
      {blocks.map((blockEl, index) => {
        const block = getBlock(article, blockEl, pageNumber, index, stateString);
        if (index === 1) {
          return (
            <Fragment key={blockEl.id}>
              <TOC blocks={blocks} />
              <section id={kebabize(blockEl.title || '')}>{block}</section>
              <p className="font-[600] text-neutral90">{`Here are the Top ${schools.meta.pagination.total} ${career} Schools in ${stateString} state:`}</p>
            </Fragment>
          );
        }
        if (index === 4) {
          return (
            <Fragment key={blockEl.id}>
              <section id={kebabize(blockEl.title || '')}>
                {block}
                <p className="mb-[24px]">
                  {`If you want to learn more about what it takes to become a ${career}, check out these articles:`}
                </p>
              </section>
            </Fragment>
          );
        }
        return (
          <section key={blockEl.id} id={kebabize(blockEl.title || '')}>
            {block}
          </section>
        );
      })}
    </section>
  );
}
