import { redirect } from 'next/navigation';

export default function CareersPage() {
  redirect('/careers/construction');
}
