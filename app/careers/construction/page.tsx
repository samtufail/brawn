import { Metadata } from 'next';
import { getCareers } from '@/api/careers';
import { MobileNav, Navbar } from '@/components';
import {
  HeaderContainer,
  HighestLowestContainer,
  ListContainer,
  RetireContainer,
  ShareContainer,
} from '@/containers/careers/construction';
import { NewsLetterContainerV2 } from '@/containers/common';
import { navbarData } from '@/data/common';

export async function generateMetadata(): Promise<Metadata> {
  const careers = await getCareers();
  return {
    title: `The Top ${careers.length} Construction Careers`,
    description: `We've put a list of the top ${careers.length} construction careers together. Learn more about careers such as Roofer, Electrician, Carpenter, Crane Operator and more.`,
  };
}

export default function ConstructionCareer() {
  return (
    <>
      <Navbar transparent links={navbarData.careerDesktopLinks} />
      <MobileNav transparent links={navbarData.careerMobileLinks} />
      <main className="mt-[-85px]">
        <HeaderContainer />
        <ListContainer />
        <HighestLowestContainer isHighest />
        <HighestLowestContainer isHighest={false} />
        <RetireContainer />
        <ShareContainer />
        <NewsLetterContainerV2 />
      </main>
    </>
  );
}
