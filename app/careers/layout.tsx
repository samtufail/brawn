import { Footer } from '@/components';

export default async function Layout({ children }: { children: React.ReactNode }) {
  return (
    <div className="overflow-hidden">
      {children}
      <Footer />
    </div>
  );
}
