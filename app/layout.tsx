import type { Metadata, Viewport } from 'next';
import { Poppins } from 'next/font/google';
import { Partytown } from '@builder.io/partytown/react';

import '@/styles/globals.css';

import Image from 'next/image';
import Script from 'next/script';

const poppins = Poppins({
  subsets: ['latin'],
  weight: ['400', '500', '600', '700'],
  variable: '--font-poppins',
});

export const viewport: Viewport = {
  initialScale: 1,
  width: 'device-width',
};

export const metadata: Metadata = {
  metadataBase: new URL(`${process.env.NEXT_PUBLIC_APP_URL}`),
  title: 'Brawn - Saving. Retirement. Advice.',
  description:
    'We help construction and trades workers save more, spend smarter and invest for the future. Brawn provides working class Americans with tools and advice to help them grow financially.',
  openGraph: {
    title: 'Brawn - Saving. Retirement. Advice.',
    description:
      'We help construction and trades workers save more, spend smarter and invest for the future. Brawn provides working class Americans with tools and advice to help them grow financially.',
    images: '/images/og-image.jpg',
    type: 'website',
    siteName: 'Brawn',
  },
  twitter: {
    title: 'Brawn - Saving. Retirement. Advice.',
    description:
      'We help construction and trades workers save more, spend smarter and invest for the future. Brawn provides working class Americans with tools and advice to help them grow financially.',
    images: '/images/og-image.jpg',
  },
};

const script = `
  // Google Tag Manager

  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-RVV5RDXVF6');

  // Hotjar
  // (function(h,o,t,j,a,r){
  //   h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
  //   h._hjSettings={hjid:3908615,hjsv:6};
  //   a=o.getElementsByTagName('head')[0];
  //   r=o.createElement('script');r.async=1;
  //   r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
  //   a.appendChild(r);
  // })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');

  // Reddit
  !function(w,d){if(!w.rdt){var p=w.rdt=function(){p.sendEvent?p.sendEvent.apply(p,arguments):p.callQueue.push(arguments)};p.callQueue=[];var t=d.createElement("script");t.src="https://www.redditstatic.com/ads/pixel.js",t.async=!0;var s=d.getElementsByTagName("script")[0];s.parentNode.insertBefore(t,s)}}(window,document);rdt('init','a2_ezscp1dp3nqn');rdt('track', 'PageVisit');

  // TikTok
  !function (w, d, t) { w.TiktokAnalyticsObject=t;var ttq=w[t]=w[t]||[];ttq.methods=["page","track","identify","instances","debug","on","off","once","ready","alias","group","enableCookie","disableCookie"],ttq.setAndDefer=function(t,e){t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}};for(var i=0;i<ttq.methods.length;i++)ttq.setAndDefer(ttq,ttq.methods[i]);ttq.instance=function(t){for(var e=ttq._i[t]||[],n=0;n<ttq.methods.length;n++)ttq.setAndDefer(e,ttq.methods[n]);return e},ttq.load=function(e,n){var i="https://analytics.tiktok.com/i18n/pixel/events.js";ttq._i=ttq._i||{},ttq._i[e]=[],ttq._i[e]._u=i,ttq._t=ttq._t||{},ttq._t[e]=+new Date,ttq._o=ttq._o||{},ttq._o[e]=n||{};var o=document.createElement("script");o.type="text/javascript",o.async=!0,o.src=i+"?sdkid="+e+"&lib="+t;var a=document.getElementsByTagName("script")[0];a.parentNode.insertBefore(o,a)}; ttq.load('CNT3ECRC77U5M8RPI5L0'); ttq.page(); }(window, document, 'ttq');

  // LinedIn
  _linkedin_partner_id = "5998354";
  window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
  window._linkedin_data_partner_ids.push(_linkedin_partner_id);

  // Snapchat
  // (function(l) {
  //   if (!l){window.lintrk = function(a,b){window.lintrk.q.push([a,b])};
  //   window.lintrk.q=[]}
  //   var s = document.getElementsByTagName("script")[0];
  //   var b = document.createElement("script");
  //   b.type = "text/javascript";b.async = true;
  //   b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
  //   s.parentNode.insertBefore(b, s);})(window.lintrk);
  `;

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en" className={`${poppins.variable} ${poppins.className}`}>
      <head>
        <Partytown debug forward={['dataLayer.push']} />
        <Script
          type="text/partytown"
          src="https://www.googletagmanager.com/gtag/js?id=G-RVV5RDXVF6"
        />
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png" />
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png" />
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png" />
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png" />
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png" />
        <link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196" />
        <link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96" />
        <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32" />
        <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16" />
        <link rel="icon" type="image/png" href="/favicon-128.png" sizes="128x128" />
        <meta name="application-name" content="Brawn" />
        <meta name="msapplication-TileColor" content="#FFFFFF" />
        <meta name="msapplication-TileImage" content="/mstile-144x144.png" />
        <meta name="msapplication-square70x70logo" content="/mstile-70x70.png" />
        <meta name="msapplication-square150x150logo" content="/mstile-150x150.png" />
        <meta name="msapplication-wide310x150logo" content="/mstile-310x150.png" />
        <meta name="msapplication-square310x310logo" content="/mstile-310x310.png" />
        <Script dangerouslySetInnerHTML={{ __html: script }} type="text/partytown" id="adScript" />
        <noscript>
          <Image
            height={1}
            width={1}
            style={{ display: 'none' }}
            alt="Fail Safe Image"
            src="https://px.ads.linkedin.com/collect/?pid=5998354&fmt=gif"
          />
        </noscript>
      </head>
      <body className="text-[#121212]">{children}</body>
    </html>
  );
}
