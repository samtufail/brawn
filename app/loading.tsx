import { Loader } from '@/icons';

export default function Loading() {
  return (
    <div className="flex h-screen w-screen items-center justify-center">
      <div className="size-[50px]">
        <Loader />
      </div>
    </div>
  );
}
