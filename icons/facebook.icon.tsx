export function FacebookIcon() {
  return (
    <svg width="35" height="24" viewBox="0 0 35 24" fill="none" xmlns="http://www.w3.org/2000/svg">
      <rect width="35" height="24" rx="2" fill="#1877F2" />
      <g clipPath="url(#clip0_820_477)">
        <path
          d="M15.5938 13.6016V19.8907H18.4062V13.6016H20.5033L20.9023 11H18.4062V9.3125C18.4062 8.60006 18.755 7.90625 19.873 7.90625H21.0078V5.69141C21.0078 5.69141 19.9776 5.51562 18.9932 5.51562C16.9374 5.51562 15.5938 6.76156 15.5938 9.01719V11H13.3086V13.6016H15.5938Z"
          fill="white"
        />
      </g>
      <defs>
        <clipPath id="clip0_820_477">
          <rect width="18" height="18" fill="white" transform="translate(8 2)" />
        </clipPath>
      </defs>
    </svg>
  );
}
