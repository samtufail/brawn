export function LogoArrowRight() {
  return (
    <svg width="17" height="16" viewBox="0 0 17 16" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M16.2473 8.54404L11.014 0.0211153H6.60937L11.8412 8.54404L6.99609 15.9785H11.4007L16.2473 8.54404Z"
        fill="#8958FE"
      />
      <path
        d="M9.63791 8.54404L4.40611 0.0211153H0L5.23329 8.54404L0.386714 15.9785H4.79283L9.63791 8.54404Z"
        fill="#8958FE"
      />
    </svg>
  );
}
