import { Article, Career } from '@/entities';
import { FAQs, ToolData } from '@/entities/careers.entity';
import { Certificate } from '@/entities/certifications.entity';
import { Salaries, Salary } from '@/entities/salaries.entity';
import { Schools } from '@/entities/schools.entity';

import { sortHighestCareersFirst, sortLowestCareersFirst, toTitleCase } from '@/lib/utils';

import { strapi } from './fetch';

type Sort = 'lowest' | 'highest' | '';

export const getCareers = async (sort?: Sort): Promise<Career[]> => {
  const data = await strapi('/careers?populate[0]=articles&populate[1]=thumbnail');
  const finalData: Career[] = data?.data;
  switch (sort) {
    case 'highest':
      return sortHighestCareersFirst(finalData);
    case 'lowest':
      return sortLowestCareersFirst(finalData);
    default:
      return finalData;
  }
};

export const getArticle = async (slug: string): Promise<Article> => {
  const data = await strapi(`/articles?filters[slug][$eq]=${slug}&populate=*`);
  return data?.data[0];
};

export const getCareerWithSalaries = async (slug: string): Promise<Salary[]> => {
  const data = await strapi(`/careers?populate[salaries][populate]=*&filters[slug][$eq]=${slug}`);
  return data.data[0]?.attributes?.salaries?.data;
};

export const getTools = async (slug: string, type: string): Promise<ToolData[]> => {
  const data = await strapi(`/tools?filters[career][slug][$eq]=${slug}&filters[type][$eq]=${type}`);
  return data.data;
};

export const getArticleByCareerPage = async (slug: string, article: string): Promise<Article> => {
  const url = `/articles?populate[blocks][populate]=*&populate[career][populate]=faqs&populate[career][populate]=thumbnail&populate[cover][populate]=&filters[$and][0][career][slug][$eq]=${slug}&filters[$and][1][careerPage][$eq]=${article}`;
  const data = await strapi(url);
  const finalData = data?.data?.filter(
    (el: Article) => el.attributes.careerPage === article,
  ) as Article[];
  return finalData[0];
};

export const getFAQs = async (slug: string): Promise<FAQs> => {
  const data = await strapi(`/faqs?filters[career][slug][$eq]=${slug}`);
  return data;
};

export const getCareer = async (slug: string): Promise<Career> => {
  const data = await strapi(`/careers?filters[slug][$eq]=${slug}&populate=*`);
  return data?.data[0];
};

export const getCareerSchools = async (slug: string, page: string): Promise<Schools> => {
  const data = await strapi(
    `/schools?filters[career][slug][$eq]=${slug}&pagination[pageSize]=20&pagination[page]=${page}&sort[1]=state:asc`,
  );
  return data;
};

export const getCareerSchoolsByState = async (
  slug: string,
  page: string,
  state: string,
): Promise<Schools> => {
  const data = await strapi(
    `/schools?filters[career][slug][$eq]=${slug}&filters[state][$eq]=${toTitleCase(state)}&pagination[pageSize]=20&pagination[page]=${page}&sort[1]=city:asc`,
  );
  return data;
};

export const getCareerSalaryByState = async (
  slug: string,
  page: string,
  state: string,
): Promise<Salaries> => {
  const data = await strapi(
    `/salaries?filters[career][slug][$eq]=${slug}&filters[state][$eq]=${toTitleCase(state)}&pagination[pageSize]=100&pagination[page]=${page}`,
  );
  return data;
};

export const getCareerCertifications = async (slug: string): Promise<Certificate[]> => {
  const data = await strapi(`/certifications?filters[career][slug][$eq]=${slug}`);
  return data?.data;
};
