import { StaticData } from '@/entities';

import { strapi } from './fetch';

export const getTermsOfUse = async (): Promise<StaticData> => {
  const data = await strapi('/terms?populate[0]=metadata');
  return data?.data;
};

export const getPrivacyPolicy = async (): Promise<StaticData> => {
  const data = await strapi('/privacy?populate[0]=metadata');
  return data?.data;
};
