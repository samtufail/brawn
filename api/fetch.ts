export const strapi = async (path: string) => {
  const res = await fetch(`${process.env.NEXT_PUBLIC_STRAPI_API}${path}`, {
    next: { revalidate: 10 },
  });
  const data = await res.json();
  return data;
};
