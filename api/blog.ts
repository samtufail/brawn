import { Article, Articles, Category } from '@/entities/articles.entity';

import { strapi } from './fetch';

export async function getArticle(slug: string): Promise<Article> {
  const url = `/articles?filters[$and][0][articleType][$eq]=blog&filters[$and][1][slug][$eq]=${slug}&populate[career][populate]=*&populate[category][populate]=&populate[blocks][populate]=*&populate[cover][populate]=`;
  const data = await strapi(url);
  return data?.data[0];
}

export const getFeaturedBlogArticles = async (): Promise<Articles> => {
  const url =
    '/articles?populate[category][populate]&populate[cover][populate]=&filters[$and][0][articleType][$eq]=blog&filters[$and][1][featured][$eq]=true';
  const data = await strapi(url);
  return data;
};

export const getCategories = async (): Promise<{ data: Category[] }> => {
  const url = '/categories';
  const data = await strapi(url);
  return data;
};

export const getAllBlogArticles = async (): Promise<Articles> => {
  const url =
    '/articles?populate[category][populate]=&populate[cover][populate]=&filters[$and][0][articleType][$eq]=blog';
  const data = await strapi(url);
  return data;
};
