import Image from 'next/image';

interface OptionCardProps {
  image: string;
  title: string;
  alt: string;
}

export function OptionCard({ image, title, alt }: OptionCardProps) {
  return (
    <div className="flex w-full items-center gap-[16px] rounded-lg border border-solid border-neutral30 px-[20px] py-[16px] font-[500]">
      <div className="relative size-[48px]">
        <Image
          fill
          style={{ objectFit: 'contain', objectPosition: 'center' }}
          src={image}
          alt={alt}
          sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
        />
      </div>
      <p className="text-[16px] font-[500] leading-[1.4] text-[#393c47] md:text-[18px]">{title}</p>
    </div>
  );
}
