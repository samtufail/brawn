'use client';

import Link from 'next/link';
import { usePathname } from 'next/navigation';

import { capitalizeSlug } from '@/lib/utils';

export function Breadcrumb({ isBlog = false }: { isBlog?: boolean }) {
  const pathname = usePathname();
  const path = isBlog ? `/home/${pathname}` : pathname;
  const segments = path.split('/').filter((item) => item !== '');

  return (
    <div>
      {segments.map((segment, index) => {
        const isLast = index === segments.length - 1;
        const link = `/${segments.slice(0, index + 1).join('/')}`;
        return (
          <span key={segment} className="text-[14px] capitalize leading-[1.7] text-neutral70">
            {isLast ? (
              capitalizeSlug(segment)
            ) : (
              <Link
                prefetch={false}
                href={
                  isBlog && link === '/home'
                    ? '/'
                    : isBlog && link === '/home/blog'
                      ? '/blog'
                      : link
                }
                className="hover:text-neutral90"
              >
                {capitalizeSlug(segment)}
              </Link>
            )}
            &nbsp;
            {!isLast ? '/' : null}
            &nbsp;
          </span>
        );
      })}
    </div>
  );
}
