'use client';

import { useEffect } from 'react';
import { motion, useAnimation } from 'framer-motion';
import { useInView } from 'react-intersection-observer';

interface SlideProps {
  x?: number;
  children: React.ReactNode;
  left?: boolean;
  duration?: number;
  className?: string;
}

export function Slide({ x = 20, children, left, duration = 0.5, className = '' }: SlideProps) {
  const [ref, inView] = useInView();
  const controls = useAnimation();
  useEffect(() => {
    if (inView) {
      controls.start('visible');
    }
  }, [controls, inView]);

  return (
    <motion.div
      ref={ref}
      animate={controls}
      initial="hidden"
      className={`${className} overflow-hidden `}
      variants={{
        visible: {
          x: 0,
          opacity: 1,
          transition: { duration, type: 'keyframes' },
        },
        hidden: {
          x: left ? -x : x,
          opacity: 0,
          transition: { duration, type: 'keyframes' },
        },
      }}
    >
      {children}
    </motion.div>
  );
}

interface ZoomProps {
  children: React.ReactNode;
  duration?: number;
  className?: string;
}

export function Zoom({ duration = 0.5, children, className }: ZoomProps) {
  const [ref, inView] = useInView();
  const controls = useAnimation();

  useEffect(() => {
    if (inView) {
      controls.start('visible');
    }
  }, [controls, inView]);

  return (
    <motion.div
      ref={ref}
      animate={controls}
      initial="hidden"
      className={`overflow-hidden ${className}`}
      variants={{
        visible: {
          scale: 1,
          opacity: 1,
          transition: { duration, type: 'keyframes' },
        },
        hidden: {
          scale: 0,
          opacity: 0,
          transition: { duration, type: 'keyframes' },
        },
      }}
    >
      {children}
    </motion.div>
  );
}
