'use client';

import { usePathname, useRouter, useSearchParams } from 'next/navigation';

import {
  PaginationContent,
  Pagination as PaginationElement,
  PaginationItem,
  PaginationLink,
  PaginationNext,
  PaginationPrevious,
} from './pagination-elements.component';

interface PaginationProps {
  pageCount: number;
}

function PLink({ value, page }: { value: number; page: number }) {
  const pathname = usePathname();
  const router = useRouter();
  return (
    <PaginationItem>
      <PaginationLink
        isActive={value === page}
        onClick={() =>
          router.push(`${process.env.NEXT_PUBLIC_APP_URL}/${pathname}?page=${value}`, {
            scroll: false,
          })
        }
      >
        {value}
      </PaginationLink>
    </PaginationItem>
  );
}

export function Pagination({ pageCount }: PaginationProps) {
  const searchParams = useSearchParams();
  const pageParam = searchParams.get('page') || '';
  const page = parseInt(pageParam, 10) || 1;
  const router = useRouter();
  const pathname = usePathname();
  const arr = Array.from(Array(pageCount).keys());

  return (
    <PaginationElement>
      <PaginationContent>
        <PaginationItem>
          <PaginationPrevious
            onClick={() =>
              router.push(`${process.env.NEXT_PUBLIC_APP_URL}/${pathname}?page=${page - 1}`, {
                scroll: false,
              })
            }
            aria-disabled={page === 1}
          />
        </PaginationItem>
        {arr.length < 10 &&
          arr.map((value) => (
            <PLink key={`id-pagination-${value}`} value={value + 1} page={page} />
          ))}
        <PaginationItem>
          <PaginationNext
            onClick={() =>
              router.push(`${process.env.NEXT_PUBLIC_APP_URL}/${pathname}?page=${page + 1}`, {
                scroll: false,
              })
            }
            aria-disabled={page === pageCount}
          />
        </PaginationItem>
      </PaginationContent>
    </PaginationElement>
  );
}
