'use client';

import { Field, FieldProps } from 'formik';
import SelectElement from 'react-select';

interface InputWithSelectProps {
  selectName: string;
  label: string;
  inputName: string;
  inputPlaceholder?: string;
  selectPlaceholder?: string;
  options: { value: string | number; label: string }[];
}

export function InputWithSelect({
  selectPlaceholder,
  selectName,
  label,
  options,
  inputName,
  inputPlaceholder,
}: InputWithSelectProps) {
  return (
    <div className="flex items-center gap-[30px]">
      <div>
        <label className="text-base" htmlFor={selectName}>
          {label}
        </label>
        <div>&nbsp;</div>
      </div>
      <div className="flex items-center gap-[15px]">
        {/* Input */}
        <Field name={inputName}>
          {({ field, form, meta }: FieldProps) => (
            <div>
              <input
                value={field.value}
                type="number"
                onChange={(e) => form.setFieldValue(inputName, e.target.value)}
                placeholder={inputPlaceholder}
                className="h-[48px] w-full rounded-[4px] border border-solid border-[hsl(0,_0%,_80%)] px-[12px] text-left text-[16px] leading-normal text-neutral20 placeholder:text-[hsl(0,_0%,_50%)] focus:outline-none xl:w-[275px] xl:grow"
              />
              {meta.touched && meta.error ? (
                <div className="error w-full">{meta.error}</div>
              ) : (
                <div>&nbsp;</div>
              )}
            </div>
          )}
        </Field>
        {/* Select */}
        <Field name={selectName}>
          {({ field, form, meta }: FieldProps) => (
            <div>
              <SelectElement
                options={options}
                placeholder={selectPlaceholder}
                className="w-full rounded-[4px] text-left text-[16px] leading-normal text-neutral20 focus:outline-none xl:w-[150px] xl:grow"
                classNames={{
                  control: () => 'h-[48px]',
                }}
                isSearchable={false}
                value={options?.find((option) => option.value === field.value)}
                onChange={(e) => form.setFieldValue(selectName, e?.value)}
              />
              {meta.touched && meta.error ? (
                <div className="error w-full">{meta.error}</div>
              ) : (
                <div>&nbsp;</div>
              )}
            </div>
          )}
        </Field>
      </div>
    </div>
  );
}
