'use client';

import { Field, FieldProps } from 'formik';
import SelectElement from 'react-select';

interface SelectProps {
  name: string;
  label: string;
  placeholder?: string;
  options: { value: number | string; label: string }[];
}

export function Select({ placeholder, name, label, options }: SelectProps) {
  return (
    <Field name={name}>
      {({ field, form, meta }: FieldProps) => (
        <div>
          <div className="flex items-center gap-[24px]">
            <div>
              <label className="text-base" htmlFor={name}>
                {label}
              </label>
              <div>&nbsp;</div>
            </div>
            <div>
              <SelectElement
                options={options}
                placeholder={placeholder}
                value={options?.find((option) => option.value === field.value)}
                className="w-full rounded-[4px] text-left text-[16px] leading-normal text-neutral20 focus:outline-none xl:w-[369px] xl:grow"
                classNames={{
                  control: () => 'h-[48px]',
                }}
                onChange={(e) => form.setFieldValue(name, e?.value)}
              />
              {meta.touched && meta.error ? (
                <div className="error w-full">{meta.error}</div>
              ) : (
                <div>&nbsp;</div>
              )}
            </div>
          </div>
        </div>
      )}
    </Field>
  );
}
