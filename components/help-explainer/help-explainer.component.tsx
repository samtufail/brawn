import Image from 'next/image';
import clsx from 'clsx';

interface HelpExplainerProps {
  title: string;
  description: string;
  image: string;
  right: boolean;
}

export function HelpExplainer({ title, description, image, right }: HelpExplainerProps) {
  return (
    <div
      className={`flex flex-col-reverse justify-between ${right ? 'md:flex-row-reverse' : 'md:flex-row'}`}
    >
      <div className="max-w-[430px]">
        <h3 className="mb-[16px] mt-[20px] text-[18px] font-[600] leading-[1.4] tracking-[-0.28px] text-neutral90 md:mt-[63px] md:text-[28px]">
          {title}
        </h3>
        <p className="max-w-[400px] text-[16px] leading-normal text-neutral70">{description}</p>
      </div>
      <div className={clsx('relative h-[300px] w-[94%] md:h-[412px] md:w-[400px] xl:w-[450px]')}>
        <Image
          src={image}
          alt="My Image"
          fill
          sizes="(max-width: 768px) 100vw, (max-width: 1200px) 50vw, 33vw"
          style={{ objectFit: 'contain', objectPosition: right ? 'right' : 'left' }}
        />
      </div>
    </div>
  );
}
