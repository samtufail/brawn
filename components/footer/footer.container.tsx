import Link from 'next/link';
import { footerData } from '@/data/common';
import { LogoIcon } from '@/icons';

const { copyright, links, disclosure } = footerData;

export function Footer() {
  return (
    <footer className="global-container px-[30px] py-[40px] xl:p-0">
      <div className="flex flex-col justify-between gap-[28px] xl:gap-[40px]">
        <div className="w-full">
          <Link href="/">
            <LogoIcon />
          </Link>
        </div>
        <div className="grid grid-cols-2 items-start justify-between gap-[40px] pr-[70px] md:mt-0 md:min-w-[760px] md:gap-0 xl:flex xl:flex-row xl:pr-[96px]">
          {links.map((link) => (
            <div className="flex flex-col gap-[15px] md:gap-[20px]" key={link.title}>
              <h6 className="mb-[2px] text-[18px] font-[600] leading-[1.4] text-neutral90 xl:font-[500]">
                {link.title}
              </h6>

              <div className="flex flex-col gap-[10px] md:gap-[18px]">
                {link.links.map((el) => (
                  <Link
                    key={el.href}
                    href={el.href}
                    target={el.target || '_self'}
                    className="text-[16px] leading-normal text-neutral70 opacity-60 transition-all hover:text-[#32343a] hover:opacity-100"
                  >
                    {el.title}
                  </Link>
                ))}
              </div>
            </div>
          ))}
        </div>
      </div>
      <hr className="mb-0 mt-[20px] border-b-neutral30 md:mt-[40px]" />
      <p className="mb-0 mt-[14px] text-center text-sm text-slate-500 xl:mb-[14px]">{disclosure}</p>
      <p className="mb-0 mt-[24px] text-center xl:mb-[27px]">{copyright}</p>
    </footer>
  );
}
