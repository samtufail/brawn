import * as React from 'react';
import { Slot } from '@radix-ui/react-slot';
import { cva, type VariantProps } from 'class-variance-authority';

import { cn } from '@/lib/utils';

const buttonVariants = cva(
  'w-auto rounded-[4px] border-2 border-solid border-transparent text-[14px] font-[600] leading-[1.7] tracking-[-0.14px] px-[32px] py-[12px]',
  {
    variants: {
      variant: {
        primary:
          'bg-primary hover:border-primary hover:text-primary text-[#eeeff4] transition-all hover:bg-transparent visited:outline-none',
        secondary:
          'hover:bg-primary hover:text-[#eeeff4] border-primary text-primary transition-all bg-transparent visited:outline-none',
        secondaryFullWidth:
          'hover:bg-primary hover:text-[#eeeff4] border-primary text-primary transition-all bg-transparent visited:outline-none w-full mb-5',
        link: 'text-primary underline-offset-4 hover:underline',
      },
    },
    defaultVariants: {
      variant: 'primary',
    },
  },
);

export interface ButtonProps
  extends React.ButtonHTMLAttributes<HTMLButtonElement>,
    VariantProps<typeof buttonVariants> {
  asChild?: boolean;
}

const Button = React.forwardRef<HTMLButtonElement, ButtonProps>(
  ({ className, variant, asChild, ...props }, ref) => {
    const Comp = asChild ? Slot : 'button';
    return <Comp className={cn(buttonVariants({ variant, className }))} ref={ref} {...props} />;
  },
);

Button.defaultProps = { asChild: false };
Button.displayName = 'Button';

export { Button, buttonVariants };
