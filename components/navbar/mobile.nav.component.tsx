'use client';

import { useRef, useState } from 'react';
import Link from 'next/link';
import { LogoDarkIcon, LogoIcon } from '@/icons';
import clsx from 'clsx';
import { useOnClickOutside } from 'usehooks-ts';

import { NavItem } from './nav-item.component';
import { NavbarProps } from './navbar.component';

export function MobileNav({ links, transparent = false }: NavbarProps) {
  const [open, setOpen] = useState(false);

  const ref = useRef(null);

  const handleClickOutside = () => {
    if (open) {
      setOpen(false);
    }
  };

  useOnClickOutside(ref, handleClickOutside);

  return (
    <div className={clsx('block xl:hidden')}>
      <div
        className={clsx(
          'border-b-solid relative z-20 mx-auto flex w-full items-center justify-between px-[30px] py-[20px]',
          {
            'border-b border-b-[#eeeff4] bg-white': !transparent,
          },
        )}
      >
        <div>
          <Link href="/" className="pb-[5px]" prefetch={false}>
            {transparent ? <LogoDarkIcon /> : <LogoIcon />}
          </Link>
        </div>
        <div>
          <button
            onClick={() => setOpen((prev) => !prev)}
            ref={ref}
            aria-label="Open/Close Menu"
            type="button"
            className="flex min-h-[15px] flex-col items-center justify-center"
          >
            <span
              className={`block h-0.5 w-6 rounded-sm  transition-all duration-300 ease-out ${open ? 'translate-y-1 rotate-45' : '-translate-y-0.5'} ${transparent ? 'bg-white' : 'bg-primary'}`}
            />
            <span
              className={`my-0.5 block h-0.5 w-6 rounded-sm  transition-all duration-300 ease-out ${open ? 'opacity-0' : 'opacity-100'} ${transparent ? 'bg-white' : 'bg-primary'}`}
            />
            <span
              className={`block h-0.5 w-6 rounded-sm  transition-all duration-300 ease-out ${open ? '-translate-y-1 -rotate-45' : 'translate-y-0.5'} ${transparent ? 'bg-white' : 'bg-primary'}`}
            />
          </button>
        </div>
      </div>
      <nav
        role="navigation"
        className={clsx(
          'fixed z-10 flex w-full flex-col items-center justify-between gap-[10px] bg-[#f7f7f7] pb-[35px] pt-[20px] text-center',
          { 'translate-y-[-400px]': !open },
          { 'translate-y-[0px]': open },
        )}
        style={{ transition: 'transform 400ms ease 0s' }}
      >
        {links.map((link) => (
          <NavItem href={link.href} key={link.href}>
            {link.title}
          </NavItem>
        ))}
      </nav>
    </div>
  );
}
