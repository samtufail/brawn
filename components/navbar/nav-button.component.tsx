'use client';

import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { commonData } from '@/data/common';

import { Button } from '../button/button.component';

const { waitlistLink, waitlistButtonText, employerLink } = commonData;

export function NavButton() {
  const pathname = usePathname();
  const isEmployer = pathname === '/employers';
  return (
    <Link
      href={isEmployer ? employerLink : waitlistLink}
      target="_blank"
      prefetch={false}
      className="visited:outline-none"
    >
      <Button>{waitlistButtonText}</Button>
    </Link>
  );
}
