import Link from 'next/link';
import { LogoDarkIcon, LogoIcon } from '@/icons';
import clsx from 'clsx';

import { NavButton } from './nav-button.component';
import { NavDropdown } from './nav-dropdown.component';
import { NavItem } from './nav-item.component';

export interface LinkType {
  href: string;
  title: string;
  links?: { href: string; title: string }[];
}

export interface NavbarProps {
  links: LinkType[];
  transparent?: boolean;
  hideBorder?: boolean;
}

export async function Navbar({ links = [], transparent = false, hideBorder = false }: NavbarProps) {
  return (
    <div
      className={clsx(
        'hidden p-[15px_80px_17px] xl:block',
        { 'relative z-50 text-white': transparent },
        { 'border-b-solid border-b border-b-[#eeeff4]': !transparent && !hideBorder },
      )}
    >
      <div className="mx-auto grid w-full max-w-screen-xl auto-cols-fr auto-rows-auto grid-cols-[max-content_max-content_max-content] items-center justify-between gap-[16px]">
        <Link prefetch={false} href="/">
          {transparent ? <LogoDarkIcon /> : <LogoIcon />}
        </Link>

        <nav role="navigation" className="flex justify-between gap-[61px]">
          {links.map((link) => {
            if (link?.links) {
              return (
                <NavDropdown
                  href={link.href}
                  title={link.title}
                  links={link?.links}
                  key={link.href}
                  transparent={transparent}
                />
              );
            }
            return (
              <NavItem href={link.href} key={link.href}>
                {link.title}
              </NavItem>
            );
          })}
        </nav>

        <NavButton />
      </div>
    </div>
  );
}
