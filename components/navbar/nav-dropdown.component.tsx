'use client';

import { useRef, useState } from 'react';
import { usePathname } from 'next/navigation';
import { ArrowDownIcon } from '@/icons';
import clsx from 'clsx';
import { useOnClickOutside } from 'usehooks-ts';

import { NavItem } from './nav-item.component';
import type { LinkType } from './navbar.component';

interface NavDropdownProps extends LinkType {
  transparent?: boolean;
}

export function NavDropdown({ title, href, links, transparent = false }: NavDropdownProps) {
  const [open, setOpen] = useState(false);

  const ref = useRef(null);

  const handleClickOutside = () => {
    if (open) {
      setOpen(false);
    }
  };

  const pathname = usePathname();

  useOnClickOutside(ref, handleClickOutside);

  return (
    <div className="relative flex flex-col gap-3" ref={ref}>
      <NavItem
        href={href}
        onClick={() => setOpen((prev) => !prev)}
        className={clsx({ 'border-primary': href.includes(pathname) })}
      >
        {title}
        &nbsp;
        <ArrowDownIcon transparent={transparent} />
      </NavItem>
      <div
        className={clsx(
          'absolute top-[48px] z-10 flex min-w-[140px] flex-col items-center justify-between gap-[10px] rounded-lg bg-[#f7f7f7] pb-[10px] pt-[6px] text-center text-black transition-all',
          { 'invisible opacity-0': !open },
          { 'visible opacity-100': open },
        )}
      >
        {links?.map((link) => (
          <NavItem
            key={link.href}
            href={link.href}
            className="md:w-4/5"
            onClick={() => {
              setOpen(false);
            }}
          >
            {link.title}
          </NavItem>
        ))}
      </div>
    </div>
  );
}
