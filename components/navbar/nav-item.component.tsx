'use client';

import { MouseEventHandler, ReactNode } from 'react';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import clsx from 'clsx';

type NavItemProps = {
  href: string;
  children: ReactNode;
  onClick?: MouseEventHandler<HTMLAnchorElement> | undefined;
  className?: string;
};

export function NavItem({ href, children, onClick, className }: NavItemProps) {
  const pathname = usePathname();
  return (
    <Link
      href={href}
      onClick={onClick}
      prefetch={false}
      className={clsx(
        className,
        'flex w-fit items-center gap-2 border-b-2 border-solid border-[rgba(220,135,48,0)] p-[10px_0_8px] text-center text-[14px] font-[500] leading-[170%] outline-0 transition-all hover:border-primary hover:text-primary hover:no-underline md:w-[unset]',
        {
          'border-primary ': pathname === href,
        },
      )}
    >
      {children}
    </Link>
  );
}
