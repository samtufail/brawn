// import { BarsIcon } from '@/icons';
import { cva, type VariantProps } from 'class-variance-authority';

import { cn } from '@/lib/utils';

const pointVariants = cva(
  'flex w-fit items-center gap-2 rounded px-2 py-0 h-[27px] shadow-[0_20px_50px_0_rgba(34,51,34,0.1)] md:py-[10px] md:px-3 md:h-[44px]',
  {
    variants: {
      variant: {
        primary: 'bg-[#ffede5]',
        secondary: 'bg-[#ede6ff]',
        trinity: 'bg-[#dbf1ff]',
      },
    },
    defaultVariants: {
      variant: 'primary',
    },
  },
);

type PointProps = React.ComponentPropsWithoutRef<'div'> &
  VariantProps<typeof pointVariants> & {
    title: string;
    icon: React.ReactNode;
  };

export function HeroPoint({ title, icon, variant, className }: PointProps) {
  return (
    <div className={cn(pointVariants({ variant, className }))}>
      <div className="size-[15px] md:size-[28px]">{icon}</div>
      <span className="text-[12px] leading-[150%] md:text-[16px] md:leading-normal">{title}</span>
    </div>
  );
}
